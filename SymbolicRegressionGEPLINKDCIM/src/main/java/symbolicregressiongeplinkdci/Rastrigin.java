/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package symbolicregressiongeplinkdci;

import Evaluations.EvaluationInverseRangedRMSE_GEPLINKDCI;
import examples.modeling.CsvFilesHelper;
import examples.operators.CrossoverSingleElementSwap;
import examples.operators.CrossoverSingleDomainSwap;
import examples.operators.CrossoverSinglePoint;
import Evaluations.EvaluationRMSE_GEPLINKDCI1;
import Evaluations.EvaluationSSE_GEPLINKDCI;
import examples.operators.MutationSingleElementBoundaries;
import Operators.MutationBoundariesDC;
import Operators.MutationDomainStepDC;
import Operators.MutationElementStep;
import Operators.MutationElementStepDC;
import examples.operators.MutationRandomGeneInsertion;
import examples.operators.MutationRandomDomainInsertion;
import examples.operators.MutationTwoPointsBoundaries;
import Operators.MutationTwoPointElementStep;
import Operators.MutationPlainCopy;
import Operators.PermutationElementSwap;
import examples.operators.PermutationTranspositionIS;
import examples.operators.PermutationTranspositionRIS;
import examples.operators.SelectionSUS;
import modeling.DataHelper;
import modeling.ChromosomeCoding;
import modeling.GeneCoding;
import modeling.Evaluation;
import examples.modeling.FloatElementHandler;
import examples.modeling.IntegerElementHandler;
import modeling.Nucleotide;
import core.gepAlgorithm;
import core.Chromosome;
import core.Operator;
import core.Population;
import Operators.MutationHeadRandomElementInsertion;
import Operators.MutationHeadRandomFunctionInsertion;
import Operators.MutationHeadRandomTerminalInsertion;
import Operators.MutationRandomElementInsertionDC;
import Operators.MutationGeneHighBoundary;
import Operators.PermutationDomainSwap;
import Operators.PermutationGeneSwap;
import Operators.PermutationTranspositionGene;
import examples.operators.CrossoverSingleGeneSwap;
import examples.operators.CrossoverTwoPoints;
import examples.operators.MutationRandomChromosomeInsertion;
import examples.operators.MutationRandomElementInsertion;
import examples.operators.PermutationDomainInversion;
import java.util.ArrayList;
import modeling.ElementHandler;
import modeling.Phenotype;

/**
 *
 * @author alejandro
 */
public class Rastrigin {

    /**
     * @param args the command line arguments
     */
    public static void main( String[] args ) {
        //Create the nucleotides 
        Nucleotide[] nucleotides = new Nucleotide[]{
            new Nucleotide( "+" , 2 ) ,
            new Nucleotide( "-" , 2 ) ,
            new Nucleotide( "*" , 2 ) ,
            new Nucleotide( "/" , 2 ) ,
            new Nucleotide( "cos" , 1 ),
            new Nucleotide( "x" , 0 ) ,
            new Nucleotide( "?" , 0 ) ,
        };

        //Create a geneCoding "GEP-GENE"
        String geneCodingName = "GEP-GENE";
        String[] domainNamesArray = new String[]{ "Head" , "Tail" , "TailDCI" , "TailDC" };
        Object[] highBoundsArray = new Object[]{ 6 , 6 , 7 , (float) 10 };
        Object[] lowBoundsArray = new Object[]{ 0 , 5 , 0 , (float) 0 };
        int headSize = 9;
        int[] domainSizesArray = new int[]{ headSize , headSize * (2 - 1) + 1 , headSize * (2 - 1) + 1 , 10 };
        ElementHandler[] elementHandlersArray = new ElementHandler[]{
            new IntegerElementHandler() ,
            new IntegerElementHandler() ,
            new IntegerElementHandler() ,
            new FloatElementHandler()
        };
        GeneCoding geneCod1 = new GeneCoding(
                domainNamesArray , geneCodingName , highBoundsArray , lowBoundsArray , domainSizesArray , elementHandlersArray
        );

        // Now, create a link geneCoding
        GeneCoding geneCod2 = new GeneCoding(
                new String[]{ "Link" } //domainNamesArray
                , "Link" //geneCodingName
                , new Object[]{ 0 } //highBoundsArray
                , new Object[]{ 0 } //lowBoundsArray
                , new int[]{ 1 } //domainSizesArray
                , new ElementHandler[]{
                    new IntegerElementHandler() }//ElementHandlers
        );

        //Create chromosomeCoding
        GeneCoding[] geneCodingArray = new GeneCoding[]{ geneCod1 , geneCod1, geneCod2 };
        String chromosomeCodingName = "GEP-LINK--";
        ChromosomeCoding codcrom1 = new ChromosomeCoding(
                geneCodingArray ,
                chromosomeCodingName
        );

        //Create the algorithm
        String nombre = "GEP-LINK";
        int cantidadpoblaciones = 1;
        int[] populationSizesArray = new int[]{ 100 };
        Population[] populationsArray = new Population[]{
            new Population( populationSizesArray[0] , 0 , codcrom1 )
        };
        int[] elitesSizesArray = new int[]{ 1 };
        int generations = 20000;
        float acceptableFitness = 100000;
        Evaluation evaluation = new EvaluationInverseRangedRMSE_GEPLINKDCI( nucleotides , acceptableFitness, 100 );
        DataHelper helper = CsvFilesHelper.getInstance( "../DataSets/" , "Rastrigin.csv" );
        gepAlgorithm algorithm = new gepAlgorithm(
                nombre//nombre del algoritmo
                , cantidadpoblaciones ,
                populationSizesArray ,
                populationsArray ,
                elitesSizesArray ,
                new ArrayList<Operator>() //List of operators
                , null //valuesArray, needed when operating
                , null //originPopulationArray, needed when operating
                , null //destinationPopulationArray, needed when operating
                , generations 
                , evaluation 
                , helper
        );

//        addAllOperators( algorithm , populationsArray , populationSizesArray );
        addOptimizedOperators( algorithm, populationsArray, populationSizesArray);
//        addTestOperators( algorithm , populationsArray , populationSizesArray );
        
        algorithm.setDebug( false );
        algorithm.dontUseHistoricalRecord();
        algorithm.paralelizeFitnessEvaluation();
        
        algorithm.setGenerations( 30000 );
        for (int oi = 0; oi < algorithm.getOperatorListSize() ; oi++){
            System.out.println(algorithm.getOperator( oi).getName());
        }
        algorithm.execute();

        //Muestro el RMSE 
        Chromosome best = algorithm.getElites()[0].getChromosome( 0 );
        
        Evaluation error = new EvaluationRMSE_GEPLINKDCI1( nucleotides , acceptableFitness ,1000);
        Phenotype func = new examples.modeling.Function();
        System.out.println( best.getFitness() );
        error.evaluate( best );
        System.out.println( + best.getFitness() );
        func = error.expressChromosome( best );
        System.out.println( func.phenotype.toString() );
        System.out.print( "\n\nPaso la ejecucion sin runtime errors\n" );
    }

    public static void addAllOperators( gepAlgorithm alg1 , Population[] poblaciones , int[] tamañopoblaciones ) {
        /**
         * fc = first usable chromosome lc = last usable chromosome op = origin
         * population dp = destination population
         */
        int fc = 0, lc = poblaciones[0].getchromosomes().size(), op = 0, dp = 0;
        float value = (float) 0.1;

        alg1.addOperator( new SelectionSUS() , (float) 1 , op , fc , lc , dp , fc , lc - 1 );
        //Crossovers
        alg1.addOperator( new CrossoverSingleElementSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverSingleGeneSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverSingleDomainSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverSinglePoint( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverTwoPoints( '%' ) , value , op , fc , lc , dp , fc , lc );

        //Example Mutations
        alg1.addOperator( new MutationRandomChromosomeInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationRandomDomainInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationRandomElementInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationRandomGeneInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationSingleElementBoundaries( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationTwoPointsBoundaries( '%' ) , value , op , fc , lc , dp , fc , lc );

        //Specific mutations
        alg1.addOperator( new MutationBoundariesDC( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationDomainStepDC( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationElementStep( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationElementStepDC( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationHeadRandomElementInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationHeadRandomFunctionInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationHeadRandomTerminalInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationRandomElementInsertionDC( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationGeneHighBoundary( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationTwoPointElementStep( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationPlainCopy( '%' ) , value , op , fc , lc , dp , fc , lc );

        // Example Permutations
        alg1.addOperator( new PermutationDomainInversion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new PermutationTranspositionIS( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new PermutationTranspositionRIS( '%' ) , value , op , fc , lc , dp , fc , lc );

        //Specific permutations
        alg1.addOperator( new PermutationDomainSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator(new PermutationElementSwap('%'), value, op, fc, lc, dp, fc,lc);
        alg1.addOperator(new PermutationGeneSwap('%'), value, op, fc, lc, dp, fc,lc);
        alg1.addOperator(new PermutationTranspositionGene('%'), value, op, fc, lc, dp, fc,lc);
    }
   
     public static void addOptimizedOperators( gepAlgorithm alg1 , Population[] poblaciones , int[] tamañopoblaciones ) {

        /**
         * fc = first usable chromosome lc = last usable chromosome op = origin
         * population dp = destination population
         *
         */
        int fc = 0, lc = poblaciones[0].getchromosomes().size(), op = 0, dp = 0;
		float veryLowValue=0.01f;
		float lowValue=0.02f;
        float value=0.05f;
		float highValue=0.1f;
        alg1.addOperator( new SelectionSUS() , (float) 1 , op , fc , lc , dp , fc , lc - 1 );
        //Crossovers
        alg1.addOperator( new CrossoverSingleElementSwap( '%' ) , highValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverSingleGeneSwap( '%' ) , highValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverSingleDomainSwap( '%' ) , highValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverSinglePoint( '%' ) , highValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverTwoPoints( '%' ) , highValue , op , fc , lc , dp , fc , lc );

        //Example Mutations
        alg1.addOperator( new MutationRandomChromosomeInsertion( '%' ) , highValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationRandomDomainInsertion( '%' ) , veryLowValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationRandomElementInsertion( '%' ) , lowValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationRandomGeneInsertion( '%' ) , veryLowValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationSingleElementBoundaries( '%' ) , veryLowValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationTwoPointsBoundaries( '%' ) , veryLowValue , op , fc , lc , dp , fc , lc );

        //Specific mutations
        alg1.addOperator( new MutationBoundariesDC( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationDomainStepDC( '%' ) , lowValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationElementStep( '%' ) , lowValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationElementStepDC( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationHeadRandomElementInsertion( '%' ) , lowValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationHeadRandomFunctionInsertion( '%' ) , lowValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationHeadRandomTerminalInsertion( '%' ) , lowValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationRandomElementInsertionDC( '%' ) , veryLowValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationGeneHighBoundary( '%' ) , lowValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationTwoPointElementStep( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationPlainCopy( '%' ) , lowValue , op , fc , lc , dp , fc , lc );

        // Example Permutations
        alg1.addOperator( new PermutationDomainInversion( '%' ) , lowValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new PermutationTranspositionIS( '%' ) , veryLowValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new PermutationTranspositionRIS( '%' ) , value , op , fc , lc , dp , fc , lc );

        //Specific permutations
        alg1.addOperator( new PermutationDomainSwap( '%' ) , veryLowValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new PermutationElementSwap( '%' ) , highValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new PermutationGeneSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new PermutationTranspositionGene( '%' ) , value , op , fc , lc , dp , fc , lc );
    }

    public static void addOptimizedOperators2(gepAlgorithm alg1 , Population[] poblaciones , int[] tamañopoblaciones ){
        /**
         * fc = first usable chromosome lc = last usable chromosome op = origin
         * population dp = destination population
         */
        int fc = 0, lc = poblaciones[0].getchromosomes().size(), op = 0, dp = 0;
        float value = (float) 0.1;

        alg1.addOperator( new SelectionSUS() , (float) 1 , op , fc , lc , dp , fc , lc - 1 );
        //Crossovers
        alg1.addOperator( new CrossoverSingleElementSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverSingleGeneSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverSingleDomainSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverSinglePoint( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverTwoPoints( '%' ) , value , op , fc , lc , dp , fc , lc );

        //Example Mutations
        alg1.addOperator( new MutationRandomChromosomeInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationRandomDomainInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
//        alg1.addOperator( new MutationRandomElementInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
//        alg1.addOperator( new MutationRandomGeneInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationSingleElementBoundaries( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationTwoPointsBoundaries( '%' ) , value , op , fc , lc , dp , fc , lc );

        //Specific mutations
        alg1.addOperator( new MutationBoundariesDC( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationDomainStepDC( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationElementStep( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationElementStepDC( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationHeadRandomElementInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationHeadRandomFunctionInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationHeadRandomTerminalInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
//        alg1.addOperator( new MutationRandomElementInsertionDC( '%' ) , value , op , fc , lc , dp , fc , lc );
//        alg1.addOperator( new MutationGeneHighBoundary( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationTwoPointElementStep( '%' ) , value , op , fc , lc , dp , fc , lc );
//        alg1.addOperator( new MutationPlainCopy( '%' ) , value , op , fc , lc , dp , fc , lc );

        // Example Permutations
        alg1.addOperator( new PermutationDomainInversion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new PermutationTranspositionIS( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new PermutationTranspositionRIS( '%' ) , value , op , fc , lc , dp , fc , lc );

        //Specific permutations
        alg1.addOperator( new PermutationDomainSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator(new PermutationElementSwap('%'), value, op, fc, lc, dp, fc,lc);
//        alg1.addOperator(new PermutationGeneSwap('%'), value, op, fc, lc, dp, fc,lc);
//        alg1.addOperator(new PermutationTranspositionGene('%'), value, op, fc, lc, dp, fc,lc);
    }

    public static void addTestOperators(gepAlgorithm alg1 , Population[] poblaciones , int[] tamañopoblaciones ){
        alg1.addOperator(new SelectionSUS(),
                (float) 0.1,
                0,
                0,
                poblaciones[0].getchromosomes().size(),
                0,
                0,
                poblaciones[0].getchromosomes().size()-1
        );
        alg1.addOperator(new MutationGeneHighBoundary('%'),
                (float) 0.1,
                0,
                0,
                poblaciones[0].getchromosomes().size(),
                0,
                0,
                poblaciones[0].getchromosomes().size()-1
        );
        alg1.addOperator(new MutationElementStep('%'),
                (float) 0.1,
                0,
                0,
                poblaciones[0].getchromosomes().size(),
                0,
                0,
                poblaciones[0].getchromosomes().size()-1
        );
        alg1.addOperator(new MutationElementStepDC('%'),
                (float) 0.1,
                0,
                0,
                poblaciones[0].getchromosomes().size(),
                0,
                0,
                poblaciones[0].getchromosomes().size()-1
        );
    }

}
