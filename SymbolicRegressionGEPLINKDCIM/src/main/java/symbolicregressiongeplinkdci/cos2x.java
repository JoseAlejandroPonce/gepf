/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package symbolicregressiongeplinkdci;

import examples.modeling.CsvFilesHelper;
import examples.modeling.Function;
import examples.modeling.FloatElementHandler;
import examples.modeling.IntegerElementHandler;
import Evaluations.EvaluationInverseRangedRMSE_GEPLINKDCI;
import Evaluations.EvaluationRMSE_GEPLINKDCI1;
import Evaluations.EvaluationRangedRMSE_GEPLINKDCI;
import Evaluations.EvaluationSSE_GEPLINKDCI;
import Evaluations.EvaluationSSE_GEPLINKDCI_PP;
import examples.operators.CrossoverSingleElementSwap;
import examples.operators.CrossoverSingleDomainSwap;
import examples.operators.CrossoverSinglePoint;
import examples.operators.CrossoverTwoPoints;
import examples.operators.MutationSingleElementBoundaries;
import Operators.MutationBoundariesDC;
import Operators.MutationElementStep;
import Operators.MutationElementStepDC;
import examples.operators.MutationRandomElementInsertion;
import examples.operators.MutationRandomGeneInsertion;
import examples.operators.MutationRandomDomainInsertion;
import examples.operators.MutationTwoPointsBoundaries;
import Operators.MutationTwoPointElementStep;
import Operators.MutationPlainCopy;
import Operators.PermutationElementSwap;
import Operators.PermutationGeneSwap;
import examples.operators.PermutationTranspositionIS;
import examples.operators.PermutationTranspositionRIS;
import examples.operators.SelectionSUS;
import modeling.DataHelper;
import modeling.ChromosomeCoding;
import modeling.GeneCoding;
import modeling.Evaluation;
import modeling.Nucleotide;
import core.gepAlgorithm;
import core.Chromosome;
import core.Operator;
import core.Population;
import Operators.MutationHeadRandomElementInsertion;
import Operators.MutationHeadRandomFunctionInsertion;
import Operators.MutationHeadRandomTerminalInsertion;
import Operators.MutationDomainStepDC;
import Operators.MutationGeneHighBoundary;
import examples.operators.MutationRandomChromosomeInsertion;
import Operators.MutationRandomElementInsertionDC;
import Operators.PermutationDomainSwap;
import Operators.PermutationTranspositionGene;
import examples.operators.CrossoverSingleGeneSwap;
import examples.operators.PermutationDomainInversion;
import java.util.ArrayList;
import modeling.ElementHandler;
import modeling.Phenotype;

/**
 *
 * @author alejandro
 */
public class cos2x {

    /**
     * @param args the command line arguments
     */
    public void main( String[] args ) {
        //Create nucleotides List
        Nucleotide[] nucleotides = new Nucleotide[]{
            new Nucleotide( "+" , 2 ) ,
            new Nucleotide( "-" , 2 ) ,
            new Nucleotide( "*" , 2 ) ,
            new Nucleotide( "/" , 2 ) ,
            new Nucleotide( "cos" , 1 ) ,
            new Nucleotide( "sin" , 1 ) ,
            new Nucleotide( "?" , 0 ) ,
            new Nucleotide( "x" , 0 ) 
        };

        //Create a geneCoding
        String geneCodingName = "GEP-GENE";
        String[] domainNamesArray = new String[]{ "Head", "Tail", "TailDCI", "TailDC" };
        Object[] highBoundsArray = new Object[]{ 7 , 7 , 14 , (float) 100 };
        Object[] lowBoundsArray = new Object[]{ 0 , 6 , 0 , (float) 0 };
        int headSize = 7;
        int[] domainSizesArray = new int[]{ 
            headSize , headSize*(2-1)+1 , headSize*(2-1)+1 , 15
        };
        ElementHandler[] element_handlers = new ElementHandler[]{
            new IntegerElementHandler(),
            new IntegerElementHandler(),
            new IntegerElementHandler(),
            new FloatElementHandler()
        };
        //Call the geneCoding Constructor
        GeneCoding codgen1;
        codgen1 = new GeneCoding(
                domainNamesArray 
                , geneCodingName 
                , highBoundsArray 
                , lowBoundsArray 
                , domainSizesArray 
                , element_handlers 
        );

        //Create the GeneCoding with the linking function
        GeneCoding codgen2;
        codgen2 = new GeneCoding(
                new String[] { "Link" } ,
                "Link" ,
                new Object[] { 0 } ,
                new Object[] { 0 } , 
                new int[] { 1 } , 
                new ElementHandler[] { new IntegerElementHandler() }
        );
        //Create the chromosomeCoding
        ChromosomeCoding codcrom1 = new ChromosomeCoding( 
                new GeneCoding[] { codgen1, codgen1 , codgen2 } ,
                "GEP-LINK-+"
        );

        //Create the algorithm
        String nombre = "GEP-LINK";
        int cantidadpoblaciones = 1;
        int[] populationSizesArray = new int[]{ 100 };
        Population[] populationsArray = new Population[1];
        populationsArray[0] = new Population( populationSizesArray[0] , 0 , codcrom1 );
        int[] tamaño_elites = new int[]{ 1 };
        int generaciones = 10000;
        
        float acceptableFitness = 999;
        Evaluation evaluation = new EvaluationSSE_GEPLINKDCI_PP( nucleotides , acceptableFitness , 1000 );

        DataHelper helper = CsvFilesHelper.getInstance( "../DataSets/" , "cos(2x).txt" );

        gepAlgorithm alg1 = new gepAlgorithm(
                nombre ,//nombre del algoritmo
                cantidadpoblaciones ,
                populationSizesArray ,
                populationsArray ,
                tamaño_elites ,
                new ArrayList<Operator>() //mando una lista vacia, podria mandar null
                , null //Valores 
                , null //poblacionorigen =new int[][]
                , null //poblaciondestino = new int[][]s
                , generaciones , evaluation , helper
        );
        
//        addAllOperators( alg1 , populationsArray , populationSizesArray );
        addOptimizedOperators2( alg1 , populationsArray , populationSizesArray );

        alg1.setDebug( false );
        alg1.dontUseHistoricalRecord();
        alg1.paralelizeFitnessEvaluation();
        alg1.setGenerations( 10000 );

        alg1.execute();
//        Evaluation sse = new EvaluationSSE_GEPLINKDCI( nucleotides , acceptableFitness , 1000 );

        Chromosome best = alg1.getElites()[0].getChromosome( 0 );
        
        Evaluation error = new EvaluationRMSE_GEPLINKDCI1( nucleotides , acceptableFitness ,1000);
        Phenotype func = new Function();
        System.out.println( best.getFitness() );
        error.evaluate( best );
        System.out.println( + best.getFitness() );
        func = error.expressChromosome( best );
        System.out.println( func.phenotype.toString() );
        System.out.print( "\n\nPaso la ejecucion sin runtime errors\n" );
    }
    
    public static void addAllOperators( gepAlgorithm alg1 , Population[] poblaciones , int[] tamañopoblaciones ) {
        /**
         * fc = first usable chromosome lc = last usable chromosome op = origin
         * population dp = destination population
         */
        int fc = 0, lc = poblaciones[0].getchromosomes().size(), op = 0, dp = 0;
        float value = (float) 0.1;

        alg1.addOperator( new SelectionSUS() , (float) 1 , op , fc , lc , dp , fc , lc - 1 );
        //Crossovers
        alg1.addOperator( new CrossoverSingleElementSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverSingleGeneSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverSingleDomainSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverSinglePoint( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverTwoPoints( '%' ) , value , op , fc , lc , dp , fc , lc );

        //Example Mutations
        alg1.addOperator( new MutationRandomChromosomeInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationRandomDomainInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationRandomElementInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationRandomGeneInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationSingleElementBoundaries( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationTwoPointsBoundaries( '%' ) , value , op , fc , lc , dp , fc , lc );

        //Specific mutations
        alg1.addOperator( new MutationBoundariesDC( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationDomainStepDC( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationElementStep( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationElementStepDC( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationHeadRandomElementInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationHeadRandomFunctionInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationHeadRandomTerminalInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationRandomElementInsertionDC( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationGeneHighBoundary( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationTwoPointElementStep( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationPlainCopy( '%' ) , value , op , fc , lc , dp , fc , lc );

        // Example Permutations
        alg1.addOperator( new PermutationDomainInversion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new PermutationTranspositionIS( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new PermutationTranspositionRIS( '%' ) , value , op , fc , lc , dp , fc , lc );

        //Specific permutations
        alg1.addOperator( new PermutationDomainSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator(new PermutationElementSwap('%'), value, op, fc, lc, dp, fc,lc);
        alg1.addOperator(new PermutationGeneSwap('%'), value, op, fc, lc, dp, fc,lc);
        alg1.addOperator(new PermutationTranspositionGene('%'), value, op, fc, lc, dp, fc,lc);
    }
    
    public static void addOptimizedOperators( gepAlgorithm alg1 , Population[] poblaciones , int[] tamañopoblaciones ) {

        /**
         * fc = first usable chromosome lc = last usable chromosome op = origin
         * population dp = destination population
         *
         */
        int fc = 0, lc = poblaciones[0].getchromosomes().size(), op = 0, dp = 0;
        float value = (float) 0.1;
        //Crossovers
        alg1.addOperator( new SelectionSUS() , (float) 1 , op , fc , lc , dp , fc , lc - 1 );
        alg1.addOperator( new CrossoverSingleElementSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverSingleGeneSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverSingleDomainSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverSinglePoint( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverTwoPoints( '%' ) , value , op , fc , lc , dp , fc , lc );

        //Mutations
        alg1.addOperator( new MutationRandomChromosomeInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationRandomGeneInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationSingleElementBoundaries( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationDomainStepDC( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationElementStep( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationElementStepDC( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationHeadRandomElementInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationHeadRandomFunctionInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationHeadRandomTerminalInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationTwoPointElementStep( '%' ) , value , op , fc , lc , dp , fc , lc );

        //Permutations
        alg1.addOperator( new PermutationTranspositionIS( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new PermutationTranspositionRIS( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new PermutationDomainSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
    }
    
    public static void addOptimizedOperators2( gepAlgorithm alg1 , Population[] poblaciones , int[] tamañopoblaciones ) {

        /**
         * fc = first usable chromosome lc = last usable chromosome op = origin
         * population dp = destination population
         *
         */
        int fc = 0, lc = poblaciones[0].getchromosomes().size(), op = 0, dp = 0;
        float value = (float) 0.1;
        //Crossovers
        alg1.addOperator( new SelectionSUS() , (float) 1 , op , fc , lc , dp , fc , lc - 1 );
        alg1.addOperator( new CrossoverSingleElementSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverSingleGeneSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverSingleDomainSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverSinglePoint( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverTwoPoints( '%' ) , value , op , fc , lc , dp , fc , lc );

        //Mutations
//        alg1.addOperator( new MutationRandomChromosomeInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationRandomGeneInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationSingleElementBoundaries( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationTwoPointsBoundaries( '%' ) , value , op , fc , lc , dp , fc , lc );
//        alg1.addOperator( new MutationDomainStepDC( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationElementStep( '%' ) , value , op , fc , lc , dp , fc , lc );
//        alg1.addOperator( new MutationElementStepDC( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationHeadRandomElementInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
//        alg1.addOperator( new MutationHeadRandomFunctionInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationHeadRandomTerminalInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationTwoPointElementStep( '%' ) , value , op , fc , lc , dp , fc , lc );

        //Permutations
        alg1.addOperator( new PermutationTranspositionIS( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new PermutationTranspositionRIS( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new PermutationDomainSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new PermutationElementSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
    }
}
