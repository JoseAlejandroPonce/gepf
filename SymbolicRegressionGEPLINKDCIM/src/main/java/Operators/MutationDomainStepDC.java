/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Operators;

import core.Chromosome;
import core.Domain;
import core.Mutation;

/**
 *
 * @author José Alejandro Ponce
 */
public class MutationDomainStepDC extends Mutation{

    public MutationDomainStepDC(char modo) {
        super(modo, "MutationDomainStepDC");
    }

    @Override
    public Chromosome operate(Chromosome operando) {
        Chromosome result = new Chromosome((Chromosome) operando);
        Domain domain = result.getDomainI(0,result.getGene(0).size()-1);
        
        float low_bound = (float) domain.getLowBound();
        float high_bound = (float) domain.getHighBound();
        float step = (float) (0.001*(Math.random()*(high_bound-low_bound)+low_bound));
        float aux;
        
        for (int elementIndex = 0; elementIndex < domain.size() ; elementIndex++){
            aux = (float) domain.getElement(elementIndex)+step;
            if ( aux > low_bound && aux < high_bound){
                domain.setElement( aux,elementIndex);
            }
        }
        return result;
    }
}
