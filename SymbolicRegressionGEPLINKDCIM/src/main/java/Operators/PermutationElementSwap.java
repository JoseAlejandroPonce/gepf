/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Operators;

import core.Chromosome;
import core.Gene;
import core.Domain;
import core.Permutation;
import java.util.Random;

/**
 *
 * @author alejandro
 */
public class PermutationElementSwap extends Permutation {

    public PermutationElementSwap(char c) {
        super(c, "PermutationElementSwap");
    }

    @Override
    public Chromosome operate(Chromosome cromosomaoperando) {
        Chromosome cromosoma;
        cromosoma = (Chromosome) cromosomaoperando;
        int geneIndex, domainIndex, elementIndex, gid, pid, eid, i;
        Object elementAux;
        String nombreparte;
        Random random = new Random();
        Chromosome cromaux = new Chromosome(cromosoma);

        geneIndex = random.nextInt(cromaux.size()-1);

        domainIndex = random.nextInt(cromaux.getGene(geneIndex).size());

        elementIndex = random.nextInt(cromaux.getDomainI(geneIndex, domainIndex).size());

        gid = random.nextInt(cromaux.size());

        while (cromaux.getGene(gid).getName().compareTo(cromaux.getGene(geneIndex).getName()) != 0) {
            gid = random.nextInt(cromaux.size());
        }
        pid = domainIndex;

        eid = random.nextInt(cromaux.getGene(gid).getDomainI(pid).size());
        //Swap the element
        elementAux =  cromaux.getElement(geneIndex, domainIndex, elementIndex);
        cromaux.setElement( cromaux.getElement(gid, pid, eid), geneIndex, domainIndex, elementIndex);
        cromaux.setElement( elementAux, gid, pid, eid);

        return cromaux;
    }
}
