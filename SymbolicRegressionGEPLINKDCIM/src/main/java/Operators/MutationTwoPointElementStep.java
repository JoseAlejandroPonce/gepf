/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Operators;

import core.Chromosome;
import core.Gene;
import core.Mutation;
import core.Domain;
import java.util.Random;

/**
 *
 * @author José Alejandro Ponce
 */
public class MutationTwoPointElementStep extends Mutation {

    public MutationTwoPointElementStep(char modo) {
        super(modo, "MutationTwoPointElementStep");
    }

    @Override
    public Chromosome operate(Chromosome operando) {
        Chromosome result = new Chromosome(operando);
        Domain parte1;
        Domain parte2;
        Gene gen1;
        Gene gen2;

        int domainIndex1, domainIndex2 , geneIndex1 , geneIndex2 , elementIndex1
                , elementIndex2 , x , y , step ;

        Random random = new Random();

        geneIndex1 = random.nextInt(result.size()-1);
        geneIndex2 = random.nextInt(result.size()-1);

        gen1 = result.getGene(geneIndex1);
        gen2 = result.getGene(geneIndex2);

        domainIndex1 = (int) Math.floor((Math.random()*(gen1.size()-1)));
        domainIndex2 = (int) Math.floor((Math.random()*(gen2.size()-1)));

        parte1 = gen1.getDomainI(domainIndex1);
        parte2 = gen2.getDomainI(domainIndex2);

        elementIndex1 = (int) Math.floor((Math.random()*(parte1.size()-1)));
        elementIndex2 = (int) Math.floor((Math.random()*(parte2.size()-1)));

        x = (int) parte1.getElement(elementIndex1);
        y = (int) parte2.getElement(elementIndex2);

        if (random.nextFloat() > 0.5) {
            step = 1;
        } else {
            step = -1;
        }
        if (x + step >= (int) parte1.getLowBound() && x+step <= (int) parte1.getHighBound()) {
            parte1.setElement( x + step,elementIndex1);
        }
        if (y + step >= (int) parte2.getLowBound() && y+step <= (int) parte2.getHighBound()) {
            parte2.setElement( y + step,elementIndex2);
        }
        return result;
    }
}
