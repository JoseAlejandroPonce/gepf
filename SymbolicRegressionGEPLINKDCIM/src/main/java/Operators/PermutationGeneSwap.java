/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Operators;

import core.Chromosome;
import core.Gene;
import core.Permutation;
import java.util.Random;

/**
 *
 * @author alejandro
 */
public class PermutationGeneSwap extends Permutation {

    public PermutationGeneSwap(char modo) {
        super(modo, "PermutationGeneSwap");
    }

    @Override
    public Chromosome operate(Chromosome cromosoma) {
        Random random = new Random();
        int geneIndex1, geneIndex2;
        Gene geneAux;

        Chromosome result = new Chromosome(cromosoma);

        geneIndex1 = random.nextInt(result.size());

        geneAux = new Gene(result.getGene(geneIndex1));

        geneIndex2 = random.nextInt(result.size());
        for (int contador = 0; contador < result.size(); contador++) {
            if ( result.getGene(geneIndex2).getName().compareTo(geneAux.getName())==0 ) {
                result.setGen(result.getGene(geneIndex2), geneIndex1);
                result.setGen(geneAux, geneIndex2);
                break;
            }
            geneIndex2 = random.nextInt(result.size());
        }
        return result;
    }
}
