/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Operators;

import core.Chromosome;
import core.Mutation;

/**
 *
 * @author José Alejandro Ponce
 */
public class MutationGeneHighBoundary extends Mutation{

    public MutationGeneHighBoundary(char modo) {
        super(modo, "MutationTurnAllGenesIntoConstants");
    }

    @Override
    public Chromosome operate(Chromosome operando) {
        Chromosome result = new Chromosome(operando);
        //Since, in this case, the numerical constant is the last nucleotid
        //I will put that value in the first element of each gene.
        //The loop goes to chromosome.size()-1 because it needs to avoid the 
        //gene with the linking function or I will get an exception.
        for (int geneIndex = 0; geneIndex < result.size() ; geneIndex++){
            for (int domainIndex = 0; domainIndex < result.getGene(geneIndex).size() ; domainIndex++){
                for (int elementIndex = 0 ; elementIndex < result.getDomainI(geneIndex,0).size() ; elementIndex++){
                    result.setElement( result.getDomainI( geneIndex , 0 ).getHighBound() , geneIndex , 0 , elementIndex );
                }
            }
        }
        return result;
    }
}
