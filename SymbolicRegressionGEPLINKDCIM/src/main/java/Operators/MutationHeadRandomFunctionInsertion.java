/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Operators;

import core.Chromosome;
import core.Gene;
import core.Mutation;
import core.Domain;
import java.util.Random;

/**
 *
 * @author José Alejandro Ponce
 */
public class MutationHeadRandomFunctionInsertion extends Mutation{

    public MutationHeadRandomFunctionInsertion(char modo) {
        super(modo,"MutationHeadRandomFunctionInsertion");
    }

    @Override
    public Chromosome operate(Chromosome operando) {
        Chromosome result=new Chromosome(operando);
        Domain domain;
        Gene gene;
        
        Random random = new Random();

        domain=result.getDomainI(0,0);
        
        int elementIndex;
        
        elementIndex = random.nextInt(domain.size());
        
        Object nuevoElemento;
        double low_bound=(double)(float)(int) result.getDomainI(0,1).getLowBound();
        nuevoElemento = (int) Math.floor(Math.random()*(low_bound));
       
        domain.setElement( nuevoElemento,elementIndex);
        return result;
    }
}
