/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Operators;

import core.Chromosome;
import core.Gene;
import core.Mutation;
import core.Domain;
import java.util.Random;

/**
 * 
 * @author José Alejandro Ponce
 */
public class MutationElementStep extends Mutation{

    public MutationElementStep(char modo) {
        super(modo, "MutacionElementStep");
    }
    
    @Override
    public Chromosome operate(Chromosome operando){
        Chromosome result=new Chromosome(operando);
        Domain domain;
        Gene gene;
        
        int domainIndex , geneIndex , elementIndex , x , step;
        
        Random random = new Random();
        
        geneIndex = random.nextInt(result.size()-1);
        gene = result.getGene(geneIndex);
        
        domainIndex = random.nextInt(gene.size()-1);
        domain=gene.getDomainI(domainIndex);
        
        elementIndex = random.nextInt(domain.size());
        
        x =(int) domain.getElement(elementIndex);
        
        if ( random.nextFloat() > 0.5 ){
            step=1;
        }else{
            step=-1;
        }
        if ( (x +step >= (int)domain.getLowBound()) && x+step <= (int) domain.getHighBound()){
            domain.setElement( x+step,elementIndex);
        }  else {
            domain.setElement(x-step,elementIndex);
        }
        return result;
    } 
}
