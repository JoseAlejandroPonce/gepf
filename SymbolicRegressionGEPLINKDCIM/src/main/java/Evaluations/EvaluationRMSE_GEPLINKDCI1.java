/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Evaluations;

import examples.modeling.CsvFilesHelper;
import modeling.DataHelper;
import modeling.Evaluation;
import modeling.Phenotype;
import modeling.Nucleotide;
import core.Chromosome;
import core.Gene;
import modeling.Function;
import java.util.ArrayList;
import java.util.List;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

/**
 *
 * @author José Alejandro Ponce
 */
public class EvaluationRMSE_GEPLINKDCI1 extends Evaluation {

    private static int R;

    //public static int indiceconstantes;
    public EvaluationRMSE_GEPLINKDCI1( Nucleotide[] nucleotides , float acceptableFitness , int R ) {
        super( nucleotides , acceptableFitness );
        this.R = R;
    }

    /**
     * Implementation of the evaluation method using the RMSE for the case when
     * the phenotype is a univariate mathematical expression.
     *
     * @param phenotype
     * @return
     */
    @Override
    public float evaluation( Phenotype phenotype ) {
        Float finalFitness;
        DataHelper helper = CsvFilesHelper.getInstance();

        List data = helper.getData();

        float partialFitness = 0;
        float evaluation;
        //Create the exp4j expression
        Expression e = new ExpressionBuilder( (String) phenotype.phenotype )
                .variables( "x" )
                .build();
        Float[] s;
        //Itero por cada valor de los data de entrada (X en este caso)
        for ( int i = 2 ; i < data.size() ; i++ ) {
            s = (Float[]) data.get( i );
            e.setVariable( "x" , s[0] );
            try {
                evaluation = (float) e.evaluate();
            } catch ( java.lang.ArithmeticException ee ) {
                //Note: The value is positive because it gets substracted later
                evaluation = 1000000;
            }
            //Calculate partial fitness 
            partialFitness += (float) Math.pow( s[1] - evaluation , 2 );
        }
        //Caluclate the final fitness and return it.
        finalFitness = (float) Math.sqrt( partialFitness / data.size() );
        return finalFitness;
    }

    @Override
    public Phenotype expressChromosome( Chromosome chromosomes ) {
        Phenotype fullFunction = new Function();
        Nucleotide link;
        String linkstring = "";
        String function = "";
        List<String> subFunctionsList = new ArrayList<String>();
        boolean hasLink = false;
        Integer indicetailDC;
        int[] arities;
        Integer elementAux;
        for ( int geneIndex = 0 ; geneIndex < chromosomes.size() ; geneIndex++ ) {
            if ( chromosomes.getGene( geneIndex ).getName().compareTo( "Link" ) == 0 ) {
                //extract the linking function from the "Link" gene.
                link = Evaluation.nucleotides[ (int) chromosomes.getGene( geneIndex ).getDomainI( 0 ).getElement( 0 ) ];
                linkstring = (String) link.getNucleotide();
                hasLink = true;
            }
        }
        //Now that we have the linking function lets create the expression
        for ( int geneIndex = 0 ; geneIndex < chromosomes.size() ; geneIndex++ ) {
            if ( chromosomes.getGene( geneIndex ).getName().compareTo( "GEP-GENE" ) == 0 ) {
                //Generate the aritiesArray with the cumulative arities of each nucleotide
                arities = new int[chromosomes.getGene( geneIndex ).getDomainI( 0 ).size()];
                for ( int elementIndex = 0 ; elementIndex < arities.length ; elementIndex++ ) {
                    elementAux = (int) chromosomes.getGene( geneIndex ).getDomainI( 0 ).getElement( elementIndex );
                    arities[elementIndex] = EvaluationRMSE_GEPLINKDCI1.nucleotides[ elementAux ].getArity();
                }
                for ( int elementIndex = 1 ; elementIndex < arities.length ; elementIndex++ ) {
                    arities[elementIndex] = arities[elementIndex] + arities[elementIndex - 1];
                }
                //Translate the gene and store the expression in subgunction
                indicetailDC = 0;
                subFunctionsList.add( translateGEPGENE( chromosomes.getGene( geneIndex ) , arities , 0 , indicetailDC ) );
            }
        }

        if ( hasLink ) {
            //Create the full expression using the linking function and the subfunctions
            function = function.concat( subFunctionsList.get( 0 ) );
            for ( int geneIndex = 1 ; geneIndex < subFunctionsList.size() ; geneIndex++ ) {
                function = function.concat( linkstring );
                function = function.concat( subFunctionsList.get( geneIndex ) );
            }
            fullFunction.setPhenotype( function );
        } else {
            fullFunction.setPhenotype( subFunctionsList.get( 0 ) );
        }
        return fullFunction;
    }

    public String translateGEPGENE( Gene gene , int[] arities , int elementIndex , Integer indicetailDC ) {
        if ( elementIndex >= gene.getDomainI( 0 ).size() ) {
            int aux = elementIndex - gene.getDomainI( 0 ).size();
            if ( nucleotides[ (int) gene.getElement( 1 , aux ) ].getNucleotide() == "?" ) {
                indicetailDC++;
                return "(" + gene.getElement( 3 , (int) gene.getElement( 2 , indicetailDC - 1 ) ).toString() + ")";
            }
            return (String) "(" + nucleotides[ (int) gene.getElement( 1 , aux ) ].getNucleotide().toString() + ")";
        } else {
            Nucleotide nuc = nucleotides[ (int) gene.getDomainI( 0 ).getElement( elementIndex ) ];
            int aridad = nuc.getArity();
            switch ( aridad ) {
                case 0:
                    if ( nuc.getNucleotide() == "?" ) {
                        indicetailDC++;
                        return "(" + gene.getElement( 3 , (int) gene.getElement( 2 , indicetailDC - 1 ) ).toString() + ")";
                    }
                    return (String) "(" + nuc.getNucleotide() + ")";
                case 1:
                    if ( elementIndex > 0 ) {
                        elementIndex = arities[elementIndex - 1];
                    }
                    return (String) nuc.getNucleotide() + "(" + this.translateGEPGENE( gene , arities , elementIndex + 1 , indicetailDC ) + ")";
                case 2:
                    if ( elementIndex > 0 ) {
                        elementIndex = arities[elementIndex - 1];
                    }
                    return "(" + (String) this.translateGEPGENE( gene , arities , elementIndex + 1 , indicetailDC ) + (String) nuc.getNucleotide() + this.translateGEPGENE( gene , arities , elementIndex + 2 , indicetailDC ) + ")";

                default:
                    throw new UnsupportedOperationException();
            }
        }
    }
}
