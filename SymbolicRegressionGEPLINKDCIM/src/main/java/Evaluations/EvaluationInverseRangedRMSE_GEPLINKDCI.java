/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Evaluations;

import examples.modeling.CsvFilesHelper;
import modeling.DataHelper;
import modeling.Evaluation;
import modeling.Phenotype;
import modeling.Nucleotide;
import core.Chromosome;
import core.Gene;
import modeling.Function;
import java.util.ArrayList;
import java.util.List;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

/**
 *
 * @author José Alejandro Ponce
 */
public class EvaluationInverseRangedRMSE_GEPLINKDCI extends Evaluation {

    private static int R;

    //public static int indiceconstantes;
    public EvaluationInverseRangedRMSE_GEPLINKDCI( Nucleotide[] nucleotides , float acceptableFitness , int R ) {
        super( nucleotides , acceptableFitness );
        this.R = R;
    }

    /**
     * Implementation of the evaluation method using the RMSE for the case when
     * the phenotype is a univariate mathematical expression.
     *
     * @param phenotype
     * @return
     */
    @Override
    public float evaluation( Phenotype phenotype ) {
        Float finalFitness;
        DataHelper helper = CsvFilesHelper.getInstance();

        List data = helper.getData();

        float partialFitness = 0;
        float evaluation;
        //Create the exp4j expression
        Expression e = new ExpressionBuilder( (String) phenotype.phenotype )
                .variables( "x" )
                .build();
        Float[] s;
        //Itero por cada valor de los data de entrada (X en este caso)
        for ( int i = 2 ; i < data.size() ; i++ ) {
            s = (Float[]) data.get( i );
            e.setVariable( "x" , s[0] );
            try {
                evaluation = (float) e.evaluate();
            } catch ( java.lang.ArithmeticException ee ) {
                //Note: The value is positive because it gets substracted later
                evaluation = 1000000;
            }
            //Calculate partial fitness 
            partialFitness += (float) Math.pow( s[1] - evaluation , 2 );
        }
        //Caluclate the final fitness and return it.
        finalFitness = (float) (R + 1) / (float) Math.sqrt( partialFitness / data.size() );
        return finalFitness;
    }

    @Override
    public Phenotype expressChromosome(Chromosome chromosome) {
        Phenotype func= new Function();
        Nucleotide link;
        String linkstring = "";
        String function = "";
        List<String> subfuncion = new ArrayList<String>();
        boolean tieneLink = false;
        Integer indicetailDC;
	if (chromosome.getGene(chromosome.size()-1).getName().compareTo("Link") == 0) {
		//Extract the linking function
		link = Evaluation.nucleotides[(int) chromosome.getGene(chromosome.size()-1).getDomainI(0).getElement(0)];
		linkstring = (String) link.getNucleotide();
		tieneLink = true;
	}
        for (int geneIndex = 0; geneIndex < chromosome.size(); geneIndex++) {
            if (chromosome.getGene(geneIndex).getName().compareTo("GEP-GENE") == 0) {
                int[] arities = new int[chromosome.getGene(geneIndex).getDomainI(0).size()];
                Integer elementAux;
                for (int elementIndex = 0; elementIndex < arities.length; elementIndex++) {
                    elementAux = (int) chromosome.getGene(geneIndex).getDomainI(0).getElement(elementIndex);
                    arities[elementIndex] = EvaluationInverseRangedRMSE_GEPLINKDCI.nucleotides[elementAux].getArity();
                }
                for (int elementIndex = 1; elementIndex < arities.length; elementIndex++) {
                    arities[elementIndex] = arities[elementIndex] + arities[elementIndex - 1];
                }
                indicetailDC = 0;
                subfuncion.add(translateGEPGENE(chromosome.getGene(geneIndex), arities, 0,indicetailDC ,new StringBuilder("")));
            }
        }
        if (tieneLink) {
            function = function.concat(subfuncion.get(0));
            for (int geneIndex = 1; geneIndex < subfuncion.size(); geneIndex++) {
                function = function.concat(linkstring);
                function = function.concat(subfuncion.get(geneIndex));
            }
            func.setPhenotype(function);
        } else {
            func.setPhenotype(subfuncion.get(0));
        }
        return func;
    }

    public String translateGEPGENE(Gene gene , int[] arities , int elementIndex , Integer tailDCIndex , StringBuilder sb) {
       if (elementIndex >=gene.getDomainI(0).size()){
            int aux = elementIndex - gene.getDomainI(0).size();
            if (this.nucleotides[(int) gene.getElement(1, aux)].getNucleotide()== "?"){
                tailDCIndex++;
                return sb
			.append("(")
			.append( gene.getElement(3, (int) gene.getElement(2,tailDCIndex-1)).toString() )
			.append( ")").toString();
            }
            return sb
		    .append("(")
		    .append(this.nucleotides[(int) gene.getElement(1, aux)].getNucleotide().toString() )
		    .append( ")").toString();
        }else{
            Nucleotide nuc = this.nucleotides[(int) gene.getDomainI(0).getElement(elementIndex)];
            int aridad = nuc.getArity();
            switch (aridad) {
                case 0:
                    if (nuc.getNucleotide() == "?") {
                        tailDCIndex++;
                        return sb
				.append("(")
				.append(gene.getElement(3, (int) gene.getElement(2,tailDCIndex-1)).toString())
				.append(")").toString();
                    }
                    return sb.append("(").append(nuc.getNucleotide()).append( ")").toString();
                case 1:
                    if (elementIndex > 0) {
                        elementIndex = arities[elementIndex - 1];
                    }
                    sb.append(nuc.getNucleotide());
		    sb.append("(");
	  	    this.translateGEPGENE(gene, arities, elementIndex + 1,tailDCIndex,sb);
;		    sb.append(")").toString();
		    return sb.toString();
                case 2:
                    if (elementIndex > 0) {
                        elementIndex = arities[elementIndex - 1];
                    }
                    sb.append("(");
		    this.translateGEPGENE(gene, arities, elementIndex + 1,tailDCIndex,sb);
		    sb.append(nuc.getNucleotide());
		    this.translateGEPGENE(gene, arities, elementIndex + 2,tailDCIndex,sb);
		    sb.append(")").toString();
		    return sb.toString();
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }
}
