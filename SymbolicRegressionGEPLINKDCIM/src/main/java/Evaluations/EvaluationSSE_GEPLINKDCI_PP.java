/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Evaluations;

import examples.modeling.CsvFilesHelper;
import modeling.DataHelper;
import modeling.Evaluation;
import modeling.Phenotype;
import modeling.Nucleotide;
import core.Chromosome;
import core.Gene;
import modeling.Function;
import java.util.ArrayList;
import java.util.List;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

/**
 *
 * @author alejandro
 */
public class EvaluationSSE_GEPLINKDCI_PP extends Evaluation {

    private float R;

    //public static int indiceconstantes;
    public EvaluationSSE_GEPLINKDCI_PP( Nucleotide[] nucleotides , float acceptableFitness , float range ) {
        super( nucleotides , acceptableFitness );
        this.R = range;
    }

    /**
     * Esta es la implementacion de la funcion de fitness cuando el phenotype es
     * una expresion matematica. Para este caso realiza una evaluation de la
     * expresion matematica y devuelve el result.
     *
     * @param phenotype
     * @return
     */
    @Override
    public float evaluation( Phenotype phenotype ) {
        Float fitness;
        //Primero pasamos el directorio (carpeta) 
        //Esto se usa tambien para guardar los archivos por eso debe estar separado
        DataHelper helper = CsvFilesHelper.getInstance();

        //Ahora le indicamos el nombre del archivo desde el cual cargar los data
        List data = helper.getData();

        //Creo un array para guardar los results parciales
        //float[] partialfitness= new float[data.size()];
        float partialFitness = 0;
        float evaluation;
        boolean flag;
        Float[] s;

        Expression e = new ExpressionBuilder( (String) phenotype.phenotype )
                .variables( "x" )
                .build();
        //Itero por cada valor de los data de entrada (X en este caso)
        for ( int i = 0 ; i < data.size() ; i++ ) {

            //Cargo en s el i-ezimo dato
            s = (Float[]) data.get( i );

            // creo la variable expresion de exp4j
            e.setVariable( "x" , s[0] );

            //evaluo. esto debe ir en un try catch para tomar las excepciones 
            //matematicas (ej: division por cero)
            try {

                evaluation = (float) e.evaluate();
                flag = true;

            } catch ( java.lang.ArithmeticException ee ) {

                //Nota: Este valor es positivo por que despues se le cambia el signo
                //Debido al uso de la potencia al cuadrado en los results parciales
                evaluation = 1000000000;
                flag = false;
            }

            partialFitness += (float) Math.pow( s[1] - evaluation , 2 );

        }

        //Por ultimo una vez obtenido el vector partialfitness 
        //Calculamos el valor float haciendo una suma o promedio o lo que 
        //Corresponda
        fitness = (float) R - (partialFitness + (float) 0.1*phenotype.phenotype.toString().length());
        return fitness;
    }

    @Override
    public Phenotype expressChromosome( Chromosome chromosomes ) {

        Function phenotype = new Function();
        Nucleotide link;
        String linkstring = "";
        String function = "";
        List<String> subfuncion = new ArrayList<String>();
        boolean tieneLink = false;
        for ( int geneIndex = 0 ; geneIndex < chromosomes.size() ; geneIndex++ ) {

            if ( chromosomes.getGene( geneIndex ).getName().compareTo( "Link" ) == 0 ) {

                //Extraigo de la lista de nucleotides
                //La funcion de link
                //usando como indice el unico elemento del gene con nombre "Link" 
                link = Evaluation.nucleotides[ (int) chromosomes.getGene( geneIndex ).getDomainI( 0 ).getElement( 0 ) ];
                linkstring = (String) link.getNucleotide();
                tieneLink = true;
            }
        }
        //Ahora que ya tenemos la funcion de link tenemos que traducir 
        for ( int geneIndex = 0 ; geneIndex < chromosomes.size() ; geneIndex++ ) {
            //Primero revisamos que el nombre del gene sea GEP-GEN 

            if ( chromosomes.getGene( geneIndex ).getName().compareTo( "GEP-GENE" ) == 0 ) {
                //Ahora creamos un vector con las arities del head en el gene
                //Que sera necesario para el metodo traducir gene

                //Declaramos el vector (debe hacerse aqui lamentablemente)
                int[] arities = new int[chromosomes.getGene( geneIndex ).getDomainI( 0 ).size()];
                Integer elementAux;

                //Creamos el vector
                for ( int elementIndex = 0 ; elementIndex < arities.length ; elementIndex++ ) {

                    elementAux = (int) chromosomes.getGene( geneIndex ).getDomainI( 0 ).getElement( elementIndex );
                    arities[elementIndex] = EvaluationSSE_GEPLINKDCI_PP.nucleotides[ elementAux ].getArity();
                }

                for ( int elementIndex = 1 ; elementIndex < arities.length ; elementIndex++ ) {
                    arities[elementIndex] = arities[elementIndex] + arities[elementIndex - 1];
                }

                //Very important. set TailDCIndex = 0
                Integer tailDCIndex = 0;
                subfuncion.add( translateGEPGENE( chromosomes.getGene( geneIndex ) , arities , 0 , tailDCIndex ) );
            }
        }
        if ( tieneLink ) {
            function = function.concat( subfuncion.get( 0 ) );
            for ( int geneIndex = 1 ; geneIndex < subfuncion.size() ; geneIndex++ ) {
                function = function.concat( linkstring );
                function = function.concat( subfuncion.get( geneIndex ) );
            }
            phenotype.setPhenotype( function );
        } else {
            phenotype.setPhenotype( subfuncion.get( 0 ) );
        }
        return phenotype;
    }

    public String translateGEPGENE( Gene gene , int[] arities , int elementIndex , Integer indicetailDC ) {
        if ( elementIndex >= gene.getDomainI( 0 ).size() ) {
            int aux = elementIndex - gene.getDomainI( 0 ).size();
            if ( this.nucleotides[ (int) gene.getElement( 1 , aux ) ].getNucleotide() == "?" ) {
                indicetailDC++;
                return "(" + gene.getElement( 3 , (int) gene.getElement( 2 , indicetailDC - 1 ) ).toString() + ")";
            }
            return (String) "(" + this.nucleotides[ (int) gene.getElement( 1 , aux ) ].getNucleotide() + ")";
        } else {
            Nucleotide nuc = this.nucleotides[ (int) gene.getDomainI( 0 ).getElement( elementIndex ) ];
            int aridad = nuc.getArity();
            switch ( aridad ) {
                case 0:
                    if ( nuc.getNucleotide() == "?" ) {
                        indicetailDC++;
                        return "(" + gene.getElement( 3 , (int) gene.getElement( 2 , indicetailDC - 1 ) ).toString() + ")";
                    }
                    return (String) "(" + nuc.getNucleotide() + ")";
                case 1:
                    if ( elementIndex > 0 ) {
                        elementIndex = arities[elementIndex - 1];
                    }
                    return (String) nuc.getNucleotide() + "(" + this.translateGEPGENE( gene , arities , elementIndex + 1 , indicetailDC ) + ")";
                case 2:
                    if ( elementIndex > 0 ) {
                        elementIndex = arities[elementIndex - 1];
                    }
                    return "(" + (String) this.translateGEPGENE( gene , arities , elementIndex + 1 , indicetailDC ) + (String) nuc.getNucleotide() + this.translateGEPGENE( gene , arities , elementIndex + 2 , indicetailDC ) + ")";
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }
}
