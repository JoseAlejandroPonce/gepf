/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Evaluations;

import examples.modeling.CsvFilesHelper;
import modeling.DataHelper;
import modeling.Evaluation;
import modeling.Phenotype;
import modeling.Nucleotide;
import core.Chromosome;
import core.Gene;
import modeling.Function;
import java.util.ArrayList;
import java.util.List;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

/**
 *
 * @author José Alejandro Ponce
 */
public class EvaluationSSE_GEPLINKDCI extends Evaluation {

    private static int R;

    public EvaluationSSE_GEPLINKDCI( Nucleotide[] nucleotides , float acceptableFitness , int R ) {
        super( nucleotides , acceptableFitness );
        this.R = R;
    }

    /**
     * Implementation of the evaluation method using the SSE for the case when
     * the phenotype is an univariate mathematical expression
     *
     * @param phenotype an univariate mathematical expression
     * @return Fitness value
     */
    @Override
    public float evaluation( Phenotype phenotype ) {
        Float finalFitness;
        DataHelper helper = CsvFilesHelper.getInstance();

        List data = helper.getData();
        float partialFitness = 0;
        float evaluation;
        Float[] s;
        //Create the exp4j expression
        Expression e = new ExpressionBuilder( (String) phenotype.phenotype )
                .variables( "x" )
                .build();
        for ( int i = 0 ; i < data.size() ; i++ ) {
            s = (Float[]) data.get( i );
            e.setVariable( "x" , s[0] );
            try {
                evaluation = (float) e.evaluate();
            } catch ( java.lang.ArithmeticException ee ) {
                evaluation = 10000000;
            }
            //Calculate the partial Fitness 
            partialFitness += (float) Math.pow( s[1] - evaluation , 2 );
        }
        //Calculate the final Fitness
        finalFitness = (float) R-partialFitness;
        return finalFitness;
    }

    @Override
    public Phenotype expressChromosome( Chromosome chromosomes ) {
        Phenotype func = new Function();
        Nucleotide link;
        String linkstring = "";
        String function = "";
        List<String> subfuncion = new ArrayList<String>();
        boolean tieneLink = false;
        for ( int geneIndex = 0 ; geneIndex < chromosomes.size() ; geneIndex++ ) {
            if ( chromosomes.getGene( geneIndex ).getName().compareTo( "Link" ) == 0 ) {
                //Extract the linking function
                link = Evaluation.nucleotides[ (int) chromosomes.getGene( geneIndex ).getDomainI( 0 ).getElement( 0 ) ];
                linkstring = (String) link.getNucleotide();
                tieneLink = true;
            }
        }
        //Now that we have the linking function we have to translate.
        Integer indicetailDC;
        int[] arities;
        Integer elementAux;
        for ( int geneIndex = 0 ; geneIndex < chromosomes.size() ; geneIndex++ ) {
            if ( chromosomes.getGene( geneIndex ).getName().compareTo( "GEP-GENE" ) == 0 ) {
                arities = new int[chromosomes.getGene( geneIndex ).getDomainI( 0 ).size()];
                for ( int elementIndex = 0 ; elementIndex < arities.length ; elementIndex++ ) {
                    elementAux = (int) chromosomes.getGene( geneIndex ).getDomainI( 0 ).getElement( elementIndex );
                    arities[elementIndex] = EvaluationSSE_GEPLINKDCI.nucleotides[ elementAux ].getArity();
                }
                for ( int elementIndex = 1 ; elementIndex < arities.length ; elementIndex++ ) {
                    arities[elementIndex] = arities[elementIndex] + arities[elementIndex - 1];
                }
                indicetailDC = 0;
                subfuncion.add( translateGEPGENE( chromosomes.getGene( geneIndex ) , arities , 0 , indicetailDC ) );
            }
        }

        if ( tieneLink ) {
            function = function.concat( subfuncion.get( 0 ) );
            for ( int geneIndex = 1 ; geneIndex < subfuncion.size() ; geneIndex++ ) {
                function = function.concat( linkstring );
                function = function.concat( subfuncion.get( geneIndex ) );
            }
            func.setPhenotype( function );
        } else {
            func.setPhenotype( subfuncion.get( 0 ) );
        }
        return func;
    }

    public String translateGEPGENE( Gene gene , int[] arities , int elementIndex , Integer indicetailDC ) {
        if ( elementIndex >= gene.getDomainI( 0 ).size() ) {
            int aux = elementIndex - gene.getDomainI( 0 ).size();
            if ( this.nucleotides[ (int) gene.getElement( 1 , aux ) ].getNucleotide() == "?" ) {
                indicetailDC++;
                return "(" + gene.getElement( 3 , (int) gene.getElement( 2 , indicetailDC - 1 ) ).toString() + ")";
            }
            return (String) "(" + this.nucleotides[ (int) gene.getElement( 1 , aux ) ].getNucleotide() + ")";
        } else {
            Nucleotide nuc = this.nucleotides[ (int) gene.getDomainI( 0 ).getElement( elementIndex ) ];
            int aridad = nuc.getArity();
            switch ( aridad ) {
                case 0:
                    if ( nuc.getNucleotide() == "?" ) {
                        indicetailDC++;
                        return "(" + gene.getElement( 3 , (int) gene.getElement( 2 , indicetailDC - 1 ) ).toString() + ")";
                    }
                    return (String) "(" + nuc.getNucleotide() + ")";
                case 1:
                    if ( elementIndex > 0 ) {
                        elementIndex = arities[elementIndex - 1];
                    }
                    return (String) nuc.getNucleotide() + "(" + this.translateGEPGENE( gene , arities , elementIndex + 1 , indicetailDC ) + ")";
                case 2:
                    if ( elementIndex > 0 ) {
                        elementIndex = arities[elementIndex - 1];
                    }
                    return "(" + (String) this.translateGEPGENE( gene , arities , elementIndex + 1 , indicetailDC ) + (String) nuc.getNucleotide() + this.translateGEPGENE( gene , arities , elementIndex + 2 , indicetailDC ) + ")";
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }
}
