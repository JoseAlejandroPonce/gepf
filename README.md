# README #
 
### Source code repo for development revision and version control ###

* Please read the readme (ALL OF IT)
* Version: 1.2


### How do I get set up? ###
####I like using [NetBeans](https://netbeans.org/)
But you can use any IDE you like, or none at all (if you know what you are doing of course).

####Getting the code
You can do this by using git or directly downloading the repo from bitbucket.

* Using git
    - Install [Git](https://git-scm.com/) 
    - Create a new Folder.
    - Clone the repo in the new folder with the following command.
    - git clone https://AlePonce@bitbucket.org/AlePonce/proyecto.git.

* Downloading from the repository.
    - Go to the Downloads link and then download the repo.
    - Move it to the folder of your preference. 
    
####Using the framework with NetBeans.
* Open the projects with NetBeans
    - Launch NetBeans, go to File, Open Project, Find the projects and open them.
 
* Take a look at the example projects.
    - Once you have opened the example project in NetBeans
    - All example projects are structured as it follows
    - ProjectName
         - operators package
         - modeling package
         - projectname package (this one contains the main method)
    - You can run the project by selecting the class with the main method, right clicking on it and then Run file. 
    - For further information on the packages you can check the documentation.

* The example projects are: 
    - SymbolicRegressionGEPLINKDCI
    - TimeSeries_ARmodel_GEPLINKDCI

* Create your own project.
    - You will need to Implement a few classes and methods first. you can check the documentation and the examples for further details on how to do this. 
    - The mandatory Classes are the following ones.
    - Operator: The genetic operator to be used by the algorithm (you will actually have to extend one of the child classes Mutation,Crossover or Permutation).
    - Evaluation: Contains the methods needed to express a chromosome into a phenotype, and evaluate said phenotype.
    - Phenotype: A wraper for the phenotype object (Eg. mathematical expressions,ANNs. , trees ).
    - Main method: Defines all the objects Needed by the framework. Set up an algorithm and call algorithm.execute(). 


### Contribution guidelines ###

* Don´t modify the "core" and "modeling" packages without permission and/or revision by any of the admins.
* Code review will be done by Alejandro Ponce, Adrián Jimenez, or Nicolás Majorel Padilla.
* The package "Examples" can be edited without permission but with care.

### Who do I talk to? ###

* Alejandro Ponce
* Adrian Jimenez
* Nicolas Padilla Majorel
* Adrian Will