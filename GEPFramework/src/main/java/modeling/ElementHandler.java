/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package modeling;

/**
 * Interface to handle the generation, copy and validation(check) of elements
 *
 * @author Alejandro
 * @param <Element> Type of the element to be handled (so far it can be Integer or Float)
 */
public interface ElementHandler<Element> {

    /**
     * Randomly generates an element. Method can be redefined by the user in
     * order to get better distribution or handle other types of elements.
     *
     * @param lowBound minimum value allowed for an element
     * @param highBound maximum value allowed for an element
     * @return an Element
     */
    public abstract Element generate( Element lowBound , Element highBound );

    /**
     * Validates if an element fits in a given domain. Based on the lowBound and
     * highBound
     *
     * @param element Element to be checked.
     * @param lowBound Minimum value allowed for an element
     * @param highBound Maximum value allowed for an element
     * @return boolean
     */
    public abstract boolean check( Element element , Element lowBound , Element highBound );

    /**
     * Creates a brand new Copy of the input element
     *
     * @param element element to be Copied
     * @return element
     */
    public abstract Element copy( Element element );
}
