/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package modeling;

/**
 * This class defines how all genes must be created within the algorithm.
 * 
 * @author José Alejandro Ponce
 */
public class GeneCoding {

    /**
     * Array with the names of all the domains
     */
    private String[] domainNamesArray;
    /**
     * Name of this gene coding
     */
    private String geneCodingName;
    /**
     * Array with all the high bounds for all domains
     */
    private Object[] highBoundsArray;
    /**
     * Array with all the low bounds for all domains
     */
    private Object[] lowBoundsArray;
    /**
     * Array with the sizes of all the domains
     */
    private int[] domainSizesArray;
    /**
     * Array with all the element handlers
     */
    private ElementHandler[] elementHandlersArray;

    /**
     * Returns the name of the domain in the position "Index".
     * 
     * @param index position of the 
     * @return Name of the domain
     */
    public String getDomainName( final int index ) {
        if ( index >= this.domainNamesArray.length ) {
            throw new IndexOutOfBoundsException();
        } else {
            return this.domainNamesArray[index];
        }
    }

    /**
     * Returns this geneCoding name. Useful for some operators.
     *
     * @return name of the geneCoding
     */
    public String getName() {
        return this.geneCodingName;
    }

    /**
     * Returns the Array with both bounds of the domain in the position index.
     *
     * @param index an index
     * @return
     */
    public Object[] getBounds( int index ) {
        if ( index >= this.lowBoundsArray.length || index < 0 ) {
            throw new IndexOutOfBoundsException();
        } else {
            Object[] bounds = new Object[2];
            bounds[0] = this.lowBoundsArray[index];
            bounds[1] = this.highBoundsArray[index];
            return bounds;
        }
    }

    /**
     * Returns the domain of the specific Domain in the position "index".
     *
     * @param index an index
     * @return size of the domain
     */
    public int getDomainSize( int index ) {
        if ( index >= this.domainSizesArray.length ) {
            throw new IndexOutOfBoundsException();
        } else {
            return this.domainSizesArray[index];
        }
    }

    /**
     * Constructor: creates a new geneCoding using the input parameters
     *
     * @param domainNamesArray Array containing the names of all the Domains
     * @param geneCodingName Name of this gene Coding
     * @param highBoundsArray Array with all the high bounds 
     * @param lowBoundsArray Array with all the low bounds
     * @param domainSizesArray Array with all domain sizes
     * @param elementHandlersArray Array with all the element handlers
     */
    public GeneCoding( String[] domainNamesArray , String geneCodingName , Object[] highBoundsArray , Object[] lowBoundsArray , int[] domainSizesArray, ElementHandler[] elementHandlersArray ) {
        if ( domainNamesArray == null || domainNamesArray.length == 0 ) {
            System.err.println( "geneCoding:  domainNamesArray is null or empty." );
            throw new java.lang.IllegalArgumentException();
        } else {
            for ( int i = 0 ; i < domainNamesArray.length ; i++ ) {
                if ( domainNamesArray[i] == null || domainNamesArray[i] == "" || domainNamesArray[i].isEmpty() ) {
                    System.err.println( "geneCoding: An element of domainNamesArray is null or empty." );
                    throw new java.lang.IllegalArgumentException();
                }
            }
            for ( int i = 0 ; i < domainNamesArray.length ; i++ ) {
                for ( int j = i ; j < domainNamesArray.length ; j++ ) {
                    if ( i != j && domainNamesArray[i] == domainNamesArray[j] ) {
                        System.err.println( "geneCoding: There are two domain names repepated." );
                        throw new java.lang.IllegalArgumentException();
                    }
                }
            }
        }
        if ( geneCodingName == null || geneCodingName == "" ) {
            System.err.println( "geneCoding: geneCodingName is empty or null " );
            throw new java.lang.IllegalArgumentException();
        }
        if ( highBoundsArray == null || highBoundsArray.length == 0 ) {
            System.err.println( "geneCoding: highBounds is null or empty " );
            throw new java.lang.IllegalArgumentException();
        }
        if ( lowBoundsArray == null || lowBoundsArray.length == 0 ) {
            System.err.println( "geneCoding: lowBounds is null or empty" );
            throw new java.lang.IllegalArgumentException();
        }
        if ( highBoundsArray.length != lowBoundsArray.length ) {
            System.err.println( "geneCoding: lowBounds and highBounds must have the same length" );
            throw new java.lang.IllegalArgumentException();
        }
        for (int domainIndex = 0 ; domainIndex < highBoundsArray.length ; domainIndex ++){
            if (highBoundsArray[domainIndex] instanceof Integer){
                if ((Integer) lowBoundsArray[domainIndex] > (Integer) highBoundsArray[domainIndex]){
                    System.err.println("geneCoding: limiteinferior del dominio "+domainIndex+"no debe ser mayor que limite superior");
                    throw new java.lang.IllegalArgumentException();
                } else if ((Integer) lowBoundsArray[domainIndex]<0){
                    System.err.println("geneCoding: limiteinferior del dominio "+domainIndex+"no debe ser menor que 0.");
                    throw new java.lang.IllegalArgumentException();
                }
            } else if ( highBoundsArray[domainIndex] instanceof Float){
                if ((Float) lowBoundsArray[domainIndex]>(Float)highBoundsArray[domainIndex]){
                    System.err.println("geneCoding: limiteinferior del dominio "+domainIndex+"no debe ser mayor que limite superior");
                    throw new java.lang.IllegalArgumentException();
                }
            }
        }
        if ( domainSizesArray == null || domainSizesArray.length == 0 ) {
            System.err.println( "geneCoding: tamañopartes es nulo o esta vacio" );
            throw new java.lang.IllegalArgumentException();
        }
        for ( int i = 0 ; i < domainSizesArray.length ; i++ ) {
            if ( domainSizesArray[i] <= 0 ) {
                System.err.println( "geneCoding: un elemento de tamañopartes no puede ser menor o igual que 0." );
                throw new java.lang.IllegalArgumentException();
            }
        }
        this.domainNamesArray = domainNamesArray;
        this.geneCodingName = geneCodingName;
        this.highBoundsArray = highBoundsArray;
        this.lowBoundsArray = lowBoundsArray;
        this.domainSizesArray = domainSizesArray;
        this.elementHandlersArray = elementHandlersArray;
    }
    
    /**
     * Returns the elementHandler in the position "index".
     *
     * @param index an index
     * @return elementHandler
     */
    public ElementHandler getElement_handler( int index ) {
        return this.elementHandlersArray[index];
    }
    
    /**
     * Returns the amount of domains that this geneCoding has.
     * 
     * @return domainNamesArray.length
     */
    public int size(){
        return domainNamesArray.length;
    }
}
