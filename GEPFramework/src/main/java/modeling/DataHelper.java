/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package modeling;

import analysis.Node;
import core.Population;
import java.io.File;
import java.util.List;

/**
 * Definition of a data helper, to be implemented by the user It must transform
 * whatever kind of data that is needed into a List of objects.
 *
 * @author Alejandro
 */
public abstract class DataHelper {

    /**
     * List of objects that contains the data to be used by the evaluation object
     */
    protected List<Object> data;

    /**
     * String contains the path to the working directory
     */
    private String path;

    /**
     * Name of the file from which to retrieve data
     */
    private String fileName;

    /**
     * Return the list of objects containing the data.
     *
     * @return data
     */
    public List<Object> getData() {
        return this.data;
    }

    /**
     * Sets the list of objects containing the data.
     *
     * @param dataList a list of objects (usually float numbers)
     */
    public void setData( List<Object> dataList ) {
        this.data = dataList;
    }

    /**
     * Changes the path to the working folder. It will create a new folder if
     * the input path doesn't exist.
     *
     * @param path Path to the working folder
     */
    public void setPath( String path ) {
        File dir = new File( path );
        if ( !dir.exists() ) {
            System.out.println( "Creating folder: " + dir );
            try {
                dir.mkdir();
                System.out.println( "Folder created" );
            } catch ( SecurityException se ) {
                System.out.println( "Unable to create folder." );
            }
        }
        this.path = path;
    }

    /**
     * Changes the name of the file from which to retrieve data.
     *
     * @param fileName Name to be used when retrieving data.
     */
    public void setFileName( String fileName ) {
        this.fileName = fileName;
    }

    /**
     * Return the name of the file from which the data will be retrieved
     *
     * @return fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Return the path to the working folder
     *
     * @return path
     */
    public String getPath() {
        return path;
    }

    /**
     * Saves the population into a folder.
     *
     * @param population Population to save.
     * @param fileName Name of the file where to store the population.
     */
    public abstract void savePopulation( Population population , String fileName );

    /**
     * Save the genealogical tree of a given node into a file.
     *
     * @param node a node
     */
    public abstract void saveGenealogicalTree( Node node );

}
