/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package modeling;

/**
 *
 * @author Alejandro
 */
public abstract class Phenotype {

    /**
     * Expressed chromosome (phenotype). Its an object becasue the actual type
     * must be implemented by the user and depends on the problem.
     */
    public Object phenotype;

    /**
     * Returns the phenotype
     * 
     * @return object
     */
    public Object getPhenotype() {
        return this.phenotype;
    }

    /**
     * Changes the phenotype 
     *
     * @param newPhenotype Phenotype that will replace the existing one.
     */
    public void setPhenotype( final Object newPhenotype ) {
        this.phenotype = newPhenotype;
    }

    /** 
     * Some form of visualization for the phenotype. It depends on the phenotype´s
     * actual type.
     */
    public abstract void draw();
}
