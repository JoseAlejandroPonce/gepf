/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package modeling;

/**
 * Wrapper for the nucleotides Objects.
 *
 * @author Alejandro
 */
public class Nucleotide {

    private Object nucleotide;

    private int arity;

    /**
     * Return the nucleotide object
     *
     * @return an Object (it could be a string or a tree or some object that will be
     * required in order to create the phenotype)
     */
    public Object getNucleotide() {
        return this.nucleotide;
    }

    /**
     * Return the arity of a nucleotide
     *
     * @return an Integer that represents how many sons this nucleotide can have.
     */
    public int getArity() {
        return this.arity;
    }

    /**
     * Changes the nucleotide.
     *
     * @param nucleotide a nucleotide
     */
    public void setNucleotide( final Object nucleotide ) {
        this.nucleotide = nucleotide;
    }

    /**
     * Constructor
     *
     * @param nucleotide The nucleotide's value.
     * @param arity The nucleotide's arity
     */
    public Nucleotide( final Object nucleotide , final int arity ) {
        this.nucleotide = nucleotide;
        this.arity = arity;
    }
    
}
