/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package modeling;

import core.Chromosome;

/**
 * Evaluation: defines the model of a class evaluation to be implemented by the
 * user, Implementation depends on the architecture of a chromosome and the
 * phenotype to be evaluated.
 *
 * @author Alejandro
 */
public abstract class Evaluation {

    /**
     * List of Nucleotide used to transform a chromosome into a phenotype.
     */
    public static Nucleotide[] nucleotides;

    /**
     * Flag to be used as end condition. Once the algorithm finds an acceptable
     * fitness value it must stop searching.
     */
    protected static boolean acceptableFitnessFound;

    /**
     * Minimum acceptable fitness value.
     */
    protected static float acceptableFitness;

    /**
     * Evaluation Method. returns the fitness value for a given phenotype.
     *
     * @param phenotype a phenotype
     * @return a float (the fitness of the individual)
     */
    public abstract float evaluation( Phenotype phenotype );

    /**
     * Assigns a fitness to a given chromosome. It needs to first express the
     * input chromosome into a phenotype to then evaluate that phenotype using
     * the evaluation method. After that it will assign the fitness value.
     *
     * @param chromosome a chromosome
     */
    public final void evaluate( Chromosome chromosome ) {
        assignFitness( chromosome , evaluation( expressChromosome( chromosome ) ) );
    }

    /**
     * Transforms a chromosome into a phenotype.
     *
     * @param chromosome Chromosome from which to get the phenotype.
     * @return Phenotype
     */
    public abstract Phenotype expressChromosome( Chromosome chromosome );

    private void assignFitness( Chromosome chromosome , float fitness ) {
        chromosome.setFitness( fitness );
        chromosome.setValidFitness( true );
        if ( fitness >= getAcceptableFitness() ) {
            setAcceptableSolutionFound(true);
        }
    }

    /**
     * Consturctor:
     *
     * @param nucleotides List of Nucleotide to be used when expressing the
     * chromosomes
     * @param acceptableFitness Minimum acceptable fitness value
     */
    public Evaluation( Nucleotide[] nucleotides , float acceptableFitness ) {
        this.nucleotides = nucleotides;
        setAcceptableSolutionFound( false);
        setAcceptableFitness( acceptableFitness);
    }
    
    /**
     * Returns true if an acceptable solution was found.
     *
     * @return acceptableFitnessFound
     */
    public boolean acceptableSolutionFound() {
        return acceptableFitnessFound;
    }
    
    private float getAcceptableFitness(){
	return acceptableFitness;    
    }
    
    private void setAcceptableFitness(float fitness){
	    acceptableFitness = fitness;
    }
    
    private void setAcceptableSolutionFound(boolean solutionFound){
	    acceptableFitnessFound = solutionFound;
    }
}
