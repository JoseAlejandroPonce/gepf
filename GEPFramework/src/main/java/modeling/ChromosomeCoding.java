/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package modeling;

/**
 * 
 * @author José Alejandro Ponce
 */
public class ChromosomeCoding {

    /**
     * Name of the chromosome coding
     */
    private String name;

    private GeneCoding[] geneCodingArray;

    /**
     * Returns the geneCodign of a specific gene.
     *
     * @param index Position of the geneCoding to be retrieved from the
     * geneCodingArray
     * @return a GeneCoding object
     */
    public GeneCoding getGeneCoding( final int index ) {
        if ( index < 0 ) {
            System.err.println( "CodificacionCromosoma.getCodificaciongen: indice menor que cero" );
            throw new java.lang.IndexOutOfBoundsException();
        }
        if ( index >= geneCodingArray.length ) {
            System.err.println( "CodificacionCromosoma.getCodificaciongen: indice mayor que la longitud de codificaciongenes." );
            throw new java.lang.IndexOutOfBoundsException();
        }
        return geneCodingArray[index];
    }

    /**
     * Returns the name of the ChromosomeCodign
     *
     * @return name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Returns the quantity of geneCoding in the geneCodingArray
     *
     * @return length of the geneCodingArray
     */
    public int getGenesQuantity() {
        return this.geneCodingArray.length;
    }

    /**
     * Constructor
     *
     * @param geneCoding array of geneCodings
     * @param name Name of the ChromosomeCoding
     */
    public ChromosomeCoding( final GeneCoding[] geneCoding , final String name ) {

        if ( geneCoding == null || geneCoding.length < 1 ) {
            //Error la codificacion cromosoma debe contener al menos una codificacion de gene
            System.err.println( "CodificacionCromosoma: codificaciongenes null o vacio." );
            throw new java.lang.IllegalArgumentException();
        }
        if ( name == null || name == "" ) {

            System.err.println( "CodificacionCromosoma: name is null or empty." );
            throw new java.lang.IllegalArgumentException();
        }
        for ( int i = 0 ; i < geneCoding.length ; i++ ) {
            if ( geneCoding[i] == null ) {
                System.err.println( "CodificacionCromosoma: codificaciongenes tiene un elemento null." );
                throw new java.lang.IllegalArgumentException();
            }
        }
        this.geneCodingArray = geneCoding;
        this.name = name;
    }
}
