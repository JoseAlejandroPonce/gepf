/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package core;

/**
 *
 *
 * @author alejandro
 * @param <Parameter> Type of the object that will be used as a parameter
 * @param <Return> Type of the object to be returned
 */
public abstract class Operator< Parameter, Return> {
    
    // Name of the operator
    private final String name;

    //mode of operation
    //                "c": times to operate
    //                "%": porcentual 
    //                "p": probability
    private final char mode;
    
    /**
     * Gets the name of the operator
     *
     * @return the name of the operator
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gets the mode in which the operator must work.
     *
     * @return the mode: it can be '%', by percentage, 'p' by probability, or 'c' 
     * by quantity of operations
     */
    public char getMode() {
        return this.mode;
    }
    
    public Operator(char mode,String name){
        this.mode = mode;
        this.name=name;
    }

    /**
     * Operate: Abstract method to be implemented by the user
     *
     * @param parameter See the child classes, it should be the object to be operated but it can also be
     * used to pass other useful objects or values.
     * @return See the child classes, it should be the result of the operation.
     */
    public abstract Return operate( final Parameter parameter );

}
