/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package core;

import java.util.List;

/**
 * This class defines the methods that all operators must have. It must be
 * parameterized when defining a new operator
 *
 * @author alejandro
 */
public abstract class Selection extends Operator<List<Object>,Boolean> {

    

    /**
     *
     * @param name Name of the operator.
     */
    public Selection(String name ) {
        super('%',name);
    }
    
    @Override
    //The list of objects should contain the following elements for each selection operator to work
    /**
     * The population from where to select chromosomes The population where to
     * copy the selected chromosomes The originPopulation array with parameters
     * to be used when selecting the destinationPopulation array with parameters
     * to be used when copying selected chromosomes The amount of chromosomes to
     * be selected The current generation The historial flag
     */

    //Segun se indica en poblacion destino
    public abstract Boolean operate( List<Object> argumentos );
    
    @Override
    public char getMode(){
        return '%';
    }
}
