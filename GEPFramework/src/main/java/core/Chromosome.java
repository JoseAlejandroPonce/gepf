/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package core;

import java.util.List;
import modeling.ChromosomeCoding;
import java.util.ArrayList;

/**
 * A chromosome (or also the genotype)is a collection of one or more genes that
 * can be transformed or expressed into a phenotype (which usually is a tree) to
 * then be evaluated and have a fitness value assigned. The ones with the best
 * fitness will then be selected for reproduction and their children changed by
 * random genetic operations. The chromosome has its own coding rules that
 * determine what kind of genes can it have and how many.
 *
 * @author alejandro
 */
public class Chromosome {

    //fitness: valor del fitness del cromosoma
    private float fitness;

    //Indica si el valor que se encuentra en fitness
    //es realmente el que corresponde. 
    private boolean validFitness;

    //genes: lista de genes que componen a este cromosoma
    private List<Gene> genes;

    /**
     * Gets the size of this chromosome
     *
     * @return amount of genes present in this chromosome.
     */
    public int size() {
        return this.genes.size();
    }

    /**
     * Gets the fitness value
     *
     * @return fitness: value of fitness assigned to this chromosome
     */
    public float getFitness() {
        return this.fitness;
    }

    /**
     * Sets a new Fitness value.
     *
     * @param value Fitness to be assigned to this chromosome
     */
    public void setFitness( final float value ) {
        this.fitness = value;
    }

    /**
     * Asks if the fitness value is valid. (meaning that the chromosome was not
     * changed).
     *
     * @return boolean
     */
    public boolean hasValidFitness() {
        return this.validFitness;
    }

    /**
     * Changes the value of the flag validFitness
     *
     * @param value boolean
     */
    public void setValidFitness( final boolean value ) {
        this.validFitness = value;
    }

    /**
     * Gets the gene in the position index
     *
     * @param index: To be checked
     * @return true if the index passed the check. Or false otherwise
     */
    public Gene getGene( final int index ) {
        checkIndex( index , "Cromosoma.getGen" );
        return this.genes.get( index );
    }

    /**
     * Gets the list of genes (To be used very carefully).
     *
     * @return List of genes
     */
    public List<Gene> getGenes() {
        return this.genes;
    }

    /**
     * Sets the list of genes (To be used very carefully).
     *
     * @param genes List of genes that will replace the existing one.
     */
    public void setGenes( List<Gene> genes ) {
        if (genes == null){
            System.err.println("setGenes: genes == null");
            throw new java.lang.IllegalArgumentException();
        }
        Gene existingGene,newGene;
        for (int i = 0 ; i < genes.size() ; i++){
            existingGene = this.genes.get( i );
            newGene = genes.get( i );
            if ( newGene == null){
                System.err.println("setGenes: genes.get("+i+") == null");
                throw new java.lang.IllegalArgumentException();
            }
            if ( existingGene.getName() != newGene.getName()){
                System.err.println("setGenes: The name of the Gene "+i+" doesn't match the previous one.");
                throw new java.lang.IllegalArgumentException();
            }
            if( existingGene.size() != newGene.size()){
                System.err.println("setGenes: The size of the Gene "+i+" doesn't match the previous one.");
                throw new java.lang.IllegalArgumentException();
            }
        }
        this.genes = genes;
    }

    /**
     * Sets a gene in the position "index"
     *
     * @param gene: Gene that will replace the existing one.
     * @param index: Represents the position in which to set the new gene.
     */
    public void setGen( final Gene gene , final int index ) {
        this.genes.set( index , gene );
    }

    /**
     * Gets the domain by using indexes.
     *
     * @param geneIndex Position of the Gene from which to get the domain
     * @param domainIndex Position of the domain to get.
     * @return Domain
     */
    public Domain getDomainI( final int geneIndex , final int domainIndex ) {
        checkIndex( geneIndex , "Chromosome.getDomainI (geneIndex)" );
        return this.genes.get( geneIndex ).getDomainI( domainIndex );
    }

    /**
     * Sets a new Domain using indexes.
     *
     * @param domain Domain that will replace the existing one.
     * @param geneIndex Position of the Gene in which to set the new domain.
     * @param domainIndex Position where to set the new domain.
     */
    public void setDomainI( final Domain domain , final int geneIndex , final int domainIndex ) {
        checkIndex( geneIndex , "Chromosome.setDomainI" );
        this.genes.get( geneIndex ).setDomainI( domain , domainIndex );
    }

    /**
     * Gets a domain by name.
     *
     * @param geneIndex Position of the gene from which to get the domain.
     * @param domainName name of the domain to get.
     * @return Domain
     */
    public Domain getDomain( final int geneIndex , final String domainName ) {

        checkIndex( geneIndex , "Cromosoma.getDomain" );
        return this.genes.get( geneIndex ).getDomain( domainName );
    }

    /**
     * Set a domain by name.
     *
     * @param domain Domain that will replace the existing one.
     * @param geneIndex Position of the gene where to set the new domain.
     * @param domainName Name of the domain to be replaced.
     */
    public void setDomain( final Domain domain , final int geneIndex , final String domainName ) {
        checkIndex( geneIndex , "Cromosoma.setDomain" );
        this.genes.get( geneIndex ).setDomain( domain , domainName );
    }

    /**
     * Gets an element by using indexes. (since the elements are objects you
     * will get the object rather than its value).
     *
     * @param geneIndex Position of the gene from which to get the element.
     * @param domainIndex Position of the domain from which to get the element.
     * @param elementIndex Position of the element to retrieve.
     * @return element
     */
    public Object getElement( final int geneIndex , final int domainIndex , final int elementIndex ) {

        checkIndex( geneIndex , "Cromosoma.getElement" );
        return this.genes.get( geneIndex ).getElement( domainIndex , elementIndex );
    }

    /**
     * Sets a new element.(Since the elements are objects you will be setting
     * the object rather than its value).
     *
     * @param element Element that will replace the existing one.
     * @param geneIndex Position of the gene which element will be replaced.
     * @param domainIndex Position of the domain which element will be replaced.
     * @param elementIndex Position of the element to be replaced.
     */
    public void setElement( final Object element , final int geneIndex , final int domainIndex , final int elementIndex ) {
        checkIndex( geneIndex , "Cromosoma.setElement" );
        this.genes.get( geneIndex ).setElement( element , domainIndex , elementIndex );
    }

    /**
     * Constructor. Copies the input chromosome into a brand new one.
     *
     * @param chromosome Chromosome to be copied
     */
    public Chromosome( final Chromosome chromosome ) {
        this.validFitness = false;
        this.fitness = chromosome.getFitness();
        this.genes = new ArrayList<Gene>();
        for ( int i = 0 ; i < chromosome.size() ; i++ ) {
            this.genes.add( new Gene( chromosome.getGene( i ) ) );
        }
    }

    /**
     * Contructor.
     *
     * @param chromosomeCoding Codification for the chromosome. Defining the
     * architecture of said chromosome.
     */
    public Chromosome( final ChromosomeCoding chromosomeCoding ) {
        int geneIndex;
        this.genes = new ArrayList<Gene>( chromosomeCoding.getGenesQuantity() );
        for ( geneIndex = 0 ; geneIndex < chromosomeCoding.getGenesQuantity() ; geneIndex++ ) {
            this.genes.add( geneIndex , new Gene( chromosomeCoding.getGeneCoding( geneIndex ) ) );
        }
        this.validFitness = false;
        this.fitness = 0;
    }

    /**
     * Checks if an index is within this range [0,genes.size()-1]
     *
     * @param index value to be checked
     * @param msg massage to show in case the value does not pass the check
     */
    private void checkIndex( int index , String msg ) {
        if ( index < 0 ) {
            System.err.println( msg + ": indiceGen menor que cero" );
            throw new java.lang.IndexOutOfBoundsException();
        }
        if ( index >= this.genes.size() ) {
            System.err.println( msg + ": indiceGen mayor que tamaño del cromosoma." );
            throw new java.lang.IndexOutOfBoundsException();
        }
    }

    /**
     * Transforms the chromosome into a string of strings separated by ",".
     *
     * @return a string of numbers.
     */
    public String toString() {
        String cromosomaplano = "";
        Gene geneaux;
        Domain domainAux;
        for ( int geneIndex = 0 ; geneIndex < this.size() ; geneIndex++ ) {
            geneaux = this.getGene( geneIndex );
            for ( int domainIndex = 0 ; domainIndex < geneaux.size() ; domainIndex++ ) {
                domainAux = geneaux.getDomainI( domainIndex );
                for ( int elementIndex = 0 ; elementIndex < domainAux.size() ; elementIndex++ ) {
                    cromosomaplano = cromosomaplano + "," + domainAux.getElement( elementIndex );
                }
            }
        }
        return cromosomaplano;
    }
}
