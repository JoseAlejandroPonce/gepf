/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package core;

import modeling.GeneCoding;
import java.util.ArrayList;
import java.util.List;
import modeling.ElementHandler;

/**
 * Gene a collection of Domains.
 *
 * @author alejandro
 */
public class Gene {

    private final String names;
    private List<Domain> domainsList;

    /**
     * gets the Name of the gene
     *
     * @return nombre: Nombre de la domain.
     */
    public String getName() {
        return this.names;
    }

    /**
     * Returns the size of the gene (how many domains it has)
     *
     * @return size
     */
    public int size() {

        return this.domainsList.size();
    }

    /**
     * Gets a domain in the position "index".
     *
     * @param index Position of the Domain to be retrieved.
     * @return Domain
     */
    public Domain getDomainI( final int index ) {

        return this.domainsList.get( index );
    }

    /**
     * Sets a domain by Index
     *
     * @param index Position of the domain to be replaced
     * @param domain Domain that will replace the existing one.
     */
    public void setDomainI( final Domain domain , final int index ) {

        if ( domain == null ) {

            System.err.println( "Gene.SetDomainI: the new Domain is null." );
            throw new java.lang.IllegalArgumentException();
        }
        checkPartName( domain.getName() , "Gene.setDomainI" );

        if ( getDomainI( index ).getName().compareTo( domain.getName() ) != 0 ) {
            System.err.println( "Gene.setDomainI: The new domainName is different from the previous one." );
            throw new java.lang.IllegalArgumentException();
        }
        if ( getDomainI( index ).size() != domain.size() ) {

            System.err.println( "Gene.setDomainI: The new domain has a wrong Size." );
            throw new java.lang.IllegalArgumentException();
        }
        if ( getDomainI( index ).getLowBound() instanceof Float ) {
            Float Limiteinferior = (Float) getDomainI( index ).getLowBound();
            Float LimiteSuperior = (Float) getDomainI( index ).getHighBound();
            if ( Limiteinferior.compareTo( (Float) domain.getLowBound() ) != 0
                    || LimiteSuperior.compareTo( (Float) domain.getHighBound() ) != 0 ) {

                System.err.println( "Gene.setDomainI: The new domain has wrong Bounds." );
                throw new java.lang.IllegalArgumentException();
            }
        } else if ( getDomainI( index ).getLowBound() instanceof Integer ) {
            Integer Limiteinferior = (Integer) getDomainI( index ).getLowBound();
            Integer LimiteSuperior = (Integer) getDomainI( index ).getHighBound();
            if ( Limiteinferior.compareTo( (Integer) domain.getLowBound() ) != 0
                    || LimiteSuperior.compareTo( (Integer) domain.getHighBound() ) != 0 ) {

                System.err.println( "Gene.setDomainI: The new domain has wrong Bounds." );
                throw new java.lang.IllegalArgumentException();
            }
        }
        this.domainsList.set( index , domain );
    }

    /**
     * Gets a domain by name
     *
     * @param domainName Name of the domain to be retrieved.
     * @return Domain
     */
    public Domain getDomain( final String domainName ) {
        checkPartName( domainName , "Gene.getParte" );
        for ( int i = 0 ; i < this.size() ; i++ ) {
            if ( this.getDomainI( i ).getName().compareTo( domainName ) == 0 ) {
                return this.getDomainI( i );
            }
        }
        System.err.println( "Gene: No existe ninguna domain con el nombre " + domainName );
        throw new java.lang.IllegalArgumentException();
    }

    /**
     * Sets a domain by name , the method will throw an exception if it can't
     * find a domain with the input name. The new domain must have the same
     * name.
     *
     * @param domain Domain that will replace the existing one
     * @param domainName Name of the domain that will be replaced.
     */
    public void setDomain( final Domain domain , final String domainName ) {
        checkPartName( domainName , "Gene.setParte" );
        for ( int i = 0 ; i < this.size() ; i++ ) {
            if ( getDomainI( i ).getName().compareTo( domainName ) == 0 ) {
                setDomainI( domain , i );
                return;
            }
        }
        System.err.println( "Gene: No existe ninguna domain con el nombre " + domainName );
        throw new java.lang.IllegalArgumentException();
    }

    /**
     * Gets an Element in a given domain.
     *
     * @param domainIndex Position of the domain from which to retrieve the
     * element
     * @param elementIndex Position of the element to be retrieved.
     * @return element
     */
    public Object getElement( final int domainIndex , final int elementIndex ) {
        return this.domainsList.get( domainIndex ).getElement( elementIndex );
    }

    /**
     * Sets an element in a given domain
     *
     * @param element Element that will replace the existing one.
     * @param domainIndex Position of the index
     * @param elementIndex Position of the element
     */
    public void setElement( final Object element , final int domainIndex , final int elementIndex ) {
        this.getDomainI( domainIndex ).setElement( element , elementIndex );
    }

    /**
     * Constructor: Generates a new randomly created gene.
     *
     * @param geneCoding Codification for a gene. It defines the architecture
     * for a kind of gene.
     */
    public Gene( final GeneCoding geneCoding ) {
        int domainIndex, domainSize;
        String domainName;
        List<Domain> domainsAuxList;
        int cantidadpartes = geneCoding.size();
        domainsAuxList = new ArrayList<Domain>( cantidadpartes );
        for ( domainIndex = 0 ; domainIndex < geneCoding.size() ; domainIndex++ ) {
            domainsAuxList.add( new Domain(
                    geneCoding.getDomainName( domainIndex ) ,
                    geneCoding.getDomainSize( domainIndex ) ,
                    geneCoding.getBounds( domainIndex )[0] ,
                    geneCoding.getBounds( domainIndex )[1] ,
                    geneCoding.getElement_handler( domainIndex )
                )
            );
        }
        this.domainsList = domainsAuxList;
        this.names = geneCoding.getName();
    }

    /**
     * Constructor: Copies the input gene into a brand new one.
     *
     * @param gene Gene to be copied.
     */
    public Gene( Gene gene ) {
        this.names = gene.getName();
        this.domainsList = new ArrayList<Domain>();
        for ( int indicepartes = 0 ; indicepartes < gene.size() ; indicepartes++ ) {
            this.domainsList.add( new Domain( gene.getDomainI( indicepartes ) ) );
        }
    }

    /**
     * Checks if a given domain Name is valid (empty string or null validation).
     *
     * @param domainName Name to be checked.
     * @param msg Message to be shown if the name does not pass the check.
     */
    private void checkPartName( String domainName , String msg ) {
        if ( domainName == null || domainName.isEmpty() ) {
            System.err.println( msg + ": The input domainName cannot be null or empty" );
            throw new java.lang.IllegalArgumentException();
        }
    }
}
