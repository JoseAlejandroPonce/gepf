/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package core;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Selection operator specifically designed to select elites from a regular
 * population and copy them into the respective elites population.
 *
 * @author alejandro
 */
public class SelectElites extends Selection {

    /**
     * Constructor
     */
    public SelectElites() {
        super( "SelectElites" );
    }

    @Override
    public Boolean operate( List<Object> arguments ) {

        Random random = new Random();

        Population originPop = (Population) arguments.get( 0 );
        Population destinationPop = (Population) arguments.get( 1 );
        int valor = (int) arguments.get( 4 );

        int currentGeneration = (int) arguments.get( 5 );

        boolean historial = (boolean) arguments.get( 6 );
        //Make a copy of the chromosomesList from the originPopulation.
        ArrayList<Chromosome> chromosomes = new ArrayList<Chromosome>( originPop.getchromosomes() );

        List<Chromosome> result = new ArrayList<Chromosome>();

        //Sort the copy in a descending order (this will not sort the original list)
        chromosomes.sort( new ChromosomeComparator() );

        if ( destinationPop.size() < result.size() ) {
            System.err.println( "SeleccionElites: El tamaño de los seleccionados es superior al de la poblacion destino" );
        }
        //destinationPop: poblacion de elites
        //ci: chromosome index
        int ci = 0;
        //For each elite I must compare against the regular population to know if
        //there is a better chromosome.
        for ( int i_elite = 0 ; i_elite < destinationPop.size() ; i_elite++ ) {
            ci = 0;
            while ( ci < chromosomes.size() ) {
                if ( destinationPop.getChromosome( i_elite ).getFitness() < chromosomes.get( ci ).getFitness() ) {
                    //Replce the elite with a copy of the new elite.
                    destinationPop.setChromosome( new Chromosome( chromosomes.get( ci ) ) , i_elite );
                    ci++;
                    break;
                }
                ci++;
            }
        }
        return true;
    }

}
