/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package core;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import modeling.ElementHandler;

/**
 * Domain of the gene.
 *
 * @author alejandro
 * @param <Element> an element
 */
public class Domain<Element> {

    private String name;
    private Element lowBound;
    private Element highBound;
    private ElementHandler elementHandler;
    private List<Element> elements;

    /**
     * Gets the name of the Domain
     *
     * @return name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gets the size of the Domain (how many elements it has).
     *
     * @return size
     */
    public int size() {
        return this.elements.size();
    }

    /**
     * Gets the low boundary
     *
     * @return lowBound
     */
    public Element getLowBound() {
        return this.lowBound;
    }

    /**
     * gets the High bound.
     *
     * @return Element
     */
    public Element getHighBound() {

        return this.highBound;
    }

    /**
     * Gets the element in the position "index".
     *
     * @param index Position of the element to be retrieved
     * @return element
     */
    public Element getElement( final int index ) {
        if ( index < 0 || index >= size() ) {
            System.err.println( "Domain.getElement, Index less than zero or greater than the domain size" );
            throw new java.lang.IndexOutOfBoundsException();
        }
        return this.elements.get( index );
    }

    /**
     * Replaces the element in the position "index" with the new one.
     *
     * @param element Element that will replace the existing one
     * @param index position of the element to be replaced
     */
    public void setElement( Element element , int index ) {
        checkElement( element , "setElement:" );
        this.elements.set( index , element );
    }

    /**
     * Verifies if the Value of the element is within the Domain Boundaries.
     *
     * @param element Element to verify.
     * @param msg Message to show in case the element does not pass the check.
     */
    public void checkElement( Element element , String msg ) {
        if ( !elementHandler.check( element , lowBound , highBound ) ) {
            System.err.println( "The element" + element + "doesn't meet the requirements" );
            throw new java.lang.IllegalArgumentException();
        }
    }

    /**
     * Returns a randomly generated element. The method must be first
     * implemented in the elementHandler interface´s implementation
     *
     * @return element
     */
    public Element generateElement() {
        return (Element) this.elementHandler.generate( lowBound , highBound );
    }

    /**
     * Constructor, generates random elements for the domain.
     *
     * @param name Name of the domain
     * @param domainSize Size of the domain
     * @param lowBound Minimum value allowed for an element
     * @param highBound Maximum value allowed for an element
     * @param elementHandler Object responsible for the generation checking and
     * copy of elements.
     */
    public Domain( String name , int domainSize , Element lowBound , Element highBound , ElementHandler elementHandler ) {
        if ( domainSize <= 0 ) {
            System.err.println( "Domain: a domain cannot have a size lesser than or equal to zero." );
            throw new java.lang.IllegalArgumentException();
        }
        if (name == null || name == ""){
            System.err.println("Domain: A Domain cannot have a null or empty name" );
            throw new java.lang.IllegalArgumentException();
        }
        this.name = name;
        Random random = new Random();
        int i;
        Element randomaux;
        ArrayList<Element> elementsx = new ArrayList<>( 0 );
        this.lowBound = lowBound;
        this.highBound = highBound;
        this.elementHandler = elementHandler;
        for ( i = 0 ; i < domainSize ; i++ ) {
            randomaux = (Element) elementHandler.generate( lowBound , highBound );
            checkElement( randomaux , "Constructor1" );
            elementsx.add( randomaux );
        }
        this.elements = elementsx;
    }

    /**
     * Constructor: copies the input domain into a brand new one.
     *
     * @param domain Domain to be copied.
     */
    public Domain( Domain domain ) {
        if ( domain == null ) {
            System.err.println( "Domain: The constructor received a null Domain." );
            throw new java.lang.IllegalArgumentException();
        }
        this.name = domain.getName();
        this.lowBound = (Element) domain.getLowBound();
        this.highBound = (Element) domain.getHighBound();
        this.elementHandler = domain.getElementHandler();
        this.elements = new ArrayList<Element>();
        for ( int i = 0 ; i < domain.size() ; i++ ) {
            this.elements.add( (Element) elementHandler.copy( (Element) domain.getElement( i ) ) );
        }
    }

    /**
     * Gets the element handler. To be used by operators.
     *
     * @return elementHandler
     */
    public ElementHandler getElementHandler() {
        return this.elementHandler;
    }

    /**
     * Gets a copy of an element.
     *
     * @param element Element to be copied
     * @return element copy of the input element.
     */
    public Element copy( Element element ) {
        return (Element) this.elementHandler.copy( element );
    }
}
