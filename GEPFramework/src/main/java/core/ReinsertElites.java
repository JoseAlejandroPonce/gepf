/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package core;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Selection operator specifically designed to reinsert elites back into their
 * original population.
 *
 * @author alejandro
 */
public class ReinsertElites extends Selection {

    /**
     * Constructor
     */
    public ReinsertElites() {
        super( "SeleccionReplicarElites" );
    }

    @Override
    public Boolean operate( List<Object> arguments ) {

        Random random = new Random();

        Population originPopulation = (Population) arguments.get( 0 );
        Population destinatioPopulation = (Population) arguments.get( 1 );

        int[] destinationPopulationArray = (int[]) arguments.get( 3 );

        int valor = (int) arguments.get( 4 );

        int generacionactual = (int) arguments.get( 5 );

        boolean historial = (boolean) arguments.get( 6 );

        //Copy of the Chromosomes list in the originPopulation
        ArrayList<Chromosome> chromosomes = new ArrayList<Chromosome>( originPopulation.getchromosomes() );
        Chromosome auxiliar;

        //cid = chromosome index destination
        int cid;

        for ( int i_elite = 0 ; i_elite < originPopulation.size() ; i_elite++ ) {
            //Real copy of the chose chromosome.
            auxiliar = new Chromosome( originPopulation.getChromosome( i_elite ) );
            // Try not to replace a chromosome that is better than the elite.
            for ( int i_crom = 0 ; i_crom < destinatioPopulation.size() ; i_crom++ ) {
                //generate a random index.
                cid = random.nextInt( destinationPopulationArray[2] - destinationPopulationArray[1] + 1 ) + destinationPopulationArray[1];
                if ( auxiliar.getFitness() >= destinatioPopulation.getChromosome( cid ).getFitness() ) {
                    destinatioPopulation.setChromosome( auxiliar , cid );
                }
            }
        }
        return true;
    }
}
