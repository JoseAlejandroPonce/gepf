/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package core;

/**
 * Mutation operators should implement this abstract class
 *
 * @author alejandro
 */
public abstract class Mutation extends Operator<Chromosome,Chromosome> {
    
    @Override
    public abstract Chromosome operate( Chromosome chromosome );

    /**
     *
     * @param mode Mode of operation. %, by percentage of operated chromosomes.
     * p, by probability of operation. c, by quantity of operations
     * @param name Name of the mutation.
     */
    public Mutation( char mode , String name ) {
        super(mode,name);
    }
    
}
