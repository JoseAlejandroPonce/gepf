/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package core;

import java.util.List;
import analysis.HistoricalRecord;
import modeling.ChromosomeCoding;
import java.util.ArrayList;

/**
 * Population: Collection of chromosomes
 *
 * @author alejandro
 */
public class Population {

    /**
     * List of chromosomes, containing the "individuals" of this population.
     */
    private List<Chromosome> chromosomes;

    /**
     * Data structure to store information about the use of operators.
     */
    private final HistoricalRecord historicalRecord;

    /**
     * Codification for the chromosome, Defines the chromosome architecture how
     * many genes and what kind of genes.
     */
    private ChromosomeCoding chromosomeCoding;

    /**
     * Fills the population with randomly generated chromosomes that respect the
     * chromosomeCoding.
     *
     * @param size Size of the population.
     */
    public void initialize( int size ) {
        if ( size < 0 ) {
            System.err.println( "Poblaciones.Inicializar: El tamaño no puede ser menor que cero" );
            throw new java.lang.IllegalArgumentException();
        }
        this.chromosomes = new ArrayList<Chromosome>();
        for ( int ci = 0 ; ci < size ; ci++ ) {
            this.chromosomes.add( new Chromosome( this.chromosomeCoding ) );
        }
    }

    /**
     * Returns the size of the population
     *
     * @return size
     */
    public int size() { 
        return this.chromosomes.size();
    }

    /**
     * Gets the List of chromosomes (to be used very carefully).
     *
     * @return chromosomes
     */
    public List<Chromosome> getchromosomes() {
        return this.chromosomes;
    }

    /**
     * Sets the list of chromosomes (To be used very VERY CAREFULLY).
     *
     * @param chromosomes a list of chromosomes
     */
    public void setchromosomes( final List<Chromosome> chromosomes ) {

        if ( chromosomes == null ) {

            System.err.println( "Poblaciones.setchromosomes: Se ingreso un objeto null" );
            throw new java.lang.IllegalArgumentException();
        }
        if ( chromosomes.isEmpty() ) {

            System.err.println( "Poblaciones.setchromosomes: Se ingreso un objeto vacio null" );
            throw new java.lang.IllegalArgumentException();
        }
        if ( chromosomes.size() != this.chromosomes.size() ) {

            System.err.println( "Poblaciones.setchromosomes: Los tamaños de la lista ingresada y la lista en poblacion no coinciden" );
            throw new java.lang.IllegalArgumentException();
        }
        for ( int i = 0 ; i < chromosomes.size() ; i++ ) {

            if ( chromosomes.get( i ) == null ) {

                System.err.println( "Poblaciones.setchromosomes: There is a Null Chromosome." );
                throw new java.lang.IllegalArgumentException();
            }
            if ( chromosomes.get( i ).size() != this.chromosomes.get( i ).size() ) {

                System.err.println( "Poblaciones.setchromosomes: Un elemento de la nueva lista tiene tamaño distinto al que ocupa su lugar actualmente, y no deberia." );
                throw new java.lang.IllegalArgumentException();
            }
        }
        this.chromosomes = chromosomes;
    }

    /**
     * Gets a Chromosome.
     *
     * @param chromosomeindex Position of the chromosome to be retrieved.
     *
     * @return chromosome
     */
    public Chromosome getChromosome( final int chromosomeindex ) {
        checkIndice( chromosomeindex , "Poblaciones.getCromosoma" );
        return this.chromosomes.get( chromosomeindex );
    }

    /**
     * Sets a new chromosome in a given position
     *
     * @param chromosome Chromosome that will replace the existing one.
     * @param chromosomeIndex Position of the chromosome to be replaced.
     */
    public void setChromosome( final Chromosome chromosome , final int chromosomeIndex ) {
        if ( chromosome == null || chromosome.getGenes().isEmpty() ) {
            System.err.println( "Population.setChromosome: The input chromosome is null or empty." );
            throw new java.lang.IllegalArgumentException();
        }
	if (chromosomeIndex > size() || chromosomeIndex < 0){
            System.err.println( "Population.setChromosome: The input chromosomeIndex Is out of bounds." );
            throw new java.lang.IndexOutOfBoundsException();
        }
	if ( chromosome.size() != this.getChromosome( chromosomeIndex ).size()) {
            System.err.println( "Population.setChromosome: The input chromosome has a wrong size." );
            throw new java.lang.IllegalArgumentException();
        }
	
        this.chromosomes.set( chromosomeIndex , chromosome );
    }

    /**
     * Gets a gene from given Chromosome position, and gene position
     *
     * @param chromosomeindex Position of the chromosome from which to retrieve
     * the gene.
     * @param geneIndex Position of the gene to be retrieved
     * @return gene
     */
    public Gene getGen( final int chromosomeindex , final int geneIndex ) {
        checkIndice( chromosomeindex , "Population.getGene" );
        return getChromosome( chromosomeindex ).getGene( geneIndex );
    }

    /**
     * Sets a new gene in given Chromosome position,gene position
     *
     * @param chromosomeindex Position of the chromosome in which to replace a
     * gene.
     * @param geneIndex Position of the gene to be replaced.
     * @param gene Gene that will replace the existing one.
     */
    public void setGen( final int chromosomeindex , final int geneIndex , final Gene gene ) {
        checkIndice( chromosomeindex , "Population.setGene" );
        getChromosome( chromosomeindex ).setGen( gene , geneIndex );
    }

    /**
     * Gets a domain from a given Chromosome position, Gene position, and Domain
     * position.
     *
     * @param chromosomeindex Position of the chromosome from which to retrieve
     * a gene.
     * @param geneIndex Position of the gene from which to retrieve a domain.
     * @param domainName Name of the domain to be retrieved.
     * @return Domain
     */
    public Domain getDomain( final int chromosomeindex , final int geneIndex , final String domainName ) {
        checkIndice( chromosomeindex , "Population.getDomain" );
        return this.chromosomes.get( chromosomeindex ).getGene( geneIndex ).getDomain( domainName );
    }

    /**
     * Sets a new Domain in a given chromosome position,gene position, and
     * domain position.
     *
     * @param newDomain Domain that will replace the existing one.
     * @param chromosomeindex Position of the chromosome in which to replace a
     * domain.
     * @param geneIndex Position of the gene in which to replace a domain.
     * @param domainName Name of the domain to be replaced.
     */
    public void setDomain( final Domain newDomain , final int chromosomeindex , final int geneIndex , final String domainName ) {
        checkIndice( chromosomeindex , "Poopulation.setDomain" );
        this.chromosomes.get( chromosomeindex ).setDomain( newDomain , geneIndex , domainName );
    }

    /**
     * gets the domain in a given chromosome position, gene position, and domain
     * position.
     *
     * @param chromosomeindex Position of the chromosome from which to retrieve
     * a domain
     * @param geneIndex Position of the gene from which to retrieve a domain
     * @param domainIndex Position of the domain to be retrieved.
     * @return domain
     */
    public Domain getDomainI( final int chromosomeindex , final int geneIndex , final int domainIndex ) {

        checkIndice( chromosomeindex , "Population.getDomainI" );
        return chromosomes.get( chromosomeindex ).getDomainI( geneIndex , domainIndex );
    }

    /**
     * Sets a domain in a given chromosome position,gene position, and domain
     * position.
     *
     * @param newDomain Domain that will replace the existing one.
     * @param chromosomeindex Position of the chromosome in which to replace a
     * domain.
     * @param geneIndex Position of the gene in which to replace a domain.
     * @param domainIndex Name of the domain to be replaced.
     */
    public void setDomainI( final Domain newDomain , final int chromosomeindex , final int geneIndex , final int domainIndex ) {
        checkIndice( chromosomeindex , "Population.setDomainI" );
        this.chromosomes.get( chromosomeindex ).setDomainI( newDomain , geneIndex , domainIndex );
    }

    /**
     * Gets an element
     *
     * @param chromosomeindex Position of the chromosome from which to retrieve
     * an element.
     * @param geneIndex Position of the gene from which to retrieve an element.
     * @param domainIndex Position of the domain from which to retrieve an
     * element.
     * @param elementIndex Position of the element to be retrieved.
     * @return element
     */
    public Object getElement( final int chromosomeindex , final int geneIndex , final int domainIndex , final int elementIndex ) {
        checkIndice( chromosomeindex , "Population.getElement" );
        return this.chromosomes.get( chromosomeindex ).getElement( geneIndex , domainIndex , elementIndex );
    }

    /**
     * Sets an element
     *
     * @param elemento Element that will replace the existing one.
     * @param chromosomeindex Position of the chromosome in which to replace the
     * element.
     * @param geneIndex Position of the gene in which to replace the element.
     * @param domainIndex Position of the domain in which to replace the element
     * @param elementIndex Position of the element to be replaced.
     */
    public void setElement( int elemento , final int chromosomeindex , final int geneIndex , final int domainIndex , final int elementIndex ) {
        this.chromosomes.get( chromosomeindex ).getGene( geneIndex ).getDomainI( domainIndex ).setElement( elementIndex , elemento );
    }

    /**
     * Gets the HistoricalRecord of this population
     *
     * @return HistoricalRecord
     */
    public HistoricalRecord getHistoricalRecord() {
        return this.historicalRecord;
    }

    /**
     * Gets the chromosomeCoding used in this population.
     *
     * @return chromosomeCoding
     */
    public ChromosomeCoding getChromosomeCoding() {
        return this.chromosomeCoding;
    }

    /**
     * Constructor: generates a population.
     *
     * @param size Size of the population
     * @param populationNumber Position of the population.
     * @param chromosomeCoding ChromosomeCoding definition of the chromosome
     * architecture.
     */
    public Population( final int size , int populationNumber , ChromosomeCoding chromosomeCoding ) {
        this.chromosomes = new ArrayList<Chromosome>();
        this.historicalRecord = new HistoricalRecord( size , populationNumber );
        this.chromosomeCoding = chromosomeCoding;

        this.initialize( size );
    }

    /**
     * Constructor: Copies a population into a brand new one.
     *
     * @param population a population
     */
    public Population( Population population ) {
        this.chromosomes = new ArrayList<Chromosome>( population.getchromosomes() );
        this.chromosomeCoding = population.chromosomeCoding;
        this.historicalRecord = new HistoricalRecord( population.size() , population.getHistoricalRecord().getPopulationNumber() );
    }

    /**
     * Checks if an index is within [0, population size]
     *
     * @param index value to be checked.
     * @param msg Message to show if the index does not pass the check.
     */
    private void checkIndice( int index , String msg ) {
        if ( index < 0 ) {
            System.err.println( msg + ": Index < 0" );
            throw new java.lang.IndexOutOfBoundsException();
        }
        if ( index >= size() ) {
            System.err.println( msg + ": Index >= populationSize." );
            throw new java.lang.IndexOutOfBoundsException();
        }
    }

}
