/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package core;

import analysis.Analyzer;
import analysis.Node;
import modeling.DataHelper;
import modeling.Evaluation;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Main flow of the GEP algorithm
 *
 * @author alejandro
 */
public class gepAlgorithm extends Algorithm{

    private boolean massextintion;

    /** Name of the algorithm. */
    protected final String algorithmName;

    /** Array containing the size of all the populations to be used. */
    private int[] populationSizes;

    /**
     * Bidimensional array of indexes to be used when choosing chromosomes for
     * an operator.
     *
     * - row1: Origin population - row2: Position of the First usable chromosome
     * - row3: Position of the Last usable chromosome
     */
    private int[][] originPopulation;

    /**
     * Bidimensional array of indexes to be used when choosing the destination
     * of a chromosome resulting of an operation.
     *
     * -row1: Destination population -row2: First usable position -row3: Last
     * usable position
     */
    public int[][] destinationPopulation;

    /**
     * Unidimensional array of floats containing the values to be used when
     * deciding "how much" to operate. The interpretation depends on the operand
     * and the mode.
     */
    private float[] values;

    /** Integer representing the quantity of populations. */
    private final int populationsQuantity;

    /** Array containing populations. */
    private Population[] populations;

    /**
     * Array containing the size for each elites population.
     */
    private int[] elitesSizes;

    /**
     * Array that contains the elites populations.
     */
    private Population[] elites;

    /**
     * Integer , that represents the limit of generations that this algorithm
     * can use.
     */
    private int generations;

    /**
     * List of Operators
     */
    private List<Operator> operators = new ArrayList<Operator>();

    /**
     * Evaluation object that will assign a fitness to the chromosome
     */
    public Evaluation evaluation;

    /**
     * Random numbers generator.
     */
    private Random random;

    /**
     * Data helper. Needed to read/write files or handling data in general.
     */
    private DataHelper datahelper;

    /**
     * Flag, if its true the algorithm will show progress by console. If not It
     * will only show the last result. Made this way to improve performance.
     */
    private boolean debug;

    /**
     * Flag, if its true the algorithm will use the HistoricalRecord to gather data
 through the run.
     */
    private boolean useHistoricalRecord;
    
    /**
     * Flag, if its true the algorithm will show the Patrilineal branch by console.
     */
    private boolean showPatrilinealBranch;
    
    /**
     * Flag, if its true the algorithm will paralelize the Evaluation of the fitness.
     */
    private boolean parallelizeFitnessEvaluation;
	
	private Analyzer analyzer;

    /**
     * This method executes the main flow of the algorithm.
     */
	@Override
    public void execute() {
        /**
         * i: general puropse indice, currentGeneration: int representing which
         * generation is the current one. oi: operators index used to run
         * through the array of operators ci: chromosomes index geneIndex: genes index
         * cid chromosome index destination gid gene index destination ep:
         * evaluable population ci1 indice de chromosomes 1 (para cruzamientos)
         * cid1 indice de chromosomes destino 1 (para cruzamientos)
         *
         */
        random = new Random();
        savePopulations( "InitialPopulation" );
        System.out.println( "Starting execution \n" );
        evaluate();
        selectElites( 0 );

        for ( int currentGeneration = 0 ; currentGeneration < generations ; currentGeneration++ ) {
            /**
             * Except for the generation 0 each new generation requires a new
             * generation to be generated in the Historial too
             */
            if ( currentGeneration >= 1 && useHistoricalRecord ) {
                for ( int poi = 0 ; poi < this.populations.length ; poi++ ) {
                    this.populations[poi].getHistoricalRecord().addNewGeneration();
                    this.elites[poi].getHistoricalRecord().addNewGeneration();
                }
            }
            evaluate();
            reinsertElites( currentGeneration );
            selectElites( currentGeneration );
            if ( debug || currentGeneration == generations - 1 ) {
                System.out.printf( "Generation: %d\n" , currentGeneration );
                showElites();
            }
            if ( termination_condition() ) {
                break;
            }
            operate( currentGeneration );
        }

        savePopulations( "EndPopulation" );
        if ( useHistoricalRecord ) {
            analyzeHistoricalRecord( );
        }
    }

	private void analyzeHistoricalRecord(){
		Float bestFitness;
		for (int i = 0;i<populations.length;i++){
			bestFitness = this.elites[i].getChromosome( 0 ).getFitness();
			analyzer.analyzeTheHistoricalRecord(populations[i], operators, bestFitness);
		}
	}


    /**
     * Shows fitness and the phenotype of the elites.
     */
    private void showElites() {
        List<Chromosome> auxiliar;
        for ( int i_elites = 0 ; i_elites < this.elites.length ; i_elites++ ) {
            auxiliar = new ArrayList<>( this.elites[i_elites].getchromosomes() );
            auxiliar.sort( new ChromosomeComparator() );
            System.out.printf( "Best %d of the population %d: \n" , auxiliar.size() , i_elites );
            for ( int i = 0 ; i < auxiliar.size() ; i++ ) {
                System.out.print( " " + auxiliar.get( i ).getFitness() + "," );
                System.out.print( evaluation.expressChromosome( auxiliar.get( i ) ).phenotype.toString() );
            }
            System.out.print( "\n" );
        }
        for ( Population elite : this.elites ) {
            for ( int i_elite = 0 ; i_elite < elite.size() ; i_elite++ ) {
                evaluation.expressChromosome( elite.getChromosome( i_elite ) ).draw();
            }
        }
    }

    /**
     * Verify if the end condition is reached.
     *
     * @return true if the condition was reached, or false
     */
    private boolean termination_condition() {
        //If there is a fitness good enough it will finish the run.
        return evaluation.acceptableSolutionFound();
    }

    /**
     * Evaulates all evaluable populations
     */
    private void evaluate() {
        Chromosome chrom;
        //Recorremos las populations para evaluarlas
        if(parallelizeFitnessEvaluation){
            for ( Population pop_evaluable : this.populations ) {
                pop_evaluable.getchromosomes().parallelStream().forEach( cromosoma -> evaluation.evaluate( cromosoma ) );
            }
        }else{
            for (Population population : this.populations){
                for (int chromIndex = 0; chromIndex < population.size() ; chromIndex++) {
                    chrom = population.getChromosome( chromIndex );
                    if(chrom.hasValidFitness()){
                        this.evaluation.evaluate( chrom );
                    }
                }
            }
        }
    }

    /**
     * Selects individuals from a population and inserts them in other
     * population (it can reinsert in the same population too).
     *
     * @param operatorIndex Represents the position of the operator in the
     * operators list
     * @param currentGeneration Represents the current generation. Needed for
     * the historial.
     */
    private void select( int operatorIndex , int currentGeneration ) {

        /**
         * The list of parameters must contain the following elements origin:
         * The population from which chromosomes will be selected destination:
         * The population to which the chromosomes will be inserted
         * originPopulation: parameters with the indexes to use
         * destinationPopulation: parameters with the indexes to use quantity of
         * chromosomes to select historial: the flag that indicates if it should
         * add nodes or not
         */
        List<Object> parameters = new ArrayList<>();

        //Cargamos las populations origen y destino involucradas 
        parameters.add( this.populations[this.originPopulation[0][operatorIndex]] );
        parameters.add( this.populations[this.destinationPopulation[0][operatorIndex]] );

        /**
         * Just to remember originPopulation was the array with the indexes to
         * use. 0. Position of the origin population 1. First selectable
         * chromosome 2. Last selectable chromosome
         */
        int[] auxOriginPopulationArray = new int[3], auxDestinationPopulationArray = new int[3];
        for ( int in = 0 ; in < 3 ; in++ ) {
            auxOriginPopulationArray[in] = this.originPopulation[in][operatorIndex];
            auxDestinationPopulationArray[in] = this.destinationPopulation[in][operatorIndex];
        }

        //Now we pass the amount of chromosomes to be selected
        float amountOfChromosomesToSelect = (float) this.values[operatorIndex];
        parameters.add( auxOriginPopulationArray );
        parameters.add( auxDestinationPopulationArray );
        parameters.add( amountOfChromosomesToSelect );
        parameters.add( currentGeneration );

        //we need to add the flag "historial" so that the 
        parameters.add( useHistoricalRecord );
        //Finally we pass the list of parameters to the selection operator
        if ( (boolean) this.operators.get( operatorIndex ).operate( parameters ) != true ) {
            System.err.println( "Error within the selection operator: "
                    + this.operators.get( operatorIndex ).getName() );
        }
    }

    /**
     * Applies mutation operator on a chromosome randomly selected, and places
     * the resulting chromosome in one of two options, 1) Replacing the original
     * one (if the originPopulation and DestinationPopulation are the same) 2)
     * In a random position based on the DestinationPopulation array
     *
     * @param operatorIndex indice del operador a aplicar
     * @param currentGeneration Represents the current generation. Needed for
     * the historial
     */
    private void mutate( int operatorIndex , int currentGeneration ) {
        int mi, ci, cid;
        int max = 0; //cantidad de veces que itero
        float p = (float) 1.1; //probabilidad de aplicar operador sobre un individuo

        Population pop = this.populations[this.originPopulation[0][operatorIndex]];
        int pos_inic_crom = this.originPopulation[1][operatorIndex];
        int cant_crom = this.originPopulation[2][operatorIndex] - pos_inic_crom;

        if ( this.operators.get( operatorIndex ).getMode() == 'c' ) {
            max = (int) this.values[operatorIndex];
        } else if ( this.operators.get( operatorIndex ).getMode() == '%' ) {
            max = (int) (this.values[operatorIndex] * cant_crom);
        } else if ( this.operators.get( operatorIndex ).getMode() == 'p' ) {
            max = pop.size();
            p = this.values[operatorIndex];
        }

        for ( mi = 0 ; mi < max ; mi++ ) {
            //Si tiro una moneda y sale aplico operador a ese cromosoma
            if ( p <= 1 && random.nextFloat() < p ) {
                ci = mi;
                cid = mi;
            } else {
                //Generamos indice para el cromosoma "origen" (operando)
                ci = random.nextInt( cant_crom ) + pos_inic_crom;
                //indice para el cromosoma destino 
                //Se usan los mismos indices para gene y domain 
                //Dado que todos los chromosomes tienen la misma codificacion
                //Si originPopulation=destinationPopulation (incluyendo rangos),
                //el hijo reemplaza al padre
                if ( this.destinationPopulation[0][operatorIndex] == this.originPopulation[0][operatorIndex]
                        && this.destinationPopulation[1][operatorIndex] == this.originPopulation[1][operatorIndex]
                        && this.destinationPopulation[2][operatorIndex] == this.originPopulation[2][operatorIndex] ) {
                    cid = ci;
                } else {
                    cid = random.nextInt( cant_crom ) + this.destinationPopulation[1][operatorIndex];
                }
            }

            //aplicamos el operador (esto opera y guarda el result donde corresponda
            int ci_aux = cid;
            /**
             * Now we must make a difference between 2 cases. if the origen and
             * destination are the same then we replace the original chromosome
             * with the result of being operated. otherwise we insert the
             * result at a random place
             */
            if ( getOriginPopulation( operatorIndex ) == getDestinationPopulation( operatorIndex ) ) {
                ci_aux = ci;
            }
            Population pop_o = this.populations[this.originPopulation[0][operatorIndex]];
            Population pop_d = this.populations[this.destinationPopulation[0][operatorIndex]];
            pop_d.setChromosome( (Chromosome) this.operators.get( operatorIndex ).operate(
                    pop_o.getChromosome( ci ) ) ,
                    ci_aux );
            pop_d.getChromosome( ci_aux ).setValidFitness( false );
            if ( useHistoricalRecord ) {
                addNodeByOperator( operatorIndex , ci , cid , currentGeneration );
            }

        }
    }

    /**
     * Applies permutation operator on a chromosome randomly selected, and
     * places the resulting chromosome in one of two options, 1) Replacing the
     * original one (if the originPopulation and DestinationPopulation are the
     * same) 2) In a random position based on the DestinationPopulation array
     *
     * @param operatorIndex Represents the position of the operator in the
     * operators list
     * @param currentGeneration Represents the current generation. Needed for
     * the historial
     */
    private void permutate( int operatorIndex , int currentGeneration ) {
        int mi, ci, cid;
        int max = 0; //cantidad de veces que itero
        float p = (float) 1.1; //probabilidad de aplicar operador sobre un individuo
        Population pop_d,pop_o;
        Population pop = this.populations[this.originPopulation[0][operatorIndex]];
        int pos_inic_crom = this.originPopulation[1][operatorIndex];
        int cant_crom = this.originPopulation[2][operatorIndex] - pos_inic_crom;

        if ( this.operators.get( operatorIndex ).getMode() == 'c' ) {
            max = (int) this.values[operatorIndex];
        } else if ( this.operators.get( operatorIndex ).getMode() == '%' ) {
            max = (int) (this.values[operatorIndex] * cant_crom);
        } else if ( this.operators.get( operatorIndex ).getMode() == 'p' ) {
            max = pop.getchromosomes().size();
            p = this.values[operatorIndex];
        }

        for ( mi = 0 ; mi < max ; mi++ ) {
            //si tiro una moneda y sale aplico permutacion a ese cromosoma
            if ( p <= 1 && random.nextFloat() > p ) {
                ci = mi;
                cid = mi;
            } else {
                ci = random.nextInt( cant_crom ) + pos_inic_crom;
                if ( this.destinationPopulation[0][operatorIndex] == this.originPopulation[0][operatorIndex]
                        && this.destinationPopulation[1][operatorIndex] == this.originPopulation[1][operatorIndex]
                        && this.destinationPopulation[2][operatorIndex] == this.originPopulation[2][operatorIndex] ) {
                    cid = ci;
                } else {
                    cid = random.nextInt( cant_crom ) + this.destinationPopulation[1][operatorIndex];
                }
            }
            int ci_aux = cid;
            /**
             * Now we must make diference between 2 cases. if the origen and
             * destination are the same then we replace the original chromosome
             * with the result of beeing operated. otherwise we insert the
             * result at a random place
             */
            if ( this.originPopulation[0][operatorIndex] == this.destinationPopulation[0][operatorIndex] && this.originPopulation[1][operatorIndex] == this.destinationPopulation[1][operatorIndex] && this.originPopulation[2][operatorIndex] == this.destinationPopulation[2][operatorIndex] ) {
                ci_aux = ci;
            }
            pop_o = this.populations[this.originPopulation[0][operatorIndex]];
            pop_d = this.populations[this.destinationPopulation[0][operatorIndex]];
			if ( useHistoricalRecord ) {
                addNodeByOperator( operatorIndex , ci , cid , currentGeneration );
            }
            pop_d.setChromosome((Chromosome) this.operators.get( operatorIndex ).operate( pop_o.getChromosome( ci ) ) , ci_aux );
            pop_d.getChromosome( ci_aux ).setValidFitness( false );
            
        }
    }

    /**
     * Applies mutation operator on a chromosome randomly selected, and places
     * the resulting chromosome in one of two options, 1) Replacing the original
     * one (if the originPopulation and DestinationPopulation are the same) 2)
     * In a random position based on the DestinationPopulation array
     *
     * @param operatorIndex Represents the position of the operator in the
     * operators list
     * @param currentGeneration Represents the current generation. Needed by the
     * historial
     */
    private void cross( int operatorIndex , int currentGeneration ) {
        //Usefull indexes
        int mi, ci, ci1, cid, cid1, ci_aux, ci1_aux;
        //Amount of operations applied
        int max = 0; 
        float p = (float) 1.1; 
        Population pop_o, pop_d, pop;
        pop = this.populations[this.originPopulation[0][operatorIndex]];
        int pos_inic_crom = this.originPopulation[1][operatorIndex];
        int cant_crom = this.originPopulation[2][operatorIndex] - pos_inic_crom;

        List<Chromosome> padres = new ArrayList<Chromosome>();
        List<Chromosome> hijos = new ArrayList<Chromosome>();

        if ( this.operators.get( operatorIndex ).getMode() == 'c' ) {
            max = (int) this.values[operatorIndex];
        } else if ( this.operators.get( operatorIndex ).getMode() == '%' ) {
            max = (int) (this.values[operatorIndex] * cant_crom);
        } else if ( this.operators.get( operatorIndex ).getMode() == 'p' ) {
            max = (int) pop.size();
            p = this.values[operatorIndex];
        }
        for ( mi = 0 ; mi < max ; mi++ ) {
            ci = random.nextInt( cant_crom ) + pos_inic_crom;
            ci1 = random.nextInt( cant_crom ) + pos_inic_crom;
            if ( p <= 1 && random.nextFloat() > p ) {
                ci = mi;
                cid = mi;
            }
            if ( this.destinationPopulation[0][operatorIndex] == this.originPopulation[0][operatorIndex]
                    && this.destinationPopulation[1][operatorIndex] == this.originPopulation[1][operatorIndex]
                    && this.destinationPopulation[2][operatorIndex] == this.originPopulation[2][operatorIndex] ) {
                cid = ci;
                cid1 = ci1;
            } else {
                cid = random.nextInt( cant_crom ) + pos_inic_crom;
                cid1 = random.nextInt( cant_crom ) + pos_inic_crom;
            }

            ci_aux = cid;
            ci1_aux = cid1;
            /**
             * Now we must make difference between 2 cases. if the origin and
             * destination are the same then we replace the original chromosome
             * with the result of the operation. otherwise we insert the
             * result at a random place
             */
            if ( this.originPopulation[0][operatorIndex] == this.destinationPopulation[0][operatorIndex] && this.originPopulation[1][operatorIndex] == this.destinationPopulation[1][operatorIndex] && this.originPopulation[2][operatorIndex] == this.destinationPopulation[2][operatorIndex] ) {
                //If both parameter arrays are the exact same
                //Insert result of operate in the same position as the operand
                ci_aux = ci;
                ci1_aux = ci1;
            }
            pop_o = this.populations[this.originPopulation[0][operatorIndex]];
            pop_d = this.populations[this.destinationPopulation[0][operatorIndex]];
            padres.add( pop_o.getChromosome( ci ) );
            padres.add( pop_o.getChromosome( ci1 ) );
			if ( useHistoricalRecord ) {
                addNodeByOperator( operatorIndex , ci , ci_aux , ci1 , ci1_aux , currentGeneration );
            }
            hijos = (List<Chromosome>) this.operators.get( operatorIndex ).operate( padres );
            pop_d.setChromosome( hijos.get( 0 ) , ci_aux );
            pop_d.getChromosome( ci_aux ).setValidFitness( false );
            pop_d.setChromosome( hijos.get( 1 ) , ci1_aux );
            pop_d.getChromosome( ci1_aux ).setValidFitness( false );
            
        }
    }

    /**
     * Returns the algorithmName of the algorithm.
     *
     * @return algorithmName of the algorithm
     */
    public String getName() {
        return this.algorithmName;
    }

    /**
     * Returns an array containing the size of all the populations.
     *
     * @return populationSizes
     */
    public int[] getPopulationSizes() {
        return this.populationSizes;
    }

    /**
     * Sets the array with the sizes of all populations.
     *
     * @param populationSizes array containing sizes of all populations
     */
    public void setPopulationSizes( final int[] populationSizes ) {
        this.populationSizes = populationSizes;
    }

    /**
     * Sets the size of the population in the specified position.
     *
     * @param index Position of the population
     * @param populationSize array containing sizes of all populations
     */
    public void setPopulationSize( final int index , final int populationSize ) {
        this.populationSizes[index] = populationSize;
    }

    /**
     * Returns the size of the population in the specified position.
     *
     * @param index Represents the position of the population in the populations
     * array, and therefore the position of its size in the populationSizes
     * array
     *
     * @return populationSizes
     */
    public int getTamañopoblacion( final int index ) {
        return this.populationSizes[index];
    }

    /**
     * Returns the array with all the parameters to be used when operating
     * population number, and range of usable chromosomes.
     *
     * @return originPopulation
     */
    public int[][] getOriginPopulation() {
        return this.originPopulation;
    }

    /**
     * Sets the originPopulation, array with parameters to be used when
     * operating
     *
     * @param originPopulation array containing parameters
     */
    public void setOriginPopulation( final int[][] originPopulation ) {
        this.originPopulation = originPopulation;
    }

    /**
     * Returns the column in the position "index" of the originPopulation array.
     * as an unidimensional array.
     *
     * @param index position of the column
     * @return originPopulationColumn column to be copied
     */
    public int[] getOriginPopulation( final int index ) {
        if ( index >= originPopulation[0].length ) {
            System.err.println( "index " + index + " > size of originPopulation" );
            throw new java.lang.IndexOutOfBoundsException();
        }
        int[] originPopulationColumn = new int[3];
        originPopulationColumn[0] = originPopulation[0][index];
        originPopulationColumn[1] = originPopulation[1][index];
        originPopulationColumn[2] = originPopulation[2][index];
        return originPopulationColumn;
    }

    /**
     * Sets the column in the position "index" of the originPopulation array
     * using the values of the unidimensional originPopulationColumn input
     * parameter.
     *
     * @param index position of the column.
     * @param originPopulationColumn Column that will replace the existing one.
     */
    public void setOriginPopulation( final int index , final int[] originPopulationColumn ) {
        originPopulation[0][index] = originPopulationColumn[0];
        originPopulation[1][index] = originPopulationColumn[1];
        originPopulation[2][index] = originPopulationColumn[2];
    }

    /**
     * Returns the destinationPopulation array, to be used when operating.
     *
     * @return destinationPopulation
     */
    public int[][] getDestinationPopulation() {
        return this.destinationPopulation;
    }

    /**
     * Sets the destinationPopulation, array to be used when operating.
     *
     * @param destinationPopulationArray an array of indexes of populations to be used when 
     * placing the results of an operation.
     */
    public void setDestinationPopulation( final int[][] destinationPopulationArray ) {
        this.destinationPopulation = destinationPopulationArray;
    }

    /**
     * Returns the column in the position "index" from the destinationPopulation
     * array
     *
     * @param index position of the column
     * @return destinationPopulation
     */
    public int[] getDestinationPopulation( final int index ) {
        int[] destinationPopulationColumn = new int[3];
        destinationPopulationColumn[0] = this.destinationPopulation[0][index];
        destinationPopulationColumn[1] = this.destinationPopulation[1][index];
        destinationPopulationColumn[2] = this.destinationPopulation[2][index];
        return destinationPopulationColumn;
    }

    /**
     * Sets the destinationPopulation array, to be used when operating.
     *
     * @param indice position of the column.
     * @param destinationPopulationColumn Column that will replace the existing
     * one.
     */
    public void setDestinationPopulation( final int indice , final int[] destinationPopulationColumn ) {
        destinationPopulation[0][indice] = destinationPopulationColumn[0];
        destinationPopulation[1][indice] = destinationPopulationColumn[1];
        destinationPopulation[2][indice] = destinationPopulationColumn[2];
    }

    /**
     * Returns the values array, to be used when operating.
     *
     * @return values array with all the values to be used by each operator.
     */
    public float[] getValues() {
        return this.values;
    }

    /**
     * Sets the array values, to be used when operating
     *
     * @param values they determine how much to operate, the interpretation depends on
     * the mode in which the operator works.
     */
    public void setValues( final float[] values ) {
        this.values = values;
    }

    /**
     * Returns a specific value at the position "index" from the values array.
     *
     * @param index Position of the value in the values array.
     * @return value
     */
    public float getValue( final int index ) {
        return this.values[index];
    }

    /**
     * Sets a specific value in the position "index" from the values array.
     *
     * @param index position of the value to be changed.
     * @param value value that will replace the existing one.
     */
    public void setValor( final int index , final float value ) {
        this.values[index] = value;
    }

    /**
     * Returns, how many populations the algorithm has.
     *
     * @return populationQuantity Represents how many populations the algorithm
     * can handle
     */
    public int getPopulationsQuantity() {
        return this.populationsQuantity;
    }

    /**
     * Return the array with all populations.
     *
     * @return the array with all the populations
     */
    public Population[] getPopulations() {
        return this.populations;
    }

    /**
     * Return the array with all the elites populations.
     *
     * @return elites
     */
    public Population[] getElites() {
        return this.elites;
    }

    /**
     * Sets all populations.
     *
     * @param populations Array containing all the populations.
     */
    public void setPopulations( final Population[] populations ) {
        this.populations = populations;
    }

    /**
     * Returns a specific population at the position "index".
     *
     * @param index Position of the population in the populations array
     * @return populations[index]
     */
    public Population getPopulation( final int index ) {
        return this.populations[index];
    }

    /**
     * Sets a specific population at the position "index".
     *
     * @param index Position of the population
     * @param population a population
     */
    public void setPopulation( final int index , final Population population ) {
        this.populations[index] = population;
    }

    /**
     * Adds an operator to the List of operators.
     *
     * @param operator The operator to be added.
     * @param value How much to operate, the usage depends of the operator.
     * @param originPopulationIndex position of the population where to take operands
     * @param firstChromosome first usable chromosome
     * @param lastChromosome last usable chromosome
     * @param destinationPopulation position of the population where to place
     * the result of operation
     * @param initialChromosome first usable position where to place the result
     * of operation
     * @param endingChromosome last usable position where to place the result of
     * operation
     */
    public void addOperator( final Operator operator , float value , int originPopulationIndex , int firstChromosome , int lastChromosome , int destinationPopulationIndex , int initialChromosome , int endingChromosome ) {
        int tamOp = 0, tamVal = 0;
        if (operators == null){
            operators = new ArrayList<>();
        }
        if ( !this.operators.isEmpty() ) {
            tamOp = this.operators.size();
            tamVal = this.values.length;
        }
        int[][] originPopulation = new int[3][tamOp + 1];
        int[][] destinationPopulation = new int[3][tamOp + 1];
        float[] valoresaux = new float[tamVal + 1];
        //copio los operators que tengo hasta ahora
        for ( int i = 0 ; i < this.operators.size() ; i++ ) {
            for ( int j = 0 ; j < 3 ; j++ ) {
                originPopulation[j][i] = this.originPopulation[j][i];
                destinationPopulation[j][i] = this.destinationPopulation[j][i];
            }
            valoresaux[i] = this.values[i];
        }
        //add new Operator at the end
        originPopulation[0][tamOp] = originPopulationIndex;
        originPopulation[1][tamOp] = firstChromosome;
        originPopulation[2][tamOp] = lastChromosome;
        destinationPopulation[0][tamOp] = destinationPopulationIndex;
        destinationPopulation[1][tamOp] = initialChromosome;
        destinationPopulation[2][tamOp] = endingChromosome;
        valoresaux[tamOp] = value;

        this.originPopulation = originPopulation;
        this.destinationPopulation = destinationPopulation;
        this.values = valoresaux;
        this.operators.add( operator );
    }

    /**
     * Removes an operator from the list of operators and all the elements
     * needed by this operator. value, originPopulation column and
     * destinationPopulation column. (This method is not meant to be used during
     * the execution of an algorithm but after).
     *
     * @param index Position of the operator to be removed
     */
    public void removeOperator( final int index ) {
        int tamOp = this.operators.size();
        int tamVal = this.values.length;

        this.operators.remove( index );
        int[][] poblacionorigeneAux = new int[3][tamOp + 1];
        int[][] poblaciondestinoaux = new int[3][tamOp + 1];
        float[] valoresaux = new float[tamVal + 1];
        //copio los operators que tengo hasta ahora
        int inserted = 0;
        for ( int i = 0 ; i < this.operators.size() ; i++ ) {
            if ( i != index ) {
                for ( int j = 0 ; j < 3 ; j++ ) {
                    poblacionorigeneAux[j][inserted] = this.originPopulation[j][i];
                    poblaciondestinoaux[j][inserted] = this.destinationPopulation[j][i];
                }
                valoresaux[inserted] = this.values[i];
            }
        }
        setValues( valoresaux );
        setOriginPopulation( poblacionorigeneAux );
        setDestinationPopulation( poblaciondestinoaux );
    }

    /**
     * Searches an operator by algorithmName and removes it operator and all the elements
 it needs. Value, originPopulation column, destinationPopulation column.
 If there is more than one operator with the same algorithmName it will remove all
 of them.
     *
     * @param operatorName Name of the operator to be removed
     */
    public void removeOperator( final String operatorName ) {
        boolean success = false;
        for ( int i = 0 ; i < this.operators.size() ; i++ ) {
            if ( operators.get( i ).getName().compareTo( operatorName ) == 0 ) {
                removeOperator( i );
                success = true;
            }
        }
        if ( !success ) {
            System.err.println( "removeOperator can't find an operator named:" + operatorName );
            throw new java.lang.IllegalArgumentException();
        }
    }

    /**
     * Gets the maximum number of generations allowed per execution.
     *
     * @return generations
     */
    public int getGenerations() {
        return this.generations;
    }

    /**
     * Sets the maximum number of generations allowed per execution.
     *
     * @param generations Maximum number of generations allowed
     */
    public void setGenerations( int generations ) {
        this.generations = generations;
    }

    /**
     * Add node to the HistoricalRecord (one parent , one son)
     *
     * @param operatorIndex Position of the operator
     * @param chromosomeindex Position of the original chromosome
     * @param chromosomeDestinationIndex Position where the chromosome resulting
     * of the operation will be placed
     * @param currentGeneration Current generation
     */
    private void addNodeByOperator( int operatorIndex , int chromosomeindex , int chromosomeDestinationIndex , int currentGeneration ) {
        int numeropoblacionpadre_o = this.originPopulation[0][operatorIndex];
        int numeropoblacionpadre_d = this.destinationPopulation[0][operatorIndex];

        Population pDest = this.populations[numeropoblacionpadre_d];
        Population pOrig = this.populations[numeropoblacionpadre_o];

        Node parentNode = pOrig.getHistoricalRecord().getLastNode( chromosomeindex );

        pDest.getHistoricalRecord().addNode( parentNode ,
                pOrig.getChromosome( chromosomeindex ).getFitness() ,
                this.operators.get( operatorIndex ).getName() ,
                currentGeneration ,
                chromosomeDestinationIndex );
    }

    /**
     * Add node to the HistoricalRecord ( two parents, 2 sons)
     *
     * @param operatorIndex Position of the operator
     * @param chromosomeindex1 Position of the first original chromosome
     * @param chromosomeDestinationIndex1 Position where the first chromosome
     * resulting of the operation will be placed
     * @param chromosomeindex2 Position of the second original chromosome
     * @param chromosomeDestinationIndex2 Position where the second chromosome
     * resulting of the operation will be placed
     * @param currentGeneration Current generation
     */
    private void addNodeByOperator( int operatorIndex , int chromosomeindex1 , int chromosomeDestinationIndex1 , int chromosomeindex2 , int chromosomeDestinationIndex2 , int currentGeneration ) {
        int numeropoblacionpadre_o = this.originPopulation[0][operatorIndex];
        int numeropoblacionpadre_d = this.destinationPopulation[0][operatorIndex];

        Population pDest = this.populations[numeropoblacionpadre_d];
        Population pOrig = this.populations[numeropoblacionpadre_o];

        //Get the parents from using chromosomeindex 1 and 2
        Node parentNode1 = pOrig.getHistoricalRecord().getLastNode( chromosomeindex1 );
        Node parentNode2 = pOrig.getHistoricalRecord().getLastNode( chromosomeindex2 );

        //add sonNode1 to parentNode1
        pDest.getHistoricalRecord().addNode( parentNode1 ,
                pOrig.getChromosome( chromosomeindex1 ).getFitness() ,
                this.operators.get( operatorIndex ).getName() ,
                currentGeneration ,
                chromosomeDestinationIndex1 );
        //add sonNode2 to parentNode2
        pDest.getHistoricalRecord().addNode( parentNode1 ,
                pOrig.getChromosome( chromosomeindex1 ).getFitness() ,
                this.operators.get( operatorIndex ).getName() ,
                currentGeneration ,
                chromosomeDestinationIndex2 );

        Node sonNode1 = parentNode1.getSonNode(parentNode1.sonNodes.size()-2);
        Node sonNode2 = parentNode1.getSonNode(parentNode1.sonNodes.size()-1);

        pDest.getHistoricalRecord().addEdge( parentNode2 , sonNode1 );
		sonNode1.setParentFitness( pOrig.getChromosome(chromosomeindex2).getFitness(),1);
        pDest.getHistoricalRecord().addEdge( parentNode2 , sonNode2 );
		sonNode2.setParentFitness( pOrig.getChromosome(chromosomeindex2).getFitness() ,1);
		
        int a = 0;
    }

    /**
     * Saves population to a file in the working folder "Path" given to the
     * dataHelper using the fileName specified. If there isn't a file with the
 given algorithmName a new one will be created.
     *
     * @param fileName Name of the file where to save.
     */
    public void savePopulations( String fileName ) {
        //Save the populations to a file named "fileName"
        for ( int p = 0 ; p < populations.length ; p++ ) {
            //We need to call the helper for this
            datahelper.savePopulation( populations[p] , fileName + p );
        }
    }

    /**
     * Selects the elites from the regular populations and insert them into the
     * elites populations
     *
     * @param currentGeneration Current generation. Needed for the HistoricalRecord
     */
    private void selectElites( int currentGeneration ) {
        SelectElites seleccion_elites = new SelectElites();
        //we must do it for all populations
        for ( int ip = 0 ; ip < this.populations.length ; ip++ ) {
            //As with any selection operator we must pass it several parameters
            /**
             * origin population destination population origin population range
             * of selectable chromosomes destination population range of usable
             * positions amount of chromosomes to be selected current generation
             * historial flag
             */
            List<Object> parameters = new ArrayList<Object>();

            //Cargamos las populations origen y destino involucradas 
            parameters.add( this.populations[ip] );
            parameters.add( this.elites[ip] );

            //Cargamos los vectores originPopulation y destinationPopulation correspondientes
            //Recordemos que el contenido de este vector es el siguiente
            //  0. indice de la poblacion origen (De donde se seleccionaran chromosomes),
            //  1. indice del primer cromosoma (a partir del cual se seleccionara) , 
            //  2. indice del ultimo cromosoma seleccionable  
            int[] auxiliarpoblacionorigen = new int[3], auxiliarpoblaciondestino = new int[3];

            //we need to send this the way that the operator is expecting them
            auxiliarpoblacionorigen[0] = ip;
            auxiliarpoblacionorigen[1] = 0;
            auxiliarpoblacionorigen[2] = this.populations[ip].size();

            auxiliarpoblaciondestino[0] = ip;
            auxiliarpoblaciondestino[1] = 0;
            auxiliarpoblaciondestino[2] = this.elites[ip].size();

            //We set the amount of chromosomes to be selected
            int valor = (int) this.elitesSizes[ip];
            parameters.add( auxiliarpoblacionorigen );
            parameters.add( auxiliarpoblaciondestino );

            parameters.add( valor );
            parameters.add( currentGeneration );
            parameters.add( useHistoricalRecord );

            seleccion_elites.operate( parameters );
        }
    }

    /**
     * Reinserts elites by placing copies of them into the corresponding regular
     * population
     *
     * @param currentGeneration Current Generation, Needed for the HistoricalRecord.
     */
    private void reinsertElites( int currentGeneration ) {
        ReinsertElites seleccion_replicar = new ReinsertElites();
        //We must do this for all populations
        for ( int ip = 0 ; ip < this.populations.length ; ip++ ) {
            //As with any selection operator we must pass it several parameters
            /**
             * origin population destination population origin population range
             * of selectable chromosomes destination population range of usable
             * positions amount of chromosomes to be selected current generation
             * historial flag
             */
            List<Object> parametros = new ArrayList<Object>();
            //We send origin (elites)  and destination (regular) populations 
            parametros.add( this.elites[ip] );
            parametros.add( this.populations[ip] );

            /**
             * 0. position of the population from where to take chromosomes
             * 1. position of the first usable chromosome
             * 2. position of the last usable chromosome 
             */ 
            int[] auxiliarpoblacionorigen = new int[3], auxiliarpoblaciondestino = new int[3];

            //we need to send this the way that the operator is expecting them
            auxiliarpoblacionorigen[0] = ip;
            auxiliarpoblacionorigen[1] = 0;
            auxiliarpoblacionorigen[2] = this.populations[ip].size();

            auxiliarpoblaciondestino[0] = ip;
            auxiliarpoblaciondestino[1] = 0;
            auxiliarpoblaciondestino[2] = this.elites[ip].size();

            //We set the amount of chromosomes to be selected
            int valor = (int) this.elitesSizes[ip];
            parametros.add( auxiliarpoblacionorigen );
            parametros.add( auxiliarpoblaciondestino );

            parametros.add( valor );
            parametros.add( currentGeneration );
            parametros.add( useHistoricalRecord );

            seleccion_replicar.operate( parametros );
        }

    }

    /**
     * Gets the value of the debug flag.
     *
     * @return Value of the debug flag.
     */
    public boolean getDebug() {
        return debug;
    }

    /**
     * setDebug: Changes the value of the debug flag
     *
     * @param value for the debug flag
     */
    public void setDebug( boolean value ) {
        debug = value;
    }

    /**
     * Changes the value of the useHistoricalRecord flag to true.
     *
     */
    public void useHistoricalRecord() {
        useHistoricalRecord = true;
    }
    
    /**
     * Changes the value of the useHistoricalRecord flag to false.
     *
     */
    public void dontUseHistoricalRecord() {
        useHistoricalRecord = false;
    }

    /**
     * Gets the value of the historial flag.
     *
     * @return a boolean
     */
    public boolean getUseHistoricalRecord() {
        return useHistoricalRecord;
    }

    /**
     * Contructor.
     *
     * @param name Name of the Algorithm
     * @param populationQuantity Quantity of populations the algorithm will
     * handle.
     * @param populationSizes Sizes for each of the populations.
     * @param populations Array with all the populations.
     * @param elitesSizes Sizes of the elites populations.
     * @param operators List with all the operators
     * @param values Array with parameters to be used by the algorithm to decide
     * how many times operate.
     * @param originPopulation Bidimensional Array with parameters to be used by
     * the algorithm to decide from where to choose operands when operating.
     * @param destinationPopulation Bidimensional Array with parameters to be
     * used by the algorithm to decide where to place the results of operating.
     * @param generations Maximum number of generations allowed.
     * @param evaluation Evaluation object containing methods to express a
     * chromosome and to assign a fitness based on some criteria.
     * @param dataHelper Helper to handle data input and output from the
     * algorithm
     */
    public gepAlgorithm( final String name , final int populationQuantity , final int[] populationSizes , Population[] populations , int[] elitesSizes , List<Operator> operators , final float[] values , final int[][] originPopulation , final int[][] destinationPopulation , final int generations , Evaluation evaluation , DataHelper dataHelper ) {
        this.algorithmName = name;
        this.populationSizes = populationSizes;
        this.populationsQuantity = populationQuantity;
        this.values = values;
        this.originPopulation = originPopulation;
        this.destinationPopulation = destinationPopulation;
        this.generations = generations;
        this.populations = populations;
        this.operators = operators;
        this.evaluation = evaluation;
        this.datahelper = dataHelper;
        this.elitesSizes = elitesSizes;
        this.useHistoricalRecord = false;
        this.debug = false;
        this.showPatrilinealBranch = false;
        this.parallelizeFitnessEvaluation = false;
        this.elites = new Population[populations.length];
        //We create the elites now
        for ( int ie = 0 ; ie < this.populations.length ; ie++ ) {
            //We must use the size specified in the elitesSize array
            for ( int ite = 0 ; ite < this.elitesSizes.length ; ite++ ) {
                //Then we create it as any other population
                this.elites[ite] = new Population(
                        this.elitesSizes[ite] , ite , this.populations[ite].getChromosomeCoding()
                );
                for ( int ic = 0 ; ic < this.elites[ite].size() ; ic++ ) {
                    this.evaluation.evaluate( this.elites[ite].getChromosome( ic ) );
                }
            }
        }
		this.analyzer = new Analyzer();
    }
    
    /**
     * Changes the Evaluation 
     * 
     * @param evaluation  container for all the methods needed to 
     * assign the fitness 
     */
    public void setEvaluation(Evaluation evaluation){
        this.evaluation = evaluation;
    }
    
    /**
     * Gets the evaluation object used by this algorithm
     * 
     * @return Evaluation.
     */
    public Evaluation getEvaluation(){
        return this.evaluation;
    }

    /**
     * Goes through the operators array and applies operators.
     *
     * @param currentGeneration Current generation. Needed by the HistoricalRecord and
 some operators.
     */
    private void operate( int currentGeneration ) {
        for ( int oi = 0 ; oi < this.operators.size() ; oi++ ) {
            //Primero obtenemos la clase de operador
            if ( this.operators.get( oi ).getClass().getSuperclass() == Mutation.class ) {
                mutate( oi , currentGeneration );
            } else if ( this.operators.get( oi ).getClass().getSuperclass() == Permutation.class ) {
                permutate( oi , currentGeneration );
            } else if ( this.operators.get( oi ).getClass().getSuperclass() == Crossover.class ) {
                cross( oi , currentGeneration );
            } else if ( this.operators.get( oi ).getClass().getSuperclass() == Selection.class ) {
                select( oi , currentGeneration );
            }
        }
    }
    
    public Operator getOperator(int operatorIndex){
        return this.operators.get(operatorIndex);
    }
    
    public int getOperatorListSize(){
        return this.operators.size();
    }
    
    public void showPatrilinealBranch(){
        this.showPatrilinealBranch = true;
    }
    public void dontShowPatrilinealBranch(){
        this.showPatrilinealBranch = false;
    }
    public void paralelizeFitnessEvaluation(){
        this.parallelizeFitnessEvaluation = true;
    }
}
