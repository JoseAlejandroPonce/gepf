/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis;

/**
 *
 * @author alejandro
 */
public abstract class Walker{
	
	protected Counter operationsCounter;
	
	public Walker(Counter anOperationsCounterImpl){
		this.operationsCounter = anOperationsCounterImpl;
	}
	
	public abstract void walk(Node node, Statistics statistics);
	
	public abstract void unVisitTree(Node node);
	
}
