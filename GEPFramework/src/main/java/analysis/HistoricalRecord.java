/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package analysis;

import java.util.ArrayList;
import java.util.List;

/**
 * Data structure which contains information about the operations applied to the
 * population
 *
 * @author Jose Alejandro Ponce
 */
public class HistoricalRecord {

    /**
     * Bidimenional array, where each row represents a generation, and there is
     * a column for each chromosome
     */
    private final List<ArrayList<Node>> nodes;

    /**
     * List with references pointing to the last nodes added to the HistoricalRecord
     */
    private final List<Node> lastNodes;

    /**
     * Index that represents the position of the population to which this
 HistoricalRecord belongs.
     */
    private final int populationNumber;

    /**
     * Total of Operations involved in the creation of a given chromosome.
     */
    private static int totalOperations;

    /**
     * Total of Operations using a given operator involved in the creation of a
     * given chromosome.
     */
    private static int totalOperationsOfOperator;

    /**
     * Adds a new node to the HistoricalRecord, in the position specified by
 "chromosomeIndex", and creating a relationship between parentNode and the
 new node. It also updates the "lastNodes" list. so that now the new Node
     * is also the latest node in the position "chromosomeIndex".
     *
     * @param parentNode Node that will be the parent of the new Node.
     * @param parentFitness Fitness of the chromosome that was operated.
     * @param operatorName Name of the operator used on the chromosome.
     * @param generation Number of generation in which the operator was used
     * @param chromsomeindex Represents the position of the chromosome that will
     * be replaced with the result of the operation
     */
    public Node addNode( Node parentNode , float parentFitness , String operatorName , int generation , int chromsomeindex ) {
        if ( this.nodes.size() > 0 ) {
            checkIndex( generation , this.nodes.size() , "addNode:" );
        }
        checkIndex( chromsomeindex , this.nodes.get( generation ).size() , "addNode:" );
        Node sonNode = new Node( parentFitness , chromsomeindex , generation , parentNode.getNodeNumber()+1  , operatorName ,this.populationNumber);
        addEdge( parentNode , sonNode );
        this.lastNodes.set( chromsomeindex , sonNode );
		return sonNode;
    }

    /**
     * Takes the last nodes of the HistoricalRecord and creates a new generation by
 adding them to a new Row of the nodes. It also updates the "lastNodes"
     * list.
     */
    public void addNewGeneration() {
        int generationNumber,newGenerationNumber;
        for ( int ui = 0 ; ui < lastNodes.size() ; ui++ ) {
            generationNumber = lastNodes.get( ui ).getGenerationNumber();
			newGenerationNumber=generationNumber+1;
            //Add nodes
            Node newNode = this.addNode( lastNodes.get( ui ),
                    lastNodes.get( ui ).getParentFitness( 0 ) ,
                    "Generation" + newGenerationNumber,
                    generationNumber,
                    ui );
			newNode.generation++;
			newNode.nodeNumber=0;
        }
        ArrayList<Node> newGenerations;
        newGenerations = (ArrayList<Node>) lastNodes;
        this.nodes.add( newGenerations );
    }

    /**
     * Adds a relationship between the parent node and its son.
     *
     * @param parentNode this node will be used as parent of the son node
     * @param sonNode this node will be now used as son of the parent node
     */
    public void addEdge( final Node parentNode , final Node sonNode ) {
        parentNode.addSonNode( sonNode );
        sonNode.addParentNode( parentNode );
    }

    /**
     * Constructor.
     *
     * @param populationSize size of the population 
     * @param populationNumber position of the population within the algorithm
     */
    public HistoricalRecord( final int populationSize , final int populationNumber ) {
        //generate initial nodes
        ArrayList<Node> generationZero = new ArrayList<Node>();
        nodes = new ArrayList<ArrayList<Node>>();
        lastNodes = new ArrayList<Node>();
        for ( int i = 0 ; i < populationSize ; i++ ) {
            generationZero.add( new Node( 0 , i , 0 , 0 , "Initial" ,populationNumber) );
            lastNodes.add( generationZero.get( i ) );
        }
        nodes.add( generationZero );
        this.populationNumber = populationNumber;
    }

    /**
     * Shows the sequence of operators used that lead to the creation of a
     * chromosome 
     * Note: given the complexity of the structure only the
     * patrilineal branch of the whole genealogical tree is shown with this
     * method.
     *
     * @param chromosomeIndex Position of the chromosome that we want to
     * analyze.
     */
    public void showPatrilinealBranch( int chromosomeIndex ) {
        Node nodeAux = getLastNode( chromosomeIndex );
        System.out.print( "Showing patrilineal branch of the genealogical tree.)\n" );
        while ( nodeAux != null ) {
            System.out.print( nodeAux.getOperatorName() + "\n" );
            if ( nodeAux.getNodeNumber() > 0 ) {
                nodeAux = nodeAux.getParentNode( 0 );
            } else {
                nodeAux = null;
            }
        }
    }

    /**
     * Gets a particular node using its generationNumber, nodeNumber and
     * chromosomeIndex.
     *
     * @param generationNumber Represents the generation where the node is
     * created
     * @param nodeNumber Represents the vertical distance to the first node of a
     * generation
     * @param chromosomeIndex Represents the position of the node on the
     * lastNodes list
     * @return the node that complies with the search parameters, or null.
     */
    public Node getNode( final int generationNumber , final int nodeNumber , final int chromosomeIndex ) {
        checkIndex( generationNumber , this.nodes.size() , "getNode:" );
        checkIndex( nodeNumber , 0 , "getNode:" );
		checkIndex( chromosomeIndex , this.nodes.get( generationNumber).size() , "getNode:" );
	
        ArrayList<Node> auxNodesList = this.nodes.get( generationNumber );
        Node auxNode = auxNodesList.get( chromosomeIndex );
        if ( nodeNumber == 0 ) {
            return auxNode;
        } else if ( nodeNumber > 0 ) {
            while ( auxNode.getSonNode( 0 ) != null ) {
                if ( auxNode.getNodeNumber() == nodeNumber ) {
                    return auxNode;
                }
                auxNode = auxNode.getSonNode( 0 );
            }
        }
        if ( auxNode.getNodeNumber() == nodeNumber ) {
            return auxNode;
        }
        if ( auxNode.getNodeNumber() < nodeNumber ) {
            System.err.println( "HistoricalRecord.getNodo: nodeNumber" + nodeNumber
                    + " is Greater than the times an operator was used on the chromosome" + chromosomeIndex
                    + " dring the generation " + generationNumber + " ." );
            throw new java.lang.IllegalArgumentException();
        }
        System.err.println( "HistoricalRecord.getNode() Wrong endpoint reached." );
        throw new java.lang.IllegalArgumentException();
    }

    /**
     * Gets the last node inserted in the position chromosomeIndex.
     *
     * @param chromosomeIndex Represents the position of the node in the
     * LastNodes list
     * @return The last node on the specified position
     */
    public Node getLastNode( final int chromosomeIndex ) {
	checkIndex( chromosomeIndex , lastNodes.size() , "getLastNode:" );
        return this.lastNodes.get( chromosomeIndex );
    }

    /**
     * Gets the last node on the position chromosomeIndex of a given generation.
     *
     * @param generationNumber Represents the generation in which the node was
     * created
     * @param chromosomeIndex Represents the position of the node in the
     * LastNodes list
     * @return The last node of a given generation in the specified position
     */
    public Node getLastNodeOfGeneration( int generationNumber , int chromosomeIndex ) {
        checkIndex(generationNumber,this.nodes.size(),"getLastNodeOfGeneration:");
        ArrayList<Node> nodesOfGeneration = this.nodes.get( generationNumber );
		checkIndex(chromosomeIndex,nodesOfGeneration.size(),"getLastNodeOfGeneration:");
        Node auxNode = nodesOfGeneration.get( chromosomeIndex );

        while ( auxNode.getSonNode( 0 ) != null ) {
            if ( auxNode.getSonNode( 0 ).getGenerationNumber() != generationNumber ) {
                return auxNode;
            }
            auxNode = auxNode.getSonNode( 0 );
        }
        if ( auxNode.getSonNode( 0 ) == null ) {
            return auxNode;
        }
        System.err.println( "HistoricalRecord.getLastNodeOfGeneration() Wrong endpoint reached." );
        throw new java.lang.Error();
    }

    /**
     * Gets the population number of this historial. Population number
     * represents the position of the population to which this historial
     * belongs.
     *
     * @return populationNumber
     */
    public int getPopulationNumber() {
        return this.populationNumber;
    }

    /**
     * Checks if the index is within [0,highBound]
     *
     * @param i Index to verify
     * @param highBound maximum allowed value.
     * @param msg Message to show in case the index doesn't pass the check.
     */
    public void checkIndex( int i , int highBound , String msg ) {
        if ( i < 0 ) {
            System.err.println( "HistoricalRecord. " + msg + "The index cannot be minor than 0" );
            throw new java.lang.IndexOutOfBoundsException();
        } else if ( i >= highBound && highBound != 0) {
            System.err.println( "HistoricalRecord. " + msg + "The index cannot be greater than: " + highBound );
            throw new java.lang.IndexOutOfBoundsException();
        }
    }

    /**
     * Shows the statistics for a given operator when used during the creation
     * of a given chromosome.
     *
     * @param operatorName Statistics will be collected for the operation with
     * this name.
     * @param chromosomeIndex The genealogical tree of the chromosome in this
     * position will be analyzed in order to get the statistics for the
     * operations used.
     */
//    public void showStatsByConsole( String operatorName , int chromosomeIndex ) {
//        float percentage;
//        // Go through the genealogicaltree to count the following items
//        totalOperationsOfOperator = 0;
//        totalOperations = 0;
//        
//        walkTheGenealogicalTree( getLastNode( chromosomeIndex ) , operatorName );
//        unVisitTree( getLastNode( chromosomeIndex ) );
//        percentage = ((float) totalOperationsOfOperator / (float) totalOperations) * 100;
//        System.out.println( operatorName + ": " + totalOperationsOfOperator );
//        System.out.println( String.format( "%.2f" , percentage ) );
//    }

    /**
     * Shows the total of operations used during the creation of the chromosome
     * in the position chromosomeIndex
     *
     * @param chromosomeIndex Represents the position of the node in the
     * LastNodes list
     */
//    public void showTotalByConsole( int chromosomeIndex ) {
//        totalOperations = 0;
//        unVisitTree( getLastNode( chromosomeIndex ) );
//        walkTheGenealogicalTree( getLastNode( chromosomeIndex ), " " );
//        unVisitTree( getLastNode( chromosomeIndex ) );
//        System.out.println( "Total of operations: " + totalOperations );
//    }

    /**
     * Goes through the genealogical tree recursively.
     *
     * @param node Node which genealogical tree will be walked.
     * @param operatorName Name of the operator to be accounted.
     */
//    public void walkTheGenealogicalTree( Node node , String operatorName ) {
//        if ( !node.wasVisited() ) {
//            node.visit();
//            if ( node.getOperatorName().compareTo( "Generation" + node.getGenerationNumber() ) != 0 && node.getOperatorName().compareTo( "Initial") != 0 ) {
//                totalOperations++;
//            }
//            if ( node.getOperatorName().compareTo( operatorName ) == 0 ) {
//                totalOperationsOfOperator++;
//            }
//            if ( node.getParentNode( 0 ) != null ) {
//                walkTheGenealogicalTree( node.getParentNode( 0 ) , operatorName );
//            }
//            if ( node.getParentNode( 1 ) != null ) {
//                walkTheGenealogicalTree( node.getParentNode( 1 ) , operatorName );
//            }
//            if ( node.getParentNode( 2 ) != null ) {
//                System.err.println( "A node should have at most to 2 parents" );
//                throw new java.lang.UnsupportedOperationException();
//            }
//        }
//    }

    /**
     * Goes through the first parent branch of the tree iteratively.
     *
     * @param node Node from which to start
     * @param operatorName Name of the operator to be counted
     */
//    public void walkPatrilinealBranch( Node node , String operatorName ) {
//        while ( node.getParentNode( 0 ) != null ) {
//            if ( node.getOperatorName().compareTo( "Generation: " + node.getGenerationNumber() ) != 0 ) {
//                totalOperations++;
//            }
//            if ( node.getOperatorName().compareTo( operatorName ) == 0 ) {
//                totalOperationsOfOperator++;
//            }
//            node = node.getParentNode( 0 );
//        }
//    }

    /**
     * Marks all nodes of the input parameter node's genealogical tree as unvisited
     *
     * @param node Node from which to start
     */
//    public void unVisitTree( Node node ) {
//        if(node.wasVisited()){
//            node.unvisit();
//            if ( node.getParentNode( 0 ) != null ) {
//                unVisitTree( node.getParentNode( 0 ) );
//            }
//            if ( node.getParentNode( 1 ) != null ) {
//                unVisitTree( node.getParentNode( 1 ) );
//            }
//            if ( node.getParentNode( 2 ) != null ) {
//                System.err.println( "A node of the HistoricalRecord should have at most 2 parents." );
//                throw new java.lang.UnsupportedOperationException();
//            }
//        }
//    }
	
	public int getSizeByGenerations(){
		return this.nodes.size();
	} 
	
	public int getSizeByPopulation(){
		return this.nodes.get(0).size();
	}
}
