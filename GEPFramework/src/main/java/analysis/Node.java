/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package analysis;

import java.util.ArrayList;
import java.util.List;

/**
 * Node of the Historial, Represents an operation made to a chromosome.
 *
 * @author Alejandro
 */
public class Node {

    /**
     * Boolean visited, used to mark a node as already visited when doing
     * searches or
     */
    private boolean visited;

    private float[] parentFitness;

    /**
     * Name of the operator.
     */
    private String operatorName;

    /**
     * Generation in which this node was created
     */
    protected int generation;

    /**
     * Index that represents the position of the chromosome that will be
     * replaced with the result of the operation.
     */
    private int chromosomeindex;

    /**
     * Index that represents the vertical position of the node. Counting from
     * the start of a generation and only within that generation.
     */
    protected int nodeNumber;

    /**
     * Index that represents the position of the population to which the parent
     * of this node belongs.
     */
    private int populationNumber;

    /**
     * List that contains references to the parents of this node.
     */
    public List<Node> parentNodes = new ArrayList<Node>();

    /**
     * List that contains references to the sons of this node.
     */
    public List<Node> sonNodes = new ArrayList<Node>();

    /**
     * Gets the name of the operator that this node represents.
     *
     * @return operatorName
     */
    public String getOperatorName() {

        return this.operatorName;
    }

    /**
     * Gets the generation in which this node was created.
     *
     * @return generation
     */
    public int getGenerationNumber() {

        return this.generation;
    }

    /**
     * Gets the chromosome Index of this node, that represents the position of
     * the chromosome resulting of the operation.
     *
     * @return chromosomeindex of this node.
     */
    public int getIndicecromosoma() {
        return this.chromosomeindex;
    }

    /**
     * Gets the nodeNumber, used to identify the node vertically within a
     * generation.
     *
     * @return nodeNumber
     */
    public int getNodeNumber() {
        return this.nodeNumber;
    }

    /**
     * Gets the parent node in the position "index".
     *
     * @param index Position of the parent node, counting from zero.
     * @return parent node.
     */
    public Node getParentNode( int index ) {
        if ( this.parentNodes == null || this.parentNodes.isEmpty() ) {
            return null;
        }
        if ( (index >= parentNodes.size()) ) {
            return null;
        }
        return this.parentNodes.get( index );
    }

    /**
     * Gets the son node in the position index.
     *
     * @param index Position of the son to retrieve.
     * @return sonNode
     */
    public Node getSonNode( int index ) {

        if ( this.sonNodes == null || this.sonNodes.isEmpty() ) {
            return null;
        }
        if ( index < 0 || index >= sonNodes.size() ) {
            System.err.println( "Historial,Nodos.getNodoHijo: Index out of bounds" );
            throw new java.lang.IndexOutOfBoundsException();
        }
        return this.sonNodes.get( index );
    }
	
	public int getHowManySons(){
		return this.sonNodes.size();
	}

    /**
     * Adds a new Node as son of this node, at the end of the list of son nodes.
     *
     * @param sonNode Node that will be the son of this node.
     */
    public void addSonNode( Node sonNode ) {
        this.sonNodes.add( sonNode );
    }

    /**
     * Adds a new Node as parent of this node,at the end of the list of parent
     * nodes.
     *
     * @param parentNode Node that will be the father of this node
     */
    public void addParentNode( Node parentNode ) {
        this.parentNodes.add( parentNode );
    }

    /**
     * Gets the fitness of the operated chromosome (before beeing operated).
     * It requires an index because each node can have up to 2 parents.
     *
     * @param index position of the parent node.
     * @return parentFitness of the parent in the specified position
     */
    public float getParentFitness( int index ) {
        checkIndex( index , 1 , "Nodos.getFitnessPadre: " );
        return this.parentFitness[index];
    }

    /**
     * Sets the fitness of the operated chromosome. It requires an index because
     * each node can have up to 2 parents.
     *
     * @param fitness fitness to be set.
     * @param index position of the parent node to which set the fitness.
     */
    public void setParentFitness( float fitness , int index ) {
        checkIndex( index , 1 , "Nodos.setFitnessPadre: " );
        if ( this.getParentNode( index ) == null ) {
            System.out.println( "Nodos.setParentFitness: This node doesn't have a parent with that index" );
            throw new java.lang.UnsupportedOperationException();
        }
        this.parentFitness[index] = fitness;
    }
    
    /**
     * Returns the position of the population to which this node belongs.
     * 
     * @return populationNumber
     */
    public int getPopulationNumber(){
        return populationNumber;
    }
    
    /**
     * Sets the Population number for this node.
     * 
     * @param newNodePopNumber value that replace the existing one.
     */
    public void setPopulationNumber(int newNodePopNumber){
        populationNumber = newNodePopNumber;
    }

    /**
     * Constructor.
     *
     * @param parentFitness Represents the fitness of the chromosome that was
     * operated.
     * @param chromosomeindex Represents the position where the chromosome
     * resulting of the operation will be placed.
     * @param generationNumber Represents the generation in which this node was
     * created.
     * @param nodeNumber Represents the number of operations that were made to
     * the chromosome during a generation.
     *
     * @param operatorName Nombre del operador que da origen a este nodo.
     * 
     * @param populationNumber Index representing the position of the population in the
     * populationsArray
     */
    public Node( float parentFitness , int chromosomeindex , int generationNumber , int nodeNumber , String operatorName ,int populationNumber) {
        if ( chromosomeindex < 0 ) {
            System.err.println( "Nodos, chromosomeindex cannot be lower than 0." );
            throw new java.lang.IllegalArgumentException();
        }
        if ( generationNumber < 0 ) {
            System.err.println( "Nodos, generationNumber cannot be lower than 0." );
            throw new java.lang.IllegalArgumentException();
        }
        if ( nodeNumber < 0 ) {
            System.err.println( "Nodos, nodeNumber cannot be lower than 0." );
            throw new java.lang.IllegalArgumentException();
        }
        this.parentFitness = new float[2];
        this.parentFitness[0] = parentFitness;
        this.chromosomeindex = chromosomeindex;
        this.generation = generationNumber;
        this.parentNodes = new ArrayList<Node>();
        this.sonNodes = new ArrayList<Node>();
        this.nodeNumber = nodeNumber;
        this.operatorName = operatorName;
        this.populationNumber = populationNumber;
        visited = false;
    }

    /**
     * Checks that the index is within [0 , highBound]
     *
     * @param index value to be checked
     * @param highBound maximum value allowed (exclusive)
     * @param msg message to be shown if the index does not pass the check
     */
    private void checkIndex( int index , int highBound , String msg ) {
        if ( index < 0 ) {
            System.err.println( msg + ": Index minor than zero." );
            throw new java.lang.IndexOutOfBoundsException();
        }
        if ( index > highBound ) {
            System.err.println( msg + ": Index greater than " + highBound + ".");
            throw new java.lang.IndexOutOfBoundsException();
        }
    }

    /**
     * Marks this node as Visited.
     */
    public void visit() {
        this.visited = true;
    }

    /**
     * Marks this node as not visited.
     */
    public void unvisit() {
        this.visited = false;
    }

    /**
     * Asks if the node was visited. Returns a boolean.
     *
     * @return visited
     */
    public boolean wasVisited() {
        return this.visited;
    }
}
