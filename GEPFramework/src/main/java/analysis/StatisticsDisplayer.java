/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis;

/**
 *
 * @author alejandro
 */
public interface StatisticsDisplayer{
	/**
	 * Shows the statistics for a given operator when used during the creation
	 * of a given chromosome.
	 *
	 * @param statistics
	 */
	public abstract void showStatistics(Statistics statistics);
	
}
