/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis;

import java.util.List;


/**
 *
 * @author alejandro
 */
public class Statistics{
	
	public int totalOperations;
	public int totalOperationsOfOperator;
	public int amountOfFitnessJumps;
	public String operatorName;
	public List<String> operatorsInvolvedInFitnessJumps;
	
	public Statistics(String operatorName){
		this.operatorName = operatorName;
		this.totalOperations = 0;
		this.totalOperationsOfOperator = 0;
		this.amountOfFitnessJumps = 0;
	}
}
