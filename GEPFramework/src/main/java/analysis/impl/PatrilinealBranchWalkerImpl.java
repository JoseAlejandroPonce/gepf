/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis.impl;

import analysis.HistoricalRecord;
import analysis.Node;
import analysis.Statistics;
import analysis.Walker;
import analysis.Counter;

/**
 *
 * @author alejandro
 */
public class PatrilinealBranchWalkerImpl extends Walker{

	public PatrilinealBranchWalkerImpl(Counter anOperationsCounterImpl){
		super(anOperationsCounterImpl);
	}

	@Override
	public void walk(Node node,Statistics statistics){
        while ( node.getParentNode( 0 ) != null ) {
            operationsCounter.countOperations(node,statistics);
            node = node.getParentNode( 0 );
        }
	}

	@Override
	public void unVisitTree(Node node){
		//no need to do anything here
	}

}