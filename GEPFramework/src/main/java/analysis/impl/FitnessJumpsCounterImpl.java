/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis.impl;

import analysis.Node;
import analysis.Statistics;
import analysis.Counter;

/**
 *
 * @author alejandro
 */
public class FitnessJumpsCounterImpl implements Counter{
	
	@Override
	public void countOperations(Node node,Statistics statistics){
		statistics.totalOperations++;
		if(shouldNotCount(node,statistics))return;
		statistics.totalOperationsOfOperator++;
		Float nextGenerationFitness = getNextGenerationFitness(node,node);
		if(nextGenerationFitness == null){
			return;
		}
		if(node.getParentNode(0)!= null && node.getParentFitness(0) < nextGenerationFitness ){
			statistics.amountOfFitnessJumps++;
		}else if( node.getParentNode(1) != null && node.getParentFitness(1) < nextGenerationFitness){
			statistics.amountOfFitnessJumps++;
		}
	}

	private boolean shouldNotCount( Node node , Statistics statistics ){
		return node.getSonNode(0) == null || 
				isSelectionOperator(node)||
				node.getOperatorName().compareTo("Generation"+node.getGenerationNumber())==0 ||
				!node.wasVisited()||
				node.getOperatorName().compareTo(statistics.operatorName)!=0;
	}

	private Float getNextGenerationFitness(Node sonNode,Node analyzedNode){
		int index = 0;
		int parentIndex = 0;
		int sonIndex=0;
		while(index < sonNode.getHowManySons() && sonNode.getSonNode(index) != null ){
			if(sonNode.getSonNode(index).wasVisited()){
				sonNode= sonNode.getSonNode(index);
				if(sonNode.getGenerationNumber()!=analyzedNode.getGenerationNumber()){
					if(sonNode.getSonNode(0)!=null){
						sonNode=sonNode.getSonNode(0);
						parentIndex=determineParentIndex(sonNode);
						return sonNode.getParentFitness(parentIndex);
					}
				}
				return getNextGenerationFitness(sonNode,analyzedNode);
			}
			index++;
		}
		return null;
	}

	private int determineParentIndex(Node node){
		if(node.getSonNode(0).getParentNode(0) == node){
			return 0;
		}else if(node.getSonNode(0).getParentNode(1) == node){
			return 1;
		}
		throw new java.lang.UnsupportedOperationException();
	}
	
	public boolean isSelectionOperator(Node node){
		return node.getOperatorName().contains("Selection");
	}
}
