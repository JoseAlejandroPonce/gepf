/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis.impl;

import analysis.Node;
import analysis.Statistics;
import analysis.Walker;
import analysis.Counter;

/**
 *
 * @author alejandro
 */
public class BasicTreeWalkerImpl extends Walker{
	
	public BasicTreeWalkerImpl(Counter operationsCounter){
		super(operationsCounter);
	}
	
	@Override
	public void walk(Node node, Statistics statistics) {
		if ( !node.wasVisited() ) {
		    node.visit();
		    operationsCounter.countOperations(node,statistics);
		    if ( node.getParentNode( 0 ) != null ) {
			walk(node.getParentNode( 0 ), statistics );
		    }
		    if ( node.getParentNode( 1 ) != null ) {
			walk( node.getParentNode( 1 ) , statistics );
		    }
		    if ( node.getParentNode( 2 ) != null ) {
			System.err.println( "A node should have at most to 2 parents" );
			throw new java.lang.UnsupportedOperationException();
		    }
		}
	}

	@Override
	public void unVisitTree(Node node) {
		if ( node.wasVisited() ) {
			node.unvisit();
			if ( node.getParentNode( 0 ) != null ) {
				unVisitTree( node.getParentNode( 0 ) );
			}
			if ( node.getParentNode( 1 ) != null ) {
				unVisitTree( node.getParentNode( 1 ) );
			}
			if ( node.getParentNode( 2 ) != null ) {
				System.err.println( "A node of the HistoricalRecord should have at most 2 parents." );
				throw new UnsupportedOperationException();
			}
		}
	}
}