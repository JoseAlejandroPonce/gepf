/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis.impl;

import analysis.Statistics;
import analysis.StatisticsDisplayer;

/**
 *
 * @author alejandro
 */
public class BasicStatisticsConsoleDisplayerImpl implements StatisticsDisplayer{
	
	@Override
	public void showStatistics(Statistics statistics){
		float percentage;
		int totalOperations;
		int totalOperationsOfOperator; 
		String operatorName;
		operatorName = (String) statistics.operatorName;
		totalOperations = (int) statistics.totalOperations;
		totalOperationsOfOperator = (int) statistics.totalOperationsOfOperator;
		percentage=((float)totalOperationsOfOperator/(float)totalOperations)*100;
		System.out.println(operatorName);
		System.out.println(totalOperationsOfOperator);
		System.out.println(String.format("%.2f",percentage));
		System.out.println(statistics.amountOfFitnessJumps);
	}
}
