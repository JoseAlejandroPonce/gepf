/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis.impl;

import analysis.Node;
import analysis.Statistics;
import analysis.Counter;

/**
 *
 * @author alejandro
 */
public class BasicOperationsCounterImpl implements Counter{
	
	@Override
	public void countOperations(Node node, Statistics statistics ) {
		if ( node.getOperatorName().compareTo( "Generation" + node.getGenerationNumber() ) != 0 && node.getOperatorName().compareTo( "Initial") != 0 ) {
			statistics.totalOperations ++;
		}
		if ( node.getOperatorName().compareTo(statistics.operatorName) == 0 ) {
			statistics.totalOperationsOfOperator++;
		}
	}
}
