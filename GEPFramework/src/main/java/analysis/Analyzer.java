/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis;

import analysis.impl.BasicOperationsCounterImpl;
import analysis.impl.BasicStatisticsConsoleDisplayerImpl;
import analysis.impl.BasicTreeWalkerImpl;
import analysis.impl.FitnessJumpsCounterImpl;
import analysis.impl.PatrilinealBranchWalkerImpl;
import core.Operator;
import core.Population;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author alejandro
 */
public class Analyzer {
	
	private List<Walker> walkers;
	private StatisticsDisplayer displayer;
	private Statistics statistics;
	
	public void analyzeTree(int walker,Node node, String operatorName ){
		statistics = new Statistics(operatorName);
		walkers.get(walker).unVisitTree(node);
		walkers.get(walker).walk( node, statistics);
		displayer.showStatistics( statistics );
	}
	
	public List<Node> getTwins(Population population,float bestFitness,float e){
		List<Node> twins = new ArrayList<Node>();
		for (int i = 0;i<population.size();i++){
			if(population.getChromosome(i).getFitness() >= bestFitness - e){
				twins.add(population.getHistoricalRecord().getLastNode(i));
			}
		}
		return twins;
	}
	
	public Analyzer(){
		walkers = new ArrayList<Walker>(); 
		walkers.add(new BasicTreeWalkerImpl(new BasicOperationsCounterImpl()));
		walkers.add(new PatrilinealBranchWalkerImpl(new BasicOperationsCounterImpl()));
		walkers.add(new BasicTreeWalkerImpl(new FitnessJumpsCounterImpl()));
		displayer = new BasicStatisticsConsoleDisplayerImpl();
	}
	
	public Analyzer(List<Walker> aListOfWalkerImpl,StatisticsDisplayer aDisplayerImpl){
		this.walkers = aListOfWalkerImpl;
		this.displayer = aDisplayerImpl;
	}

	public void analyzeTheHistoricalRecord(Population population ,List<Operator> operators, Float bestFitness){
		boolean analyzeTwins = true;
		boolean analyzeTheBest = false;
		boolean analyzePatrilinealBranch = false;
		boolean analyzeFitnessJumps = false;

//		datahelper.saveGenealogicalTree( bestNode );

		int positionOfTheBestChromosome = getPositionOfTheBestChromosome(population,bestFitness);
		if(analyzeTheBest){
			Node bestNode = population.getHistoricalRecord().getLastNode(positionOfTheBestChromosome);
			for ( int oi = 0 ; oi < operators.size() ; oi++ ) {
				analyzeTree(0,bestNode,operators.get(oi).getName());
			}
		}
		if (analyzePatrilinealBranch){
			Node bestNode = population.getHistoricalRecord().getLastNode(positionOfTheBestChromosome);
			for ( int oi = 0 ; oi < operators.size() ; oi++ ) {
				analyzeTree(1,bestNode,operators.get(oi).getName());
			}
		}
		if( analyzeTwins){
			Float e = 0.01f;
			List<Node> nodes = getTwins(population,bestFitness,e);
			for (int j = 0; j< nodes.size();j++){
				System.out.println("Results for the node: " + j);
				for ( int oi = 0 ; oi < operators.size() ; oi++ ) {
					analyzeTree(2,nodes.get(j),operators.get(oi).getName());
				}
			}
			System.out.println("Ammount of twins: " + nodes.size());
		}
		if( analyzeFitnessJumps){
			Node bestNode = population.getHistoricalRecord().getLastNode(positionOfTheBestChromosome);
			for ( int oi = 0 ; oi < operators.size() ; oi++ ) {
				analyzeTree(2,bestNode,operators.get(oi).getName());
			}
		}
	}
	
	public int getPositionOfTheBestChromosome(Population population,Float bestFitness){
		int maxPos=0;
		float fitness;
		for ( int i = 0 ; i < population.size() ; i++ ) {
			fitness = population.getChromosome( i ).getFitness();
			if ( fitness >= bestFitness ) {
				bestFitness = fitness;
				maxPos = i;
			}
		}
		return maxPos;
	}
}
