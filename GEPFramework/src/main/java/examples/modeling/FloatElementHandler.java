/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package examples.modeling;

import java.util.Random;
import modeling.ElementHandler;
import modeling.ElementHandler;

/**
 * This is an example Implementation of the ElementHandler interface
 * 
 * @author Alejandro
 */
public class FloatElementHandler implements ElementHandler<Float> {

    /**
     * Generates a new Element between the bounds
     *
     * @param lowBound Minimum value allowed for an element
     * @param highBound Maximum value allowed for an element
     * @return element
     */
    @Override
    public Float generate( Float lowBound , Float highBound ) {
        Random random = new Random();
        return (float) Math.random() * (highBound - lowBound) + lowBound;
    }

    /**
     * Makes a copy of the input element
     * 
     * @param element The element to be copied
     * @return element copy of the input
     */
    @Override
    public Float copy( Float element ) {
        return new Float( element.floatValue() );
    }

    /**
     * Checks if an element's value is within the bounds
     *
     * @param element Element to be checked
     * @param lowBound Minimum value allowed for an element
     * @param highBound Maximum value allowed for an element
     * @return true if the element passes the check, or false.
     */
    @Override
    public boolean check( Float element , Float lowBound , Float highBound ) {
        return !(element < lowBound || element > highBound);
    }

}
