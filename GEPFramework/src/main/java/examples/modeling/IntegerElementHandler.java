/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package examples.modeling;

import modeling.ElementHandler;

/**
 * Implementation of the elementHandler inteface. It handles integer elements
 *
 * @author Alejandro
 */
public class IntegerElementHandler implements ElementHandler<Integer> {

    /**
     * Randomly generates a new element within the lowBound and HighBound
     * inclusive.
     *
     * @param lowBound The minimum value allowed for an Integer element
     * @param highBound The maximum value allowed for an Integer element
     * @return
     */
    @Override
    public Integer generate( Integer lowBound , Integer highBound ) {
        return (int) Math.floor( (Math.random() * (highBound - lowBound + 1)) + lowBound );
    }

    /**
     * Makes a brand new copy of the input element.
     *
     * @param element to by copied
     * @return copy of element
     */
    @Override
    public Integer copy( Integer element ) {
        return new Integer( element );
    }

    /**
     * Returns true if the element is within the bounds.
     *
     * @param element an element
     * @param lowBound The minimum value allowed for an element
     * @param highBound The minimum value allowed for an element
     * @return an Integer element
     */
    @Override
    public boolean check( Integer element , Integer lowBound , Integer highBound ) {
        return !(element < lowBound || element > highBound);
    }

}
