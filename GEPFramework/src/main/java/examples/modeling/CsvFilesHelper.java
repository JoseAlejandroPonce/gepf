/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package examples.modeling;

import analysis.Node;
import com.opencsv.CSVReader;
import modeling.DataHelper;
import core.Population;
import com.opencsv.CSVWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Data helper made to handle .csv files Made singleton to save memory space.
 *
 * @author Alejandro Ponce
 */
public class CsvFilesHelper extends DataHelper {

    private static CsvFilesHelper csvFilesHelper;

    /**
     * Constructor
     *
     * @param path Path to the working Folder
     * @param fileName Name of the file from where to retrieve data.
     */
    private CsvFilesHelper( String path , String fileName ) {
        this.setPath( path );
        this.setFileName( fileName );
    }

    /**
     * Retrieves the instance of this dataHelper. It needs the path and fileName
     * because it will create a new Instance if there isn't one already created.
     *
     * @param path Path to the working folder
     * @param nombrearchivo Name of the file from which to retrieve data.
     * @return csvFilesHelper
     */
    public static CsvFilesHelper getInstance( String path , String nombrearchivo ) {
        if ( csvFilesHelper == null ) {
            csvFilesHelper = new CsvFilesHelper( path , nombrearchivo );
        }
        return csvFilesHelper;
    }

    /**
     * Returns an instance of this dataHelper. This one will not try to create a
     * new instance if there isn't one already created. It is meant to be used
     * internally.
     *
     * @return csvFilesHelper
     */
    public static CsvFilesHelper getInstance() {
        return csvFilesHelper;
    }

    /**
     * Loads data from the file specified when creating the csvFilesHelper
     */
    private void loadData() {
        CSVReader reader = null;
        List myEntries;
        String[] aux= new String[2];
        List data=new ArrayList<Float[]>();
        
        try {
            reader = new CSVReader( new FileReader( getPath() + getFileName() ) );
        } catch ( FileNotFoundException ex ) {
            Logger.getLogger( CsvFilesHelper.class.getName() ).log( Level.SEVERE , null , ex );
        }
        try {
            myEntries = reader.readAll();
            for (int i = 0 ; i < myEntries.size() ; i++){
                aux = (String[]) myEntries.get( i );
                data.add(new Float[]{
                    Float.parseFloat( aux[0]),
                    Float.parseFloat(aux[1])
                }
                );
            }
            this.setData(data);
        } catch ( IOException ex ) {
            Logger.getLogger( CsvFilesHelper.class.getName() ).log( Level.SEVERE , null , ex );
        }
    }

    
    /**
     * Retrieves a list with the data.
     *
     * @return List of objects
     */
    @Override
    public List<Object> getData() {
        if ( data == null ) {
            this.loadData();
        }
        return data;
    }

    @Override
    /**
     * Stores the input population in a csv file using each line to store a
     * chromosmoe, with each element separated by a ",".
     *
     * @param population Population to be stored.
     * @param fileName Name of the file where to store the population
     */
    public void savePopulation( Population population , String nombre_archivo ) {
        CSVWriter writer = null;
        ArrayList<String> plainChromosome = new ArrayList<String>();
        for ( int chromosomeindex = 0 ; chromosomeindex < population.size() ; chromosomeindex++ ) {
            plainChromosome.add( population.getChromosome( chromosomeindex ).toString() );
        }
        // Convierto el cromosomaplano a un array de strings
        String[] cromosoma = new String[plainChromosome.size()];
        for ( int e = 0 ; e < plainChromosome.size() ; e++ ) {
            cromosoma[e] = plainChromosome.get( e ).toString();
        }
        try {
            writer = new CSVWriter( new FileWriter( getPath() + nombre_archivo ) , ',' , CSVWriter.NO_QUOTE_CHARACTER );
            writer.writeNext( cromosoma );
            writer.close();
        } catch ( IOException ex ) {
            Logger.getLogger( CsvFilesHelper.class.getName() ).log( Level.SEVERE , null , ex );
        }
    }

    @Override
    /**
     * Stores the genealogicaltree of a given node in a format that will be
     * handled by the graphviz tool.
     *
     * @param node Node which tree will be store.
     */
    public void saveGenealogicalTree( Node node ) {
        CSVWriter writer = null;
        try {
            writer = new CSVWriter( new FileWriter( getPath() + "historial" ) , ',' , CSVWriter.NO_QUOTE_CHARACTER );
            writer.writeNext( new String[]{ "digraph G {" } );
            saveNode( node , writer );
            writer.writeNext( new String[]{ "}" } );
            writer.close();
        } catch ( IOException ex ) {
            Logger.getLogger( CsvFilesHelper.class.getName() ).log( Level.SEVERE , null , ex );
        }

    }

    /**
     * Writes a line with the relationship between a node and its parents.
     *
     * @param nodo Node which relationships will be stored in a file
     * @param writer csvWriter that will do the file writing.
     */
    public void saveNode( Node nodo , CSVWriter writer ) {
        if ( !nodo.wasVisited() ) {
            nodo.visit();
            if ( nodo.getParentNode( 0 ) != null ) {
                saveNode( nodo.getParentNode( 0 ) , writer );
            }
            if ( nodo.getParentNode( 1 ) != null ) {
                saveNode( nodo.getParentNode( 1 ) , writer );
            }
            if ( nodo.getParentNode( 0 ) != null ) {
                writer.writeNext( new String[]{ nodo.getParentNode( 0 ).getOperatorName() + nodo.getParentNode( 0 ).hashCode() + "->" + nodo.getOperatorName() + nodo.hashCode() + ";" } );
                if ( nodo.getParentNode( 1 ) != null ) {
                    writer.writeNext( new String[]{ nodo.getParentNode( 1 ).getOperatorName() + nodo.getParentNode( 1 ).hashCode() + "->" + nodo.getOperatorName() + nodo.hashCode() + ";" } );
                }
            }
        }
    }
}
