/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package examples.modeling;

import modeling.Phenotype;
import java.awt.Dimension;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import org.math.plot.Plot2DPanel;

/**
 * Implementation of the Phenotype abstract class. Made to handle the function
 * type of phenotype.
 *
 * @author Alejandro Ponce
 */
public class Function extends Phenotype {

    private static Plot2DPanel plot = null;

    private static JFrame frame = null;

    /**
     * Draws the function using the jmathPlot library.
     *
     */
    @Override
    public void draw() {
        if ( plot == null ) {
            plot = new Plot2DPanel();
        }
        if ( frame == null ) {
            frame = new JFrame( "Elite" );
        }
        CsvFilesHelper helper = CsvFilesHelper.getInstance();

        int tamañodatos = helper.getData().size();
        ArrayList data = (ArrayList) helper.getData();
        Float[] s;
        boolean flag;

        double[] x = new double[tamañodatos];
        double[] y = new double[tamañodatos];

        double[] Y = new double[tamañodatos];
        Expression e;
        e = new ExpressionBuilder( (String) this.phenotype )
                    .variables( "x" )
                    .build();

        for ( int indicedatos = 0 ; indicedatos < data.size() ; indicedatos++ ) {

            s = (Float[]) data.get( indicedatos );
            
            e.setVariable( "x" , s[0] );
            

            x[indicedatos] =  s[0];
            Y[indicedatos] = s[1];

            y[indicedatos] = (float) e.evaluate();
            flag = true;
        }

        SwingUtilities.invokeLater( new Runnable() {

            public void run() {

                plot.removeAllPlots();
                plot.addLinePlot( "my plot" , x , y );
                plot.addLinePlot( "my plot" , x , Y );
                frame.setContentPane( plot );
                frame.setSize( new Dimension( 400 , 300 ) );
                frame.setVisible( true );
            }
        }
        );

    }
}
