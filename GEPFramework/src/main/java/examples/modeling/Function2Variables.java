/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package examples.modeling;

import modeling.Evaluation;
import modeling.Phenotype;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import modeling.DataHelper;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import org.math.plot.Plot2DPanel;
import org.math.plot.Plot3DPanel;

/**
 *
 * @author Alejandro
 */
public class Function2Variables extends Phenotype {

    private static Plot3DPanel plot = new Plot3DPanel();

    private static JFrame frame = new JFrame( "Elite" );

    /**
     *
     */
    @Override
    public void draw() {
        DataHelper helper = CsvFilesHelper.getInstance();

        int tamañodatos = helper.getData().size();
        ArrayList data = (ArrayList) helper.getData();
        String[] s;
        boolean flag;

        double[] x1 = new double[tamañodatos];
        double[] x2 = new double[tamañodatos];
        double[] y = new double[tamañodatos];

        double[] Y = new double[tamañodatos];
        Expression e;

        for ( int indicedatos = 0 ; indicedatos < data.size() ; indicedatos++ ) {

            s = (String[]) data.get( indicedatos );

            e = new ExpressionBuilder( (String) this.phenotype )
                    .variables( "x1" , "x2" )
                    .build()
                    .setVariable( "x1" , Double.parseDouble( s[0] ) )
                    .setVariable( "x2" , Double.parseDouble( s[1] ) );

            x1[indicedatos] = Double.parseDouble( s[0] );
            x2[indicedatos] = Double.parseDouble( s[1] );
            Y[indicedatos] = Double.parseDouble( s[2] );
            try {
                y[indicedatos] = (float) e.evaluate();
                flag = true;

            } catch ( java.lang.ArithmeticException ee ) {
                y[indicedatos] = y[indicedatos - 1];
                flag = false;
            }
        }

        try {

            Thread.sleep( 100 );
        } catch ( Exception ex ) {

            Logger.getLogger( Function2Variables.class.getName() ).log( Level.SEVERE , null , "Error con el thread sleep de Funcion.draw" );
        }

        // add a line plot to the PlotPanel
        SwingUtilities.invokeLater( new Runnable() {
            public void run() {
                plot.removeAllPlots();
                plot.addScatterPlot( "my plot" , x1 , x2 , y );
                plot.addScatterPlot( "my plot" , x1 , x2 , Y );
                frame.setContentPane( plot );
                frame.setSize( new Dimension( 800 , 600 ) );
                frame.setVisible( true );
            }
        } );

    }
}
