/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package examples.modeling;

import modeling.DataHelper;
import modeling.Evaluation;
import modeling.Phenotype;
import modeling.Nucleotide;
import core.Chromosome;
import static java.lang.Math.abs;
import java.util.List;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;


/**
 * This is an example of an evaluation.
 *
 * @author José Alejandro Ponce
 */
public class ExampleEvaluation extends Evaluation {

    /**
     * Constructor
     *
     * @param nucleotides List of nucleotides
     * @param acceptableFitness Minimum fitness value. End condition.
     */
    public ExampleEvaluation( Nucleotide[] nucleotides , float acceptableFitness ) {
        super( nucleotides , acceptableFitness );
    }

    /**
     * Implementation of the fitness evaluation function for a function phenotype.
     *
     * @param phenotype A phenotype element containing in this case the mathematical
     * expression to be evaluated.
     * @return fitness value for this phenotype
     */
    @Override
    public float evaluation( Phenotype phenotype ) {

	Float fitness;
        DataHelper dataHelper = CsvFilesHelper.getInstance();
        List data = dataHelper.getData();
        float partialFitness = 0;
        float evaluation;
        Float[] s;
	
        for ( int i = 0 ; i < data.size() ; i++ ) {
            s = (Float[]) data.get( i );
            Expression e = new ExpressionBuilder( (String) phenotype.phenotype )
                    .variables( "x" )
                    .build()
                    .setVariable( "x" , s[0] );
            //We evluate within a try catch due to possible math exeptions.
            try {
                evaluation = (float) e.evaluate();
            } catch ( java.lang.ArithmeticException ee ) {
                //If there was an exception then the fitness must be a bad one.
                evaluation = 1000000;
            }
            //We use the straight absolute value of the difference
            partialFitness += abs( s[1]  - evaluation );
        }
        
        fitness = (float) -partialFitness;
        return fitness;
    }

    /**
     * Expresses the chromosome into a phenotype. In this case a Function.
     * 
     * @param chromosome a chromosome
     * @return
     */
    @Override
    public Phenotype expressChromosome( Chromosome chromosome ) {
	Function function = new Function();
	function.setPhenotype("10");
        return function;
    }
}
