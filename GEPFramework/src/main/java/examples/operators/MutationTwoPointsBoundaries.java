/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package examples.operators;

import core.Chromosome;
import core.Gene;
import core.Mutation;
import core.Domain;
import java.util.Random;

/**
 *
 * @author José Alejandro Ponce
 */
public class MutationTwoPointsBoundaries extends Mutation {

    public MutationTwoPointsBoundaries(char c) {

        super(c, "MutationTwoPointsBoundaries");
    }

    @Override
    public Chromosome operate(Chromosome operando) {

        Chromosome result = new Chromosome(operando);

        Domain domain1;
        Domain domain2;
        Gene gene1;
        Gene gene2;

        int domainIndex1, domainIndex2;
        int geneIndex1, geneIndex2;
        int elementIndex1, elementIndex2;

        Random random = new Random();

        geneIndex1 = random.nextInt(operando.size());
        geneIndex2 = random.nextInt(operando.size());

        gene1 = result.getGene(geneIndex1);
        gene2 = result.getGene(geneIndex2);

        domainIndex1 = random.nextInt(gene1.size());
        domainIndex2 = random.nextInt(gene2.size());

        domain1 = gene1.getDomainI(domainIndex1);
        domain2 = gene2.getDomainI(domainIndex2);

        elementIndex1 = random.nextInt(domain1.size());
        elementIndex2 = random.nextInt(domain2.size());

        float x = random.nextFloat();

        if (x < 0.5) {
            domain1.setElement(domain1.getLowBound(),elementIndex1);
            domain2.setElement(domain2.getLowBound(),elementIndex2);
        } else if (x >= 0.5) {
            domain1.setElement( domain1.getHighBound(),elementIndex1);
            domain2.setElement( domain2.getHighBound(),elementIndex2);
        }
        return result;
    }
}
