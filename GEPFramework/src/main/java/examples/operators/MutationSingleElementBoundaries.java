/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package examples.operators;

import core.Chromosome;
import core.Gene;
import core.Mutation;
import core.Domain;
import java.util.Random;

/**
 *
 * @author José Alejandro Ponce
 */
public class MutationSingleElementBoundaries extends Mutation {

    public MutationSingleElementBoundaries(char c) {
        super(c, "MutationSingleElementBoundaries");
    }

    @Override
    public Chromosome operate(Chromosome operando) {

        //Copy of the input chromosome
        Chromosome result = new Chromosome(operando);
        
        Domain domain;
        Gene gene;
        
        int domainIndex;
        int geneIndex;

        Random random = new Random();

        //Retrieve the gene 
        geneIndex = random.nextInt(result.size());
        gene = result.getGene(geneIndex);
        //Retrieve the domain
        domainIndex = random.nextInt(gene.size());
        domain = gene.getDomainI(domainIndex);

        int elementIndex;

        elementIndex = random.nextInt(domain.size());

        float x = random.nextFloat();

        if (x < 0.5) {
            domain.setElement( domain.getLowBound(),elementIndex);
        } else if (x >= 0.5) {
            domain.setElement( domain.getHighBound(),elementIndex);
        }
        return result;

    }

}
