/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package examples.operators;

import core.Chromosome;
import core.Gene;
import core.Mutation;
import core.Domain;
import java.util.Random;

/**
 *
 * @author alejandro
 */
public class MutationRandomElementInsertion extends Mutation{

    public MutationRandomElementInsertion(char modo) {
        super(modo,"MutationRandomElementInsertion");
    }

    @Override
    /**
     * Inserts a randomly generated element into a randomly chosen position.
     */
    public Chromosome operate(Chromosome operando) {
        Chromosome result=new Chromosome(operando);
        Domain domain;
        Gene gene;
        
        int domainIndex;
        int geneIndex;
        int elementIndex;
        
        Random random = new Random();
        
        geneIndex = random.nextInt(result.size());
        gene = result.getGene(geneIndex);
        domainIndex = random.nextInt(gene.size());
        domain=gene.getDomainI(domainIndex);
        elementIndex = random.nextInt(domain.size());
        
        Object nuevoElemento;
        nuevoElemento = domain.generateElement();
        
        domain.setElement( nuevoElemento,elementIndex);
        
        return result;
    }
    
}
