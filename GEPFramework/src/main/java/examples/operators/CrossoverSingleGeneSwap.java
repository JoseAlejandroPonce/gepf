/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package examples.operators;

import core.Chromosome;
import core.Crossover;
import core.Gene;
import core.Domain;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author alejandro
 */
public class CrossoverSingleGeneSwap extends Crossover {

    public CrossoverSingleGeneSwap(char c) {
        super(c, "CrossoverSingleGeneSwap");
    }

    @Override
    public List<Chromosome> operate(List<Chromosome> operando) {
        List<Chromosome> chromosomesList = (List<Chromosome>) operando;
        List<Chromosome> sons = new ArrayList<Chromosome>();

        sons.add(new Chromosome(chromosomesList.get(0)));
        sons.add(new Chromosome(chromosomesList.get(0)));
        Random random = new Random();
        Integer elementAux;
        
        int geneIndex;

        geneIndex = random.nextInt(sons.get(0).size() - 1);

        Gene geneAux = sons.get(0).getGene(geneIndex);

        sons.get(0).setGen(sons.get(1).getGene(0), geneIndex);
        sons.get(1).setGen(geneAux, geneIndex);

        return sons;
    }
}
