/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package examples.operators;

import analysis.Node;
import core.Chromosome;
import core.Population;
import core.Selection;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author alejandro
 */
public class SelectionRoulette extends Selection {

    /**
     * Selection operator based on the classical roulette method.
     */
    public SelectionRoulette() {
        super( "SelectionRoulette" );
    }

    @Override
    public Boolean operate( List<Object> arguments ) {

        // random numbers generator.
        Random random = new Random();

        //
        Population originPopulation = (Population) arguments.get( 0 );
        Population destinationPopulation = (Population) arguments.get( 1 );

        int[] originPopulationParameters = (int[]) arguments.get( 2 );
        int[] destinationPopulationParameters = (int[]) arguments.get( 3 );

        float value = (float) arguments.get( 4 );

        int currentGeneration = (int) arguments.get( 5 );

        boolean historial = (boolean) arguments.get( 6 );

        // Make an actual copy of the originPopulation
        Population auxPop = new Population( originPopulation );

        //Extract the list of chromosomes from auxPop
        List<Chromosome> chromosomesList = auxPop.getchromosomes();

        // Auxiliar list of chromosomes where to place the selected chromosomes
        List<Chromosome> result = new ArrayList<Chromosome>();
        //chromosome index and chromosome index destination.
        int ci, cid;

        //Remove chromosomes from chromosomesList based on the originPopulationParameters array
        for ( ci = originPopulation.size() - 1 ; ci > originPopulationParameters[2] - 1 ; ci-- ) {
            chromosomesList.remove( ci );
        }
        if ( originPopulationParameters[1] != 0 ) {
            for ( ci = originPopulationParameters[1] - 1 ; ci >= 0 ; ci-- ) {
                chromosomesList.remove( ci );
            }
        }

        float[] F = new float[chromosomesList.size()]; //fitness de individuos 
        float[] F_min = new float[chromosomesList.size()]; //fitness normalizado
        float[] P = new float[chromosomesList.size()]; //probabilidad / ruleta
        float sum = 0, min = 0;

        //Find the minimum value
        F[0] = min = chromosomesList.get( 0 ).getFitness();
        for ( int i = 0 ; i < chromosomesList.size() ; i++ ) {
            F[i] = chromosomesList.get( i ).getFitness();
            if ( F[i] < min ) {
                min = F[i];
            }
        }

        //sustract the minimum value and add a 
        for ( int i = 0 ; i < chromosomesList.size() ; i++ ) {
            F_min[i] = F[i] - min + 0.001f;
            sum += F_min[i];
        }

        //Calculate probability
        for ( int i = 0 ; i < chromosomesList.size() ; i++ ) {
            P[i] = F_min[i] / sum;
        }

        //Calculate accumulated probability
        for ( int i = 1 ; i < chromosomesList.size() ; i++ ) {
            P[i] = P[i] + P[i - 1];
        }

        int inserted = 0, inf, sup, center;

        float pelotita = 0;
        boolean sigue = false;
        //List<chromosomes> auxiliarsito=new ArrayList<chromosomes>();

        int seleccionados = java.lang.Math.round( destinationPopulation.size() * value );
        for ( int i = 0 ; i < seleccionados ; i++ ) {

            inf = 0;
            sup = P.length - 1;
            center = (inf + sup) / 2;
            pelotita = random.nextFloat();
            sigue = true;

            while ( sigue ) {
                center = (inf + sup) / 2;
                if ( pelotita < P[center] ) {
                    if ( sup == center ) {
                        sigue = false;
                    }
                    sup = center;
                } else {
                    if ( inf == center ) {
                        sigue = false;
                    }
                    inf = center;
                }
            }
            //Add a copy of the selected chromosome to the results
            result.add( new Chromosome( chromosomesList.get( center ) ) );
            result.get( inserted ).setFitness( F[center] );
            result.get( inserted ).setValidFitness( true );

            ci = center + originPopulationParameters[1];
            cid = inserted + destinationPopulationParameters[1];

            //If the historial flag is set. Add a new node to the historial
            if ( historial ) {
                Node parentNode = originPopulation.getHistoricalRecord().getLastNode( ci );
                if ( parentNode.getOperatorName().compareTo( "SelectionRoulette" ) == 0 ) {
                    parentNode = originPopulation.getHistoricalRecord().getLastNodeOfGeneration( currentGeneration , center + originPopulationParameters[1] );
                }
                destinationPopulation.getHistoricalRecord().addNode(
                        parentNode ,
                        F[center] ,
                        "SelectionRoulette" ,
                        currentGeneration ,
                        cid
                );
            }
            inserted++;
        }

        if ( destinationPopulation.size() < result.size() ) {
            System.err.println( "SelectionRoulette: The size of the selected chromosomesList is bigger than the destination population." );
        }

        for ( int i = destinationPopulationParameters[1] ; i < destinationPopulationParameters[1] + result.size() ; i++ ) {
            destinationPopulation.setChromosome( result.get( i - destinationPopulationParameters[1] ) , i );
        }

        return true;
    }

}
