/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package examples.operators;

import core.Chromosome;
import core.Crossover;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author alejandro
 */
public class CrossoverTwoPoints extends Crossover {

    public CrossoverTwoPoints(char c) {
        super(c, "CrossoverTwoPoints");
    }

    @Override
    public List<Chromosome> operate(List<Chromosome> operando) {
        List<Chromosome> chromosomes = (List<Chromosome>) operando;
        List<Chromosome> hijos = new ArrayList<Chromosome>();

        hijos.add(new Chromosome(chromosomes.get(0)));
        hijos.add(new Chromosome(chromosomes.get(1)));
        Random random = new Random();
        Object elementAux;
        
        int geneIndex1, domainIndex1, elementIndex1, geneIndex2, domainIndex2, elementIndex2,eimax,pimax,gimax;
        int aux;
        geneIndex1 = random.nextInt( hijos.get( 0 ).size() );
        geneIndex2 = random.nextInt( hijos.get( 1 ).size() );
        if ( geneIndex2 < geneIndex1 ){
            aux = geneIndex1;
            geneIndex1 = geneIndex2;
            geneIndex2 = aux;
        } 
        domainIndex1 = random.nextInt(hijos.get(0).getGene( geneIndex1).size() );
        domainIndex2 = random.nextInt(hijos.get(0).getGene( geneIndex2).size() );
        if ( geneIndex1 == geneIndex2 ){
            if (domainIndex2 < domainIndex1){
                aux = domainIndex1;
                domainIndex1 = domainIndex2;
                domainIndex2 = aux;
            }
        }
        elementIndex1 = random.nextInt(hijos.get(0).getDomainI(geneIndex1,domainIndex1).size());
        elementIndex2 = random.nextInt(hijos.get(0).getDomainI(geneIndex2,domainIndex2).size());
        if ( geneIndex1 == geneIndex2 && domainIndex1 == domainIndex2){
            if (elementIndex2 < elementIndex1 ){
                aux = elementIndex1;
                elementIndex1 = elementIndex2;
                elementIndex2 = aux;
            }
        }
        for (int geneIndex = geneIndex1 ; geneIndex <= geneIndex2 ; geneIndex++){
            if (geneIndex == geneIndex2){
                pimax = domainIndex2;
            } else {
                pimax = hijos.get(0).getGene( geneIndex).size();
            }
            for (int domainIndex = domainIndex1; domainIndex < pimax ; domainIndex++){
                if (domainIndex == domainIndex2) {
                    eimax = elementIndex2;
                } else {
                    eimax = hijos.get(0).getDomainI(geneIndex,domainIndex).size();
                }
                for (int elementIndex = elementIndex1; elementIndex < eimax; elementIndex++) {
                    elementAux = hijos.get(0).getDomainI(geneIndex,domainIndex).copy(hijos.get( 0 ).getElement( geneIndex, domainIndex, elementIndex ));
                    hijos.get(0).setElement( hijos.get(0).getDomainI(geneIndex,domainIndex).copy( hijos.get( 1 ).getElement( geneIndex, domainIndex, elementIndex )), geneIndex, domainIndex, elementIndex );
                    hijos.get(1).setElement( elementAux, geneIndex, domainIndex, elementIndex );
                }
            }
        }
        return hijos;
    }
}
