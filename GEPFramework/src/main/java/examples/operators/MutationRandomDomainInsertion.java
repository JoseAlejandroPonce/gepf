/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package examples.operators;

import core.Chromosome;
import core.Gene;
import core.Mutation;
import core.Domain;
import java.util.Random;

/**
 * Replaces a domain with a new one randomly generated.
 *
 * @author José Alejandro Ponce
 */
public class MutationRandomDomainInsertion extends Mutation {

    public MutationRandomDomainInsertion(char modo) {
        super(modo, "MutationRandomDomainInsertion");
    }

    @Override
    public Chromosome operate(Chromosome operando) {

        Chromosome result;
        Random rand = new Random();
        int geneIndex, domainIndex;
        Gene geneAux;
        Domain domainAux;
        Domain newDomain;
        result = new Chromosome(operando);

        geneIndex = rand.nextInt(result.size());
        geneAux = result.getGene(geneIndex);

        domainIndex = rand.nextInt(geneAux.size());
        domainAux = geneAux.getDomainI(domainIndex);

        newDomain=new Domain(
                 domainAux.getName()
                ,domainAux.size()
                ,domainAux.getLowBound()
                ,domainAux.getHighBound()
                ,domainAux.getElementHandler()
        );

        result.setDomainI(newDomain, geneIndex, domainIndex);

        return result;
    }
}
