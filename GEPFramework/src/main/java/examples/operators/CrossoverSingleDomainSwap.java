/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package examples.operators;

import core.Chromosome;
import core.Crossover;
import core.Domain;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author alejandro
 */
public class CrossoverSingleDomainSwap extends Crossover {

    public CrossoverSingleDomainSwap(char c) {
        super(c, "CrossoverSingleDomainSwap");
    }

    @Override
    public List<Chromosome> operate(List<Chromosome> fathers) {
        List<Chromosome> chromosomes = (List<Chromosome>) fathers;
        List<Chromosome> hijos = new ArrayList<Chromosome>();

        hijos.add(new Chromosome(chromosomes.get(0)));
        hijos.add(new Chromosome(chromosomes.get(1)));
        Random random = new Random();
        Integer elementAux;
        int geneIndex, domainIndex, elementIndex;

        geneIndex = random.nextInt(hijos.get(0).size());

        domainIndex = random.nextInt(hijos.get(0).getGene(geneIndex).size());

        Domain domainAux = hijos.get(1).getDomainI(geneIndex, domainIndex);

        hijos.get(1).setDomainI(hijos.get(0).getDomainI(geneIndex, domainIndex), geneIndex, domainIndex);
        hijos.get(0).setDomainI(domainAux, geneIndex, domainIndex);

        return hijos;
    }
}
