/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package examples.operators;

import core.Chromosome;
import core.Gene;
import core.Domain;
import core.Permutation;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author alejandro
 */
public class PermutationTranspositionIS extends Permutation{

    public PermutationTranspositionIS(char modo) {
        super(modo, "PermutationTranspositionIS");
    }

    @Override
    public Chromosome operate(Chromosome chromosome) {
        Random rand = new Random();
        Domain headAux;
        Domain tailAux;
        Gene geneAux;
        int firstei;
        int lastei;
        int destinationIndex;
        int geneIndex;
        int max;
        List<Integer> elementsList = new ArrayList<Integer>();
        List<Gene> usableGenesList = new ArrayList<Gene>();
        
        Chromosome result = new Chromosome(chromosome);
        boolean flag = false;
        for (int i = 0; i < result.size(); i++) {
            if (result.getGene(i).getName() == "GEP-GENE") {
                usableGenesList.add(result.getGene(i));
                flag = true;
                break;
            }
        }
        if (flag == false) {
            System.err.println("TranspositionRISPermutation: The chromosome doesn't"
                    + "have a GEP-GENE, this operator cannot be used.");
            throw new java.lang.UnsupportedOperationException();
        }
        
        geneIndex = rand.nextInt(usableGenesList.size());
        geneAux = usableGenesList.get(geneIndex);
        
        geneAux = result.getGene( 0 );
        headAux = geneAux.getDomainI( 0 );
        tailAux = geneAux.getDomainI( 1 );
        
        //generate indexes for the first and last elements
        firstei = rand.nextInt(headAux.size());
        lastei = rand.nextInt( tailAux.size()+headAux.size() -firstei )+firstei;
        
        //Extract the secuence
        if( lastei >= headAux.size()){
            for ( int elementIndex = firstei ; elementIndex < headAux.size() ; elementIndex++ ){
                elementsList.add((int) headAux.getElement(elementIndex));
            }
            for ( int elementIndex = 0 ; elementIndex <= lastei-headAux.size();elementIndex++){
                elementsList.add((int) tailAux.getElement(elementIndex));
            }
        }else {
            for ( int elementIndex = 0 ; elementIndex < lastei;elementIndex++){
                elementsList.add((int) headAux.getElement(elementIndex));
            }
        }
        
        //generate the destination index 
        if (firstei == 0 ){
            destinationIndex = 0;
        } else {
            destinationIndex = rand.nextInt(firstei);
        }
        //Concatenate the elements from destination to make the shift
        for ( int elementIndex = destinationIndex ; elementIndex < headAux.size() ; elementIndex++){
            elementsList.add((int) headAux.getElement(elementIndex));
        }
        
        //Insert the secuence
        for ( int elementIndex = destinationIndex ; elementIndex < headAux.size() ; elementIndex++){    
            headAux.setElement(elementsList.get(elementIndex-destinationIndex),elementIndex);
        }
        
        return result;
    }
}
