/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package examples.operators;

import core.Chromosome;
import core.Crossover;
import core.Gene;
import core.Domain;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author José Alejandro Ponce
 */
public class CrossoverSinglePoint extends Crossover {

    public CrossoverSinglePoint(char c) {
        super(c, "CrossoverSinglePoint");
    }

    @Override
    /**
     * The well known Single Point Crossover. Takes two chromosomes and swaps 
     * all the elements from a randomly chosen position to the end.
     */
    public List<Chromosome> operate(List<Chromosome> operando) {
        List<Chromosome> chromosomes = (List<Chromosome>) operando;
        List<Chromosome> sons = new ArrayList<Chromosome>();

        sons.add(new Chromosome(chromosomes.get(0)));
        sons.add(new Chromosome(chromosomes.get(0)));
        Random random = new Random();
        Integer elementAux;
        int geneIndex, domainIndex, elementIndex;
        
        geneIndex = random.nextInt(sons.get(0).size());

        domainIndex = random.nextInt(sons.get(0).getGene(geneIndex).size());

        elementIndex = random.nextInt(sons.get(0).getDomainI(geneIndex, domainIndex).size());

        Domain parte0 = new Domain(chromosomes.get(0).getDomainI(geneIndex, domainIndex));
        Domain parte1 = new Domain(chromosomes.get(1).getDomainI(geneIndex, domainIndex));

        for (int eih = elementIndex; eih < parte0.size(); eih++) {

            sons.get(0).setElement(parte1.getElement(eih), geneIndex, domainIndex, eih);
            sons.get(1).setElement(parte0.getElement(eih), geneIndex, domainIndex, eih);
        }
        domainIndex = domainIndex + 1;
        Domain domainAux;
        for (int pih = domainIndex; pih < chromosomes.get(0).getGene(geneIndex).size(); pih++) {
            domainAux = new Domain(chromosomes.get(1).getDomainI(geneIndex, pih));
            sons.get(0).setDomainI(domainAux, geneIndex, pih);
            sons.get(1).setDomainI(new Domain(chromosomes.get(0).getDomainI(geneIndex, pih)), geneIndex, pih);
        }

        for (int gih = geneIndex + 1; gih < chromosomes.get(0).size(); gih++) {

            sons.get(0).setGen(new Gene(chromosomes.get(1).getGene(gih)), gih);
            sons.get(1).setGen(new Gene(chromosomes.get(0).getGene(gih)), gih);
        }
        return sons;
    }
}
