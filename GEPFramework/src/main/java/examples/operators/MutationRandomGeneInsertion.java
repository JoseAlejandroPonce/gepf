/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package examples.operators;

import core.Chromosome;
import core.Gene;
import core.Mutation;
import core.Domain;
import java.util.Random;

/**
 *
 * @author alejandro
 */
public class MutationRandomGeneInsertion extends Mutation {

    public MutationRandomGeneInsertion(char modo) {
        super(modo, "MutationRandomGeneInsertion");
    }

    @Override
    public Chromosome operate(final Chromosome operando) {
        Chromosome result;
        int geneIndex;
        Random rand = new Random();
        Gene geneAux;
        Domain newDomain;
        Domain domainAux;

        result = new Chromosome(operando);

        geneIndex = rand.nextInt(result.size());
                
        geneAux = new Gene(operando.getGene(geneIndex));

        //Replace the chosen gene with the random one.
        result.setGen(geneAux, geneIndex);

        return result;
    }
}
