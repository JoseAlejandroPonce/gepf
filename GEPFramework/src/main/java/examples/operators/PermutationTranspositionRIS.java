/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package examples.operators;

import modeling.Evaluation;
import core.Chromosome;
import core.Gene;
import core.Domain;
import core.Permutation;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author alejandro
 */
public class PermutationTranspositionRIS extends Permutation {

    public PermutationTranspositionRIS(char modo) {
        super(modo, "PermutationTranspositionRIS");
    }

    @Override
    public Chromosome operate(Chromosome cromosoma) {
        Random rand = new Random();
        Chromosome result = new Chromosome(cromosoma);
        Gene geneAux;
        Domain domainAux = null;
        int geneIndex;
        
        List<Integer> functionsIndexList = new ArrayList<Integer>();

        ArrayList<Gene> usableGenes = new ArrayList<Gene>();
        boolean flag = false;
        for (int i = 0; i < result.size(); i++) {
            if (result.getGene(i).getName() == "GEP-GENE") {
                usableGenes.add(result.getGene(i));
                flag = true;
                break;
            }
        }
        if (flag == false) {
            System.err.println("TranspositionRISPermutation: The chromosome doesn't"
                    + "have a GEP-GENE, this operator cannot be used.");
            throw new java.lang.UnsupportedOperationException();
        }

        geneIndex = rand.nextInt(usableGenes.size());
        geneAux = usableGenes.get(geneIndex);
        for (int domainIndex = 0; domainIndex < geneAux.size(); domainIndex++) {
            if (geneAux.getDomainI(domainIndex).getName() == "Head") {
                domainAux = geneAux.getDomainI(domainIndex);
            }
        }
        if (domainAux == null) {
            System.err.println("The Gene doesn't have a Head domain, this should"
                    + "not happen.");
            throw new java.lang.UnsupportedOperationException();
        }
        for (int elementIndex = 1; elementIndex < domainAux.size(); elementIndex++) {
            if (Evaluation.nucleotides[(int) domainAux.getElement(elementIndex)].getArity()>1) {
                functionsIndexList.add(elementIndex);
            }
        }
        //If there are not any functions. Do nothing
        if (functionsIndexList.isEmpty()) {
            return result;
        }
        //Obtain the first element
        int indexOfIndexes = rand.nextInt(functionsIndexList.size());
        int firstElement = functionsIndexList.get(indexOfIndexes);

        int lastElement = rand.nextInt(domainAux.size() - firstElement) + firstElement;

        //Create the secuence with the elements
        List<Integer> elementos = new ArrayList<Integer>();

        for (int elementIndex = firstElement; elementIndex < lastElement; elementIndex++) {
            elementos.add((Integer) domainAux.getElement(elementIndex));
        }

        //Concatenate the List of elements with the rest of the domain.
        for (int elementIndex = 0; elementIndex < domainAux.size(); elementIndex++) {
            if (elementos.size() == domainAux.size()) {
                break;
            }
            elementos.add((Integer) domainAux.getElement(elementIndex));
        }
        //Performs the transposition.
        for (int elementIndex = 0; elementIndex < domainAux.size(); elementIndex++) {
            domainAux.setElement( elementos.get(elementIndex),elementIndex);
        }

        return result;
    }
}
