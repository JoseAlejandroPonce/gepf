/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package examples.operators;

import core.Chromosome;
import core.Domain;
import core.Permutation;
import java.util.Random;

/**
 *
 * @author Alejandro
 */
public class PermutationDomainInversion extends Permutation {

    public PermutationDomainInversion(char modo) {
        super(modo, "PermutationDomainInversion");
    }

    @Override
    public Chromosome operate(Chromosome cromosoma) {
        int geneIndex, domainIndex;

        String nombreparte;
        Random random = new Random();
        Chromosome result = new Chromosome(cromosoma);

        geneIndex = random.nextInt(result.size());
        domainIndex = random.nextInt(result.getGene(geneIndex).size());
        Domain domain = result.getDomainI(geneIndex,domainIndex);
        Domain domainAux = new Domain(result.getDomainI(geneIndex, domainIndex));
       

        for (int elementIndex = 0; elementIndex < (domain.size()-1)/2; elementIndex++) {
            domain.setElement(domainAux.getElement(domainAux.size()-1-elementIndex),elementIndex );
        }

        return result;
    }

}
