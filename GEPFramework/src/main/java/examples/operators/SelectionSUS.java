/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package examples.operators;

import analysis.Node;
import core.Chromosome;
import core.Population;
import core.Selection;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author alejandro
 */
public class SelectionSUS extends Selection {

    /**
     * Selection operator based on the Stochastic Universal Sampling method
     */
    public SelectionSUS() {
        super( "SelectionSUS" );
    }

    @Override
    public Boolean operate( List<Object> arguments ) {

        Random random = new Random();

        boolean all_have_same_fitness;

        Population origin = (Population) arguments.get( 0 );
        Population destination = (Population) arguments.get( 1 );

        int[] originPopulationArray = (int[]) arguments.get( 2 );
        int[] destinationPopulationArray = (int[]) arguments.get( 3 );

        float value = (float) arguments.get( 4 );

        int currentGeneration = (int) arguments.get( 5 );

        boolean historial = (boolean) arguments.get( 6 );

        Population populationAux = new Population( origin );

        List<Chromosome> chromosomes = populationAux.getchromosomes();

        List<Chromosome> result = new ArrayList<Chromosome>();

        int ci, cid;

        for ( ci = origin.size() - 1 ; ci > originPopulationArray[2] - 1 ; ci-- ) {
            chromosomes.remove( ci );
        }
        if ( originPopulationArray[1] != 0 ) {
            for ( ci = originPopulationArray[1] - 1 ; ci >= 0 ; ci-- ) {
                chromosomes.remove( ci );
            }
        }
        for ( int indice = 0 ; indice < chromosomes.size() ; indice++ ) {
            if ( chromosomes.get( indice ) != origin.getChromosome( originPopulationArray[1] + indice ) ) {
                System.err.println( "SelectionSUS: Something wrong with the copy of the population" );
            }
        }
        //Create array with the fitness, normalized fitness and probability
        float[] F = new float[chromosomes.size()]; 
        float[] F_norm = new float[chromosomes.size()]; 
        float[] P = new float[chromosomes.size()]; 
        float sum = 0, min, max;
        //Find minimum and maximum fitness values
        min = chromosomes.get( 0 ).getFitness();
        max = min;
        for ( int i = 0 ; i < chromosomes.size() ; i++ ) {
            F[i] = chromosomes.get( i ).getFitness();
            if ( F[i] < min ) {
                min = F[i];
            }
            if ( F[i] > max ) {
                max = F[i];
            }
        }
        //Normalize the fitness
        all_have_same_fitness = true;
        for ( int i = 0 ; i < chromosomes.size() ; i++ ) {
            F_norm[i] = F[i] - min + (max - min) / chromosomes.size();
            sum += F_norm[i];
            if ( F_norm[i] != 0 ) {
                all_have_same_fitness = false;
            }
        }
        //Calculate probability
        for ( int i = 0 ; i < chromosomes.size() ; i++ ) {
            if ( all_have_same_fitness ) {
                P[i] = 1f / chromosomes.size();
            } else {
                P[i] = F_norm[i] / sum;
            }
        }
        //Calculate the accumulated probability array
        for ( int i = 1 ; i < chromosomes.size() ; i++ ) {
            P[i] = P[i] + P[i - 1];
        }

        
        int inserted = 0, low, high, center;

        int seleccionados = java.lang.Math.round( destination.size() * value );
        float cant_seleccionados = seleccionados;
        float firstTrial = (float) (Math.random() * (1 / cant_seleccionados));
        float[] rnum = new float[seleccionados];
        for ( int i = 0 ; i < seleccionados ; i++ ) {
            rnum[i] = (float) (firstTrial + ((float) i / (float) seleccionados));
        }
        Float aux;
        int Pos = 1, I = 0;
        while ( I < seleccionados ) {
            aux = rnum[I];
            aux = P[Pos];
            if ( rnum[I] < P[Pos] ) {
                result.add( new Chromosome( origin.getChromosome( Pos ) ) );
                result.get( result.size() - 1 ).setFitness( origin.getChromosome( Pos ).getFitness() );
                result.get( result.size() - 1 ).setValidFitness( true );
                I = I + 1;

                if ( historial ) {
                    Node parentNode = origin.getHistoricalRecord().getLastNode( ci );
                    if ( parentNode.getOperatorName().compareTo( "SelectionSUS" ) == 0 ) {
                        parentNode = origin.getHistoricalRecord().getLastNodeOfGeneration( currentGeneration , Pos + originPopulationArray[1] );
                    }
                    destination.getHistoricalRecord().addNode(
                            parentNode , //Nodo padre
                            origin.getChromosome( Pos ).getFitness() , //fitness padre
                            "SelectionSUS" , //Nombre operador
                            currentGeneration , //Generation
                            result.size() - 1 //indice cromosoma destino
                    );
                }
            } else {
                Pos = Pos + 1;
                if ( Pos >= P.length ) {

                    Pos = Pos - 1;
                    result.add( new Chromosome( origin.getChromosome( Pos ) ) );
                    result.get( result.size() - 1 ).setFitness( origin.getChromosome( Pos ).getFitness() );
                    result.get( result.size() - 1 ).setValidFitness( true );
                    I = I + 1;

                    if ( historial ) {
                        Node parentNode = origin.getHistoricalRecord().getLastNode( ci );
                        if ( parentNode.getOperatorName().compareTo( "SelectionSUS" ) == 0 ) {
                            parentNode = origin.getHistoricalRecord().getLastNodeOfGeneration( currentGeneration , Pos + originPopulationArray[1] );
                        }
                        destination.getHistoricalRecord().addNode(
                                parentNode , //Nodo padre
                                origin.getChromosome( Pos ).getFitness() , //fitness padre
                                "SelectionSUS" , //Nombre operador
                                currentGeneration , //Generation
                                result.size() - 1 //indice cromosoma destino
                        );
                    }
                    I = I + 1;
                }
            }
        }

        if ( destination.size() < result.size() ) {

            System.err.println( "SelectionSUS: El tamaño de los seleccionados es superior al de la poblacion destino" );
        }

        for ( int i = destinationPopulationArray[1] ; i < destinationPopulationArray[1] + result.size() ; i++ ) {

            destination.setChromosome( result.get( i - destinationPopulationArray[1] ) , i );
        }

        return true;
    }

}
