/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package examples.operators;

import core.Chromosome;
import core.Crossover;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Jose Alejandro Ponce
 */
public class CrossoverSingleElementSwap extends Crossover {

    public CrossoverSingleElementSwap(char c) {
        super(c, "CrossoverSingleElementSwap");
    }

    @Override
    /**
     * This operator takes two chromosomes and swaps the two elements in the same 
     * randomly chosen position.
     */
    public List<Chromosome> operate(List<Chromosome> operando) {
        List<Chromosome> chromosomes = (List<Chromosome>) operando;
        List<Chromosome> hijos = new ArrayList<Chromosome>();

        hijos.add(new Chromosome(chromosomes.get(0)));
        hijos.add(new Chromosome(chromosomes.get(1)));
        Random random = new Random();
        Object elementAux;
        
        int geneIndex, domainIndex, elementIndex;
        geneIndex = random.nextInt(hijos.get(0).size());
        domainIndex = random.nextInt(hijos.get(0).getGene(geneIndex).size());
        elementIndex = random.nextInt(hijos.get(0).getDomainI(geneIndex, domainIndex).size());
        elementAux = chromosomes.get(0).getElement(geneIndex, domainIndex, elementIndex);
        
        hijos.get(0).setElement( hijos.get(1).getElement(geneIndex, domainIndex, elementIndex), geneIndex, domainIndex, elementIndex);
        hijos.get(1).setElement(elementAux, geneIndex, domainIndex, elementIndex);

        return hijos;
    }
}
