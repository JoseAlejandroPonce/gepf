/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package examples;

/**
 *
 * @author alejandro
 */
import examples.modeling.ExampleEvaluation;
import examples.modeling.Function;
import modeling.ChromosomeCoding;
import modeling.GeneCoding;
import examples.modeling.IntegerElementHandler;
import modeling.Nucleotide;
import core.Chromosome;
import examples.modeling.CsvFilesHelper;
import org.junit.Test;
import modeling.ElementHandler;

public class FunctionTest {

    @Test
    public void evaluarTestCasoCorrecto() {
	String path = "../DataSets/";
        String nombre = "Rastrigin.csv";
        CsvFilesHelper helper = CsvFilesHelper.getInstance( path , nombre );
        Function funcion = new Function();
        Nucleotide[] nucleotides = new Nucleotide[]{
        new Nucleotide( "+" , 2 ) ,
        new Nucleotide( "-" , 2 ) ,
        new Nucleotide( "*" , 2 ) ,
        new Nucleotide( "/" , 2 ) ,
        new Nucleotide( "x" , 0 ) 
        };

        ExampleEvaluation evalfun = new ExampleEvaluation( nucleotides , 0 );

        String nombrecodificacion = "GEP-GEN";

        String[] nombrepartes = new String[2];
        nombrepartes[0] = "Head";
        nombrepartes[1] = "Tail";

        Integer[] limitesuperior = new Integer[2];
        limitesuperior[0] = 3;
        limitesuperior[1] = 4;

        Integer[] limiteinferior = new Integer[2];
        limiteinferior[0] = 0;
        limiteinferior[1] = 4;

        int[] tamañopartes = new int[2];
        tamañopartes[0] = 3;

        tamañopartes[1] = tamañopartes[0] * (2 - 1) + 2;
        int cantidadpartes = 2;
        int i, elementIndex, eaux;
        ElementHandler[]element_handlers = new ElementHandler[2];
        element_handlers[0] = new IntegerElementHandler();
        element_handlers[1] = new IntegerElementHandler();

        // Creamos la codificacion para el gene
        GeneCoding codgen1;
        codgen1 = new GeneCoding(
                nombrepartes // nombrepartes: vector con los nombres de las partes
                , nombrecodificacion // nombrecodificacion: nombre de la codificacion
                , limitesuperior // limitesuperior: vector con los limites superiores de todas las partes
                , limiteinferior // limiteinferior: vector con los limites inferiores de todas las partes
                , tamañopartes // tamañopartes: vector con los tamaños de todas las  partes
                 // cantidadpartes: entero que representa la cantidad de las partes. debe 
                , element_handlers
        );

        // vamos a crear una segunda codificacion de gene
        String nombrecodificacion2 = "Link";
        String[] nombrepartes2 = new String[1];
        nombrepartes2[0] = "Link";
        Integer[] limitesup2 = new Integer[1];
        limitesup2[0] = 5;
        Integer[] limiteinf2 = new Integer[1];
        limiteinf2[0] = 5;
        int[] tamañopartes2 = new int[1];
        tamañopartes2[0] = 1;
        int cantidadpartes2 = 1;

        ElementHandler[]element_handlers2 = new ElementHandler[1];
        element_handlers2[0] = new IntegerElementHandler();

        GeneCoding codgen2 = new GeneCoding( nombrepartes2 , nombrecodificacion2 , limitesup2 , limiteinf2 , tamañopartes2 , element_handlers2 );

        GeneCoding[] codgenes = new GeneCoding[2];

        codgenes[0] = codgen1;
        codgenes[1] = codgen2;

        String nombrecodcrom = "GEP-LINK-+";

        ChromosomeCoding codcrom1 = new ChromosomeCoding( codgenes , nombrecodcrom );

        Chromosome cromosoma1 = new Chromosome( codcrom1 );

        evalfun.evaluate( cromosoma1 );

        System.out.print( cromosoma1.getFitness() );
    }
}
