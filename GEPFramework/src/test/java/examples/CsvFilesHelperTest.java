/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package examples;

import examples.modeling.CsvFilesHelper;
import java.util.List;
import org.junit.Test;

/**
 *
 * @author alejandro
 */
public class CsvFilesHelperTest {

    
    @Test
    public void CSVFilesHelperCorrectUsage() {
        String path = "../DataSets/";
        String nombre = "Rastrigin.csv";
        CsvFilesHelper helper = CsvFilesHelper.getInstance( path , nombre );

        List data = helper.getData();

        Float[] s;
        for ( int i = 0 ; i < data.size() ; i++ ) {
            s = (Float[]) data.get( i );
            System.out.print( "\n " + s[0] + "," + s[1] );
        }

    }

    /*
     * 
     */
    @Test
    public void AyudanteCsvFilesTest_GuardarPoblacion() {
        //Need a path and file name
        //Need gene coding
        //Need chromosomeCoding
        //Need population
        //Save Population
    }
}
