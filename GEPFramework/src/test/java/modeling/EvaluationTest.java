/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package modeling;

import core.Chromosome;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Matchers.any;
import org.mockito.Mockito;
import static org.mockito.Mockito.CALLS_REAL_METHODS;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

/**
 *
 * @author José Alejandro Ponce
 */
public class EvaluationTest {

	Evaluation dummyEvaluation;
	Nucleotide mockNucleotide;
	Float exampleFitness;
	Phenotype mockGoodPhenotype, mockBadPhenotype;
	Chromosome mockGoodChromosome, mockBadChromosome;

	@Before
	public void setUp() throws Exception {
		mockGoodChromosome = Mockito.mock( Chromosome.class );
		mockBadChromosome = Mockito.mock( Chromosome.class );
		mockGoodPhenotype = Mockito.mock( Phenotype.class );

		Nucleotide[] nucleotides = new Nucleotide[2];
		dummyEvaluation = spy(new dummyEvaluation( nucleotides ,(float) 10 ));
		
		doNothing().when( mockGoodChromosome ).setValidFitness(any(boolean.class));
		doNothing().when( mockBadChromosome ).setValidFitness(any(boolean.class));
		doNothing().when( mockGoodChromosome ).setFitness(any(float.class));
		doNothing().when( mockBadChromosome ).setFitness(any(float.class));

		doReturn( (float) 10.1 ).when( dummyEvaluation ).evaluation( mockGoodPhenotype );
		doReturn( (float) 0.1 ).when( dummyEvaluation ).evaluation( mockBadPhenotype );
		doReturn( mockGoodPhenotype ).when( dummyEvaluation ).expressChromosome( mockGoodChromosome );
		doReturn( mockBadPhenotype ).when( dummyEvaluation ).expressChromosome( mockBadChromosome );
		
		doCallRealMethod().when(dummyEvaluation).acceptableSolutionFound();
		
	}

	@Test
	public void evaluationTest() {
		assertEquals( (float) 10.1 , dummyEvaluation.evaluation( mockGoodPhenotype ) , 0.001 );
	}

	@Test
	public void evaluateTestWithBadChromosome() throws Exception {
		dummyEvaluation.evaluate( mockBadChromosome );

		assertTrue( !dummyEvaluation.acceptableSolutionFound());
	}

	@Test
	public void evaluateTestWithAcceptableChromosome() throws Exception {
		dummyEvaluation.evaluate( mockGoodChromosome );

		assertTrue( dummyEvaluation.acceptableSolutionFound());
	}

	@Test
	public void acceptableSolutionFoundTest() {
		assertTrue( !dummyEvaluation.acceptableSolutionFound() );
	}
}

class dummyEvaluation extends Evaluation {

	public dummyEvaluation( Nucleotide[] nucleotides , float acceptableFitness ) {
		super( nucleotides , acceptableFitness );
	}

	@Override
	public float evaluation( Phenotype phenotype ) {
		return 0;
	}

	@Override
	public Phenotype expressChromosome( Chromosome chromosome ) {
		return mock(Phenotype.class);
	}

}
