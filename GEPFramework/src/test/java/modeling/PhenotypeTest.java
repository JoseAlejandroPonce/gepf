/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeling;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author alejandro
 */
public class PhenotypeTest {
	private static Phenotype dummyPhenotype;
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	 
	@Before
	public void setUp(){
		
		dummyPhenotype = new DummyPhenotype("ThisIsAStringPhenotype");
	}
	
	@After
	public void cleanUp(){
		System.setOut( null);
	}
	
	@Test
	public void getPhenotypeTest(){
		assertEquals("ThisIsAStringPhenotype",dummyPhenotype.getPhenotype());
	}
	
	@Test
	public void setPhenotypeTest(){
		dummyPhenotype.setPhenotype( "ThisIsAnotherStringPhenotype");
		assertEquals("ThisIsAnotherStringPhenotype",dummyPhenotype.getPhenotype());
	}
	
	@Test
	public void drawTest(){
		System.setOut(new PrintStream(outContent));
		dummyPhenotype.draw();
		String result = (String) outContent.toString();
		String expectedResult = (String) dummyPhenotype.getPhenotype();
		assertEquals(result,expectedResult);
	}
	
	
	
	
	public class DummyPhenotype extends Phenotype{
		
		DummyPhenotype(String aStringPhenotype){
			phenotype = aStringPhenotype;
		}
		
		@Override
		public void draw() {
			System.out.print((String) getPhenotype());
		}
		
	}
}
