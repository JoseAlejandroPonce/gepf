/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package modeling;

import examples.modeling.FloatElementHandler;
import examples.modeling.IntegerElementHandler;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author alejandro
 */
public class GeneCodingTest {

    GeneCoding geneCoding1;
    String geneCodingName;
    String[] domainNames;
    Object[] highBounds;
    Object[] lowBounds;
    int[] domainSizes;
    ElementHandler[] elementHandlers;
    
    @Before
    public void setUp(){
	    
	geneCodingName = "GEP-GEN";
	
	domainNames = new String[3];
        domainNames[0] = "Head";
        domainNames[1] = "Tail";
        domainNames[2] = "Constantes";
	
	highBounds = new Object[3];
        highBounds[0] = 10;
        highBounds[1] = 10;
        highBounds[2] = 15f;
	
	lowBounds = new Object[3];
        lowBounds[0] = 0;
        lowBounds[1] = 6;
        lowBounds[2] = 11f;
	
	domainSizes = new int[3];
        domainSizes[0] = 7;
        domainSizes[1] = domainSizes[0] * (2 - 1) + 2;
        domainSizes[2] = 10;
	
	elementHandlers = new ElementHandler[3];
        elementHandlers[0] = new IntegerElementHandler();
        elementHandlers[1] = new IntegerElementHandler();
        elementHandlers[2] = new FloatElementHandler();
	
	geneCoding1 = new GeneCoding( domainNames , geneCodingName , highBounds , lowBounds , domainSizes  , elementHandlers );
    }

    @Test
    public void constructorTestRightUsage() {

        geneCoding1 = new GeneCoding( 
		domainNames , 
		geneCodingName , 
		highBounds , 
		lowBounds , 
		domainSizes  , 
		elementHandlers 
	);

        assertEquals( 7 , geneCoding1.getDomainSize( 0 ) );
        assertEquals( 9 , geneCoding1.getDomainSize( 1 ) );
        assertEquals( 10 , geneCoding1.getDomainSize( 2 ) );

    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void constructorTestWithNullDomainNamesArray() {
	    
        String[] nullDomainNames = null;

        geneCoding1 = new GeneCoding( 
		nullDomainNames ,
		geneCodingName ,
		highBounds , 
		lowBounds , 
		domainSizes  , 
		elementHandlers 
	);
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void constructorTestWithEmptyDomainNamesArray() {
        String[] emptyDomainNames = new String[3];

        geneCoding1 = new GeneCoding( 
		emptyDomainNames ,
		geneCodingName , 
		highBounds ,
		lowBounds ,
		domainSizes  , 
		elementHandlers 
	);
    }
    
    @Test( expected = java.lang.IllegalArgumentException.class )
    public void constructorTestWithRepeatedNamesInDomainNamesArray() {
        String[] wrongDomainNames = new String[3];
	wrongDomainNames[0] = domainNames[0];
	wrongDomainNames[1] = domainNames[1];
	wrongDomainNames[2] = domainNames[0];
        geneCoding1 = new GeneCoding( 
		wrongDomainNames ,
		geneCodingName , 
		highBounds ,
		lowBounds ,
		domainSizes  , 
		elementHandlers 
	);
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void constructorTestWithANullNameInDomainNamesArray() {
	    
        String[] wrongDomainNames = new String[3];
        domainNames[0] = "head";
        domainNames[1] = "Tail";
	
        geneCoding1 = new GeneCoding(
		wrongDomainNames , 
		geneCodingName ,
		highBounds , 
		lowBounds , 
		domainSizes  , 
		elementHandlers 
	);
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void constructorTestWithAnEmptyStringInDomainNamesArray() {
        String[] wrongDomainNames = new String[3];
        wrongDomainNames[0] = "head";
        wrongDomainNames[1] = "Tail";
        wrongDomainNames[1] = "";

        geneCoding1 = new GeneCoding( 
		wrongDomainNames ,
		geneCodingName ,
		highBounds ,
		lowBounds , 
		domainSizes  , 
		elementHandlers 
	);
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void constructorTestWithANullGeneCodingName() {

        String nullGeneCodingName = null;

        geneCoding1 = new GeneCoding( 
		domainNames ,
		nullGeneCodingName , 
		highBounds , 
		lowBounds , 
		domainSizes  , 
		elementHandlers 
	);
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void constructorTestWithAnEmptyGeneCodingName() {

        String emptyGeneCodingName = "";

        geneCoding1 = new GeneCoding( domainNames , emptyGeneCodingName , highBounds , lowBounds , domainSizes  , elementHandlers );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void constructorTestWithNullLowBoundsArray() {
        Integer[] nullLowBounds = null;

        geneCoding1 = new GeneCoding( domainNames , geneCodingName , highBounds , nullLowBounds , domainSizes  , elementHandlers );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void constructorTestWithNullHighBoundsArray() {
        Integer[] nullHighBounds = null;

        geneCoding1 = new GeneCoding( domainNames , geneCodingName , nullHighBounds , lowBounds , domainSizes  , elementHandlers );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void constructorTestWithAnInvalidLowBound() {
        Integer[] wrongLowBounds = new Integer[3];
        wrongLowBounds[0] = 0;
        wrongLowBounds[1] = -2;
        wrongLowBounds[2] = 11;

        geneCoding1 = new GeneCoding( domainNames , geneCodingName , highBounds , wrongLowBounds , domainSizes  , elementHandlers );
    }
    
    @Test( expected = java.lang.IllegalArgumentException.class )
    public void constructorTestWithMissMatchingLowBoundsAndHighBounds() {
        Integer[] wrongLowBounds = new Integer[4];
        wrongLowBounds[0] = 0;
        wrongLowBounds[1] = 6;
        wrongLowBounds[2] = 11;

        geneCoding1 = new GeneCoding( domainNames , geneCodingName , highBounds , wrongLowBounds , domainSizes  , elementHandlers );
    }
    
    @Test( expected = java.lang.IllegalArgumentException.class )
    public void constructorTestWithALowBoundGreaterThanItsHighBound() {
        Object[] wrongLowBounds = new Object[3];
        wrongLowBounds[0] = 0;
        wrongLowBounds[1] = 6;
        wrongLowBounds[2] = 20f;

        geneCoding1 = new GeneCoding( domainNames , geneCodingName , highBounds , wrongLowBounds , domainSizes  , elementHandlers );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void constructorTestWithAnInvalidHighBound() {
        Integer[] wrongHighBounds = new Integer[3];
        wrongHighBounds[0] = -1;
        wrongHighBounds[1] = 10;
        wrongHighBounds[2] = 15;
	
        geneCoding1 = new GeneCoding( domainNames , geneCodingName , wrongHighBounds , lowBounds , domainSizes  , elementHandlers );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void constructorTestWithNullDomainSizesArray() {
        int[] nullDomainSizes = null;
        geneCoding1 = new GeneCoding( domainNames , geneCodingName , highBounds , lowBounds , nullDomainSizes  , elementHandlers );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void constructorTestWithADomainSizeLesserThanZero() {
        int[] wrongDomainSizes = new int[3];
        wrongDomainSizes[0] = 7;
        geneCoding1 = new GeneCoding( domainNames , geneCodingName , highBounds , lowBounds , wrongDomainSizes  , elementHandlers );
    }

    @Test
    public void getDomainNameTest() {

        assertEquals("Head" , geneCoding1.getDomainName( 0 ) );
        assertEquals("Tail" , geneCoding1.getDomainName( 1 ) );
    }

    @Test( expected = IndexOutOfBoundsException.class )
    public void getDomainNameTestWithAnIndexLessThanZero() {

        String ResultadoEsperado = "Head";
        assertEquals(ResultadoEsperado , geneCoding1.getDomainName( -1 ) );
    }

    @Test( expected = IndexOutOfBoundsException.class )
    public void getDomainNameTestWithIndexGreaterThanTheAmmountOfDomains() {

        String ResultadoEsperado = "Head";
        assertEquals(ResultadoEsperado , geneCoding1.getDomainName(geneCoding1.size() ) );
    }

    @Test
    public void getGeneCodingNameTestRightUsage() {

        String expResult = "GEP-GEN";
        String result = geneCoding1.getName();
        assertEquals( expResult , result );
    }

    @Test
    public void geneCodingtestGetLimites() {
        Object[] LimitesEsperados = new Object[2];
        LimitesEsperados[0] = 0;
        LimitesEsperados[1] = 10;

        Object[] result =  geneCoding1.getBounds( 0 );

        assertArrayEquals( LimitesEsperados , result );
    }

    @Test( expected = IndexOutOfBoundsException.class )
    public void getBoundsTestWithIndexLessThanZero() {
        Integer[] expectedResult = new Integer[2];
        expectedResult[0] = 0;
        expectedResult[1] = 10;

        Integer[] result = (Integer[]) geneCoding1.getBounds( -1 );

        assertArrayEquals( expectedResult , result );
    }

    @Test( expected = IndexOutOfBoundsException.class )
    public void getBoundsTestWithIndexGreaterThanTheAmmountOfDomains() {
        Integer[] expectedResults = new Integer[2];
        expectedResults[0] = 0;
        expectedResults[1] = 10;

        Integer[] result = (Integer[]) geneCoding1.getBounds(geneCoding1.size() );

        assertArrayEquals( expectedResults , result );
    }

    @Test
    public void getDomainSizeTest() {
        int indice = 0;
        int expectedResult = 7;
        int result = geneCoding1.getDomainSize( indice );

        assertEquals( expectedResult , result );
    }

    @Test( expected = IndexOutOfBoundsException.class )
    public void getDomainSizeTestWithIndexLesserThanZero() {
        int indice = -1;
        int ResultadoEsperado = 5;
        int result = geneCoding1.getDomainSize( indice );
	
        assertEquals( ResultadoEsperado , result );
    }

    @Test( expected = IndexOutOfBoundsException.class )
    public void getDomainSizeTestWithIndexGreaterThanTheAmmountOfDomains() {
        int indice = geneCoding1.size();
        int ResultadoEsperado = 5;
        int result = geneCoding1.getDomainSize( indice );

        assertEquals( ResultadoEsperado , result );
    }

    @Test
    public void sizeTest() {

        int expResult = 3;
        int result = geneCoding1.size();
        assertEquals( expResult , result );
    }

}
