/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeling;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author alejandro
 */
public class NucleotideTest {
	Nucleotide nucleotide;

	@Before
	public void setUp() throws Exception {
		nucleotide = new Nucleotide("+",2);
	}

	@Test
	public void getNucleotideTest() {
		Object expResult = "+";
		Object result = nucleotide.getNucleotide();
		assertEquals((String) expResult ,(String) result );
	}

	@Test
	public void testGetArity() {
		int expResult = 2;
		int result = nucleotide.getArity();
		assertEquals( expResult , result );
	}

	@Test
	public void testSetNucleotido() {
		Object expResult = "-";
		nucleotide.setNucleotide( expResult );
		Object result =  nucleotide.getNucleotide();
		assertEquals(  expResult , result );
	}
}
