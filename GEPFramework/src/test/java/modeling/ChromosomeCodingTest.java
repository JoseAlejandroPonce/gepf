/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package modeling;

import examples.modeling.IntegerElementHandler;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author alejandro
 */
public class ChromosomeCodingTest {

    private static ChromosomeCoding codificacioncromosoma;

    private static GeneCoding codificaciongen1, codificaciongen2;

    @Before
    public void setUp() {

        String nombrecodificacion = "GEP-GEN";

        String[] nombrepartes = new String[3];
        nombrepartes[0] = "Head";
        nombrepartes[1] = "Tail";
        nombrepartes[2] = "Constantes";

        Integer[] limitesuperior = new Integer[3];
        limitesuperior[0] = 10;
        limitesuperior[1] = 10;
        limitesuperior[2] = 15;

        Integer[] limiteinferior = new Integer[3];
        limiteinferior[0] = 0;
        limiteinferior[1] = 6;
        limiteinferior[2] = 11;

        int[] tamañopartes = new int[3];
        tamañopartes[0] = 7;

        tamañopartes[1] = tamañopartes[0] * (2 - 1) + 2;
        tamañopartes[2] = 10;
        int cantidadpartes = 3;

        ElementHandler[]element_handlers = new ElementHandler[3];
        element_handlers[0] = new IntegerElementHandler();
        element_handlers[1] = new IntegerElementHandler();
        element_handlers[2] = new IntegerElementHandler();

        codificaciongen1 = new GeneCoding( nombrepartes , nombrecodificacion , limitesuperior , limiteinferior , tamañopartes  , element_handlers );

        String nombrecodificacion2 = "GEP-GEN-Homeotic";

        String[] nombrepartes2 = new String[2];
        nombrepartes2[0] = "Head";
        nombrepartes2[1] = "Tail";

        Integer[] limitesuperior2 = new Integer[2];
        limitesuperior2[0] = 7;
        limitesuperior2[1] = 7;

        Integer[] limiteinferior2 = new Integer[2];
        limiteinferior2[0] = 0;
        limiteinferior2[1] = 4;

        int[] tamañopartes2 = new int[2];
        tamañopartes2[0] = 7;

        tamañopartes2[1] = tamañopartes[0] * (2 - 1) + 2;

        int cantidadpartes2 = 2;
        ElementHandler[]element_handlers2 = new ElementHandler[2];
        element_handlers2[0] = new IntegerElementHandler();
        element_handlers2[1] = new IntegerElementHandler();

        codificaciongen2 = new GeneCoding( nombrepartes2 , nombrecodificacion2 , limitesuperior2 , limiteinferior2 , tamañopartes2, element_handlers2 );

        GeneCoding[] listacodgen = new GeneCoding[5];
        listacodgen[0] = codificaciongen1;
        listacodgen[1] = codificaciongen1;
        listacodgen[2] = codificaciongen1;
        listacodgen[3] = codificaciongen1;
        listacodgen[4] = codificaciongen2;

        codificacioncromosoma = new ChromosomeCoding( listacodgen , "CodificacionCromosomaTest" );
    }

    @After
    public void tearDown() {

    }

    @Test
    public void getGeneCodingTest() {

        int indice = 0;
        GeneCoding expResult = codificaciongen1;
        GeneCoding result = codificacioncromosoma.getGeneCoding( indice );
        assertEquals( expResult , result );
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void getGeneCodingTestWithIndexLessThanZero() {

        int indice = -1;
        GeneCoding expResult = codificaciongen1;
        GeneCoding result = codificacioncromosoma.getGeneCoding( indice );
        assertEquals( expResult , result );
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void getGeneCodingTestWithIndexGreaterThanTheAmmountOfGenes() {

        int indice = codificacioncromosoma.getGenesQuantity();
        GeneCoding expResult = codificaciongen1;
        GeneCoding result = codificacioncromosoma.getGeneCoding( indice );
        assertEquals( expResult , result );
    }

    @Test
    public void getNameTest() {

        String expResult = "CodificacionCromosomaTest";
        String result = codificacioncromosoma.getName();
        assertEquals( expResult , result );
    }

    @Test
    public void getGenesQuantityTest() {

        int expResult = 5;
        int result = codificacioncromosoma.getGenesQuantity();
        assertEquals( expResult , result );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void constructorTestWithWrongName() {

        GeneCoding[] listacodgen = new GeneCoding[5];
        listacodgen[0] = codificaciongen1;
        listacodgen[1] = codificaciongen1;
        listacodgen[2] = codificaciongen1;
        listacodgen[3] = codificaciongen1;
        listacodgen[4] = codificaciongen2;

        ChromosomeCoding testcodificacioncromosoma = new ChromosomeCoding( listacodgen ,null);
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void constructorTestWithEmptyName() {

        GeneCoding[] listacodgen = new GeneCoding[5];
        listacodgen[0] = codificaciongen1;
        listacodgen[1] = codificaciongen1;
        listacodgen[2] = codificaciongen1;
        listacodgen[3] = codificaciongen1;
        listacodgen[4] = codificaciongen2;

        ChromosomeCoding testcodificacioncromosoma = new ChromosomeCoding( listacodgen , "" );
    }
    
    @Test( expected = java.lang.IllegalArgumentException.class )
    public void constructorTestWithNullGeneCoding() {

        GeneCoding[] listacodgen = null;

        ChromosomeCoding testcodificacioncromosoma = new ChromosomeCoding( listacodgen , "CodificacionCromosomaTest" );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void CodificacionCromosomaTestConstructorCongeneCodingesConUnElementoNull() {

        GeneCoding[] listacodgen = new GeneCoding[5];
        listacodgen[0] = codificaciongen1;
        listacodgen[1] = codificaciongen1;
        listacodgen[2] = codificaciongen1;
        listacodgen[3] = codificaciongen1;

        ChromosomeCoding testcodificacioncromosoma = new ChromosomeCoding( listacodgen , "CodificacionCromosomaTest" );
    }
    
    

}
