/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis;

import static analysis.HistoricalRecordTest.historicalRecord1;
import java.io.ByteArrayOutputStream;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author alejandro
 */
public class HistoricalRecordWalkerTest {
	
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	public static HistoricalRecord historicalRecord1;
	public static HistoricalRecord historicalRecord2;

	int nodeNumber1;
	int chromosomeIndex1;
	int generationNumber1;
	float parentFitness1;

	int nodeNumber2;
	int chromosomeIndex2;
	int generationNumber2;

	@Before
	public void setUp() {
		historicalRecord1 = new HistoricalRecord( 10 , 0 );
		historicalRecord2 = new HistoricalRecord( 10 , 1 );

		nodeNumber1 = 0;
		chromosomeIndex1 = 5;
		generationNumber1 = 0;
		parentFitness1 = (float) 10;

		nodeNumber2 = 0;
		chromosomeIndex2 = 5;
		generationNumber2 = 0;
	}
	
//	@Test
//	public void unVisitTreeTestSingleParent() {
//		Node parentNode = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex1 );
//		historicalRecord1.addNode( parentNode , parentFitness1 , "Testing1" , generationNumber1 , chromosomeIndex1 );
//
//		parentNode = parentNode.getSonNode( 0 );
//		historicalRecord1.addNode( parentNode , parentFitness1 , "Testing1" , generationNumber1 , chromosomeIndex1 );
//		parentNode = parentNode.getSonNode( 0 );
//
//		historicalRecord1.walkTheGenealogicalTree( parentNode , "Testing1" );
//		historicalRecord1.unVisitTree( parentNode );
//	}
//
//	@Test
//	public void unVisitTreeTreeTestTwoParent() {
//		Node parentNode1 = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex1 );
//		int chromosomeIndex3 = 6;
//		Node parentNode2 = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex3 );
//		historicalRecord1.addNode( parentNode1 , parentFitness1 , "Testing1" , generationNumber1 , chromosomeIndex3 );
//		Node sonNode = parentNode1.getSonNode( 0 );
//		sonNode.addParentNode( parentNode2 );
//
//		historicalRecord1.walkTheGenealogicalTree( sonNode , "Testing1" );
//		historicalRecord1.unVisitTree( sonNode );
//	}
//
//	@Test(expected = java.lang.UnsupportedOperationException.class)
//	public void unVisitTreeTreeTestThreParent() {
//		Node parentNode1 = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex1 );
//		int chromosomeIndex3 = 6;
//		int chromosomeIndex4 = 7;
//		Node parentNode2 = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex3 );
//		Node parentNode3 = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex4 );
//		historicalRecord1.addNode( parentNode1 , parentFitness1 , "Testing1" , generationNumber1 , chromosomeIndex3 );
//
//		Node sonNode = parentNode1.getSonNode( 0 );
//		sonNode.addParentNode( parentNode2 );
//		historicalRecord1.walkTheGenealogicalTree( sonNode , "Testing1" );
//		sonNode.addParentNode( parentNode3 );
//		historicalRecord1.unVisitTree( sonNode );
//	}
}
