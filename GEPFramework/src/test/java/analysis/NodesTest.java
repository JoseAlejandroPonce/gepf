/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package analysis;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author alejandro
 */
public class NodesTest {
	
	public static Node node;
	int chromosomeIndex;
	int generationNumber;
	int nodeNumber;
	int populationNumber;
	float parentFitness;
	String operatorName;

    @Before
    public void setUp() {
        chromosomeIndex = 0;
        generationNumber = 0;
        nodeNumber = 0;
        populationNumber = 0;
        parentFitness = -10;
        operatorName = "First node, no operator used yet";
	
        node = new Node( 
		parentFitness , 
		chromosomeIndex , 
		generationNumber , 
		nodeNumber , 
		operatorName ,
		populationNumber
	);
    }

    /**
     * Test of getOperatorName method, of class Node.
     */
    @Test
    public void getOperatorNameTest() {
        String expResult = "First node, no operator used yet";
        String result = node.getOperatorName();
	
        assertEquals( expResult , result );
    }

    /**
     * Test of getGenerationNumber method, of class Node.
     */
    @Test
    public void getGenerationNumberTest() {
        int expResult = 0;
        int result = node.getGenerationNumber();
	
        assertEquals( expResult , result );
    }

    /**
     * Test of getIndicecromosoma method, of class Node.
     */
    @Test
    public void getChromosomeIndexTest() {
        int expResult = 0;
        int result = node.getIndicecromosoma();
	
        assertEquals( expResult , result );
    }

    /**
     * Test of getNodeNumber method, of class Node.
     */
    @Test
    public void getNodeNumberTest() {
        int expResult = 0;
        int result = node.getNodeNumber();
        assertEquals( expResult , result );
    }

    @Test
    public void addSonNodeAndGetSonNodeTest() {
        String otherOperatorName = "Testing";
        Node expResult = new Node( 
		parentFitness , 
		chromosomeIndex , 
		nodeNumber , 
		generationNumber , 
		otherOperatorName ,
		populationNumber
	);
        node.addSonNode( expResult );
        int sonNodeIndex = 0;
        Node result = node.getSonNode( sonNodeIndex );

        assertEquals( expResult , result );
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void getSonNodeTestWithIndexLessThanZero() {
        Node expResult = new Node(
		parentFitness , 
		chromosomeIndex , 
		nodeNumber , 
		generationNumber , 
		operatorName,
		populationNumber
	);
        node.addSonNode( expResult );
        int indicehijo = -1;
        Node result = node.getSonNode( indicehijo );
	
	assertEquals( result, expResult);
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void getSonNodesWithIndexGreaterThanTheAmmountOfSonNodes() {

        Node expResult = new Node( 
		parentFitness , 
		chromosomeIndex , 
		nodeNumber , 
		generationNumber , 
		operatorName,
		populationNumber
	);

        node.addSonNode( expResult );

        int indicehijo = 1;
        Node result = node.getSonNode( indicehijo );
    }
    
    @Test
    public void addParentNodeTestWithNullParentNodes(){
	    Node result = node.getParentNode( 0 );
	    Node expectedResult = null;
	    
	    assertEquals(result,expectedResult);
    }

    @Test
    public void addParentNodeAndGetParentNodeTest() {
        Node expResult = new Node( 
		parentFitness , 
		chromosomeIndex , 
		nodeNumber , 
		generationNumber , 
		operatorName ,
		populationNumber
	);
        node.addParentNode( expResult );
        int parentIndex = 0;
        Node result = node.getParentNode( parentIndex );

        assertEquals( result , expResult );
    }
    
    @Test
    public void getParentNodeTestWithNullParentNodes() {
        int parentIndex = 0;
        Node result = node.getParentNode( parentIndex );

        assertEquals( result , null );
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void getParentNodeTestWithIndexLessThanZero() {
        Node expResult = new Node( 
		parentFitness , 
		chromosomeIndex ,
		nodeNumber ,
		generationNumber ,
		operatorName , 
		populationNumber 
	);

        node.addParentNode( expResult );

        int parentIndex = -1;
        Node result = node.getParentNode( parentIndex );

        assertEquals( result , expResult );
    }

    @Test
    public void getParentNodeWithIndexGreaterThanTheAmmountOfParentNodes() {
        Node parentNode = new Node( 
		parentFitness , 
		chromosomeIndex , 
		nodeNumber , 
		generationNumber , 
		operatorName , 
		populationNumber
	);

        node.addParentNode( parentNode );
        int indicepadre = 1;
        Node result = node.getParentNode( indicepadre );
        Node expResult = null;

        assertEquals( result , expResult );
    }
    
    @Test
    public void getParentFitnessTestWithGoodIndex(){
        Float result = node.getParentFitness(0);
        Float expResult = parentFitness;

        assertEquals( result , expResult );
    }
    
    @Test(expected = java.lang.IndexOutOfBoundsException.class)
    public void getParentFitnessTestWithIndexMinorThanZero(){
	node.getParentFitness(-1);
    }
    
    @Test(expected = java.lang.IndexOutOfBoundsException.class)
    public void getParentFitnessTestWithIndexTooBig(){
	node.getParentFitness(2);
    }
    
    @Test
    public void setParentFitnessTestWithGoodIndex(){
	float newParentFitness = -5;
	Node parentNode = new Node( 
		parentFitness , 
		chromosomeIndex , 
		nodeNumber , 
		generationNumber , 
		operatorName , 
		populationNumber
	);

        node.addParentNode( parentNode );
	node.setParentFitness( newParentFitness, 0);
	float result = node.getParentFitness(0);
	    
	assertEquals(newParentFitness,result,0.1);
	    
    }
    
    @Test(expected = java.lang.UnsupportedOperationException.class)
    public void setParentFitnessTestWithNoParent(){
	    float newParentFitness = -5;
	    node.setParentFitness( newParentFitness, 0);
    }
    
    @Test
    public void setPopulationNumberAndgetPopulationNumberTest(){
	    int expectedResult = 4;
	    node.setPopulationNumber( expectedResult);
	    int result = node.getPopulationNumber();
	    
	    assertEquals(expectedResult,result);
    }
    
    @Test 
    public void visitNodeAndUnvisitNodeTest(){
	    assertTrue(!node.wasVisited());
	    node.visit();
	    assertTrue(node.wasVisited());
	    node.unvisit();
	    assertTrue(!node.wasVisited());
    }
    
    @Test( expected = java.lang.IllegalArgumentException.class)
    public void constructorTestWithChromosomeIndexMinorThanZero(){
	    Node newNode= new Node(
		parentFitness , 
		-1 , 
		generationNumber , 
		nodeNumber , 
		operatorName ,
		populationNumber
	);
    }
    
    @Test( expected = java.lang.IllegalArgumentException.class)
    public void constructorTestWithGenerationNumberMinorThanZero(){
	    Node newNode= new Node(
		parentFitness , 
		chromosomeIndex , 
		-1 , 
		nodeNumber , 
		operatorName ,
		populationNumber
	);
    }
    
    @Test( expected = java.lang.IllegalArgumentException.class)
    public void constructorTestWithNodeNumberMinorThanZero(){
	    Node newNode= new Node(
		parentFitness , 
		chromosomeIndex , 
		generationNumber , 
		-1 , 
		operatorName ,
		populationNumber
	);
    }
    
    //Todo: add more than 2 parents
    //Todo: add more than 2 sons 
    //Todo: Crossed case1
    //Crossed case2

}
