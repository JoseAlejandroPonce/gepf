/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analysis.impl;

import analysis.Analyzer;
import analysis.HistoricalRecord;
import analysis.Node;
import analysis.Statistics;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author alejandro
 */
public class FitnessJumpCounterImplTest{

	private static HistoricalRecord historicalRecord;
	List<String> operatorNames;
	FitnessJumpsCounterImpl counter;
	BasicTreeWalkerImpl walker;
	Statistics statistics;
	
	@Before
	public void setUp(){
		historicalRecord=new HistoricalRecord( 10 , 0 );
		counter = new FitnessJumpsCounterImpl();
		walker = new BasicTreeWalkerImpl(counter);
		setOperatorNames();
	}
	
	public void setOperatorNames(){
		operatorNames=new ArrayList<String>();
		operatorNames.add("SelectionSUS");
		operatorNames.add("Crossover1");
		operatorNames.add("Crossover2");
		operatorNames.add("Mutation1");
		operatorNames.add("Mutation2");
	}
	
	private Node setUpBasicHistoricalRecord(){
		Node parentNode=historicalRecord.getNode(0,0,0);
		historicalRecord.addNode(parentNode,-100000f,"Crossover1",0,0);
		historicalRecord.addNode(parentNode.getSonNode(0),-100000,"SelectionSUS",0,0);
		historicalRecord.addNewGeneration();
		Node node = historicalRecord.getLastNode(0);
		historicalRecord.addNode(node,-1000f,"Mutation1",1,0);
		node.setParentFitness(-1000,0);
		node = historicalRecord.getLastNode(0);
		historicalRecord.addNode(node,-1000f,"Mutation1",1,0);
		node = historicalRecord.getLastNode(0);
		historicalRecord.addNode(node,-1000f,"Mutation2",1,0);
		historicalRecord.getLastNode(0);
		return node;
	}
	
	private void setUpComplexHistoricalRecord(){
		Node parentNode1 = historicalRecord.getNode(0,0,0);
		Node parentNode2 = historicalRecord.getNode(0,0,1);
		Node sonNode1 = historicalRecord.addNode(parentNode1,-10000,"Crossover1",0,0);
		Node sonNode2 = historicalRecord.addNode(parentNode1,-10000,"Crossover1",0,1);
		
		historicalRecord.addEdge(parentNode2,sonNode1);
		sonNode1.setParentFitness(-10000f,1);
		historicalRecord.addEdge(parentNode2,sonNode2);
		sonNode2.setParentFitness(-10000f,1);
		
		parentNode1=sonNode1;
		parentNode2=sonNode2;
		sonNode1 = historicalRecord.addNode(parentNode1,-10000f,"Crossover2",0,0);
		sonNode2 = historicalRecord.addNode(parentNode2,-10000f,"Crossover2",0,1);
		
		historicalRecord.addEdge(parentNode2,sonNode1);
		sonNode1.setParentFitness(-10000f,1);
		historicalRecord.addEdge(parentNode1,sonNode2);
		sonNode2.setParentFitness(-10000f,1);
		
		historicalRecord.addNewGeneration();
		
		parentNode1 = sonNode1.getSonNode(0);
		parentNode2 = sonNode2.getSonNode(0);
		sonNode1 = historicalRecord.addNode(parentNode1,-1000,"Mutation1",1,0);
		sonNode2 = historicalRecord.addNode(parentNode2,-1000,"Mutation1",1,1);
		
		parentNode1 = sonNode1;
		parentNode2 = sonNode2;
		sonNode1 = historicalRecord.addNode(parentNode1,-1000f,"Mutation2",1,0);
		sonNode2 = historicalRecord.addNode(parentNode2,-1000f,"Mutation2",1,1);
		
		historicalRecord.addNewGeneration();
		
		parentNode1 = sonNode1.getSonNode(0);
		parentNode2 = sonNode2.getSonNode(0);
		
		historicalRecord.addNode(parentNode1,-100f,"Mutation2",2,0);
		historicalRecord.addNode(parentNode2,-100f,"Mutation2",2,1);
		
		historicalRecord.addNewGeneration();
	}
	
	@Test 
	public void CountTestShouldNotTestNewGenerationNode(){
		Node node=setUpBasicHistoricalRecord();
		
		statistics = new Statistics("Generation1");
		walker.walk(node,statistics);
		
		int result = statistics.amountOfFitnessJumps;
		int expectedResult = 0;
		assertEquals(result,expectedResult);
	}
	
	@Test 
	public void CountTestShouldNotTestSelectionOperator(){
		Node node=setUpBasicHistoricalRecord();
		
		statistics = new Statistics("SelectionSUS");
		walker.walk(node,statistics);
		
		int result = statistics.amountOfFitnessJumps;
		int expectedResult = 0;
		assertEquals(result,expectedResult);
	}

	@Test
	public void CountTestShouldNotCountDifferentOperatorName(){
		Node node=setUpBasicHistoricalRecord();
		
		statistics = new Statistics("Crossover2");
		walker.walk(node,statistics);
		
		int result = statistics.amountOfFitnessJumps;
		int expectedResult = 0;
		assertEquals(result,expectedResult);
	}
	
	@Test
	public void CountTestSimpleSameNameOnceShouldCount1(){
		Node node=setUpBasicHistoricalRecord();
		
		statistics = new Statistics("Crossover1");
		walker.walk(node,statistics);
		
		int result = statistics.amountOfFitnessJumps;
		int expectedResult = 1;
		assertEquals(result,expectedResult);
	}
	
	@Test
	public void CountTestSimpleWithSameNameTwiceShouldNotCount(){
		Node node=setUpBasicHistoricalRecord();
		
		statistics = new Statistics("Mutation2");
		walker.walk(node,statistics);
		
		int result = statistics.amountOfFitnessJumps;
		int expectedResult = 0;
		assertEquals( result , expectedResult );
	}
	
	@Test 
	public void countTestSimpleWithSameNameOnceShouldCount2(){
		Node node=setUpBasicHistoricalRecord();
		historicalRecord.addNewGeneration();
		node = historicalRecord.getLastNode(0);
		historicalRecord.addNode(node,-100f,"Mutation1",2,0);
		historicalRecord.addNewGeneration();
		node=historicalRecord.getLastNode(0);
		statistics = new Statistics("Mutation1");
		walker.walk(node,statistics);
		
		int result = statistics.amountOfFitnessJumps;
		int expectedResult = 2;
		assertEquals(expectedResult, result);
	}
	
	@Test
	public void countTestComplexCaseSameNameShouldCount2Crossover1(){
		setUpComplexHistoricalRecord();
		Node node = historicalRecord.getLastNode(0);
		statistics=new Statistics("Crossover1");
		walker.walk(node,statistics);
		int result = statistics.amountOfFitnessJumps;
		int expectedResult=2;
		assertEquals(expectedResult, result);
	}
	
	@Test
	public void countTestComplexCaseSameNameShouldCount1Crossover2(){
		setUpComplexHistoricalRecord();
		
		Node node = historicalRecord.getLastNode(0);
		statistics=new Statistics("Crossover2");
		walker.walk(node,statistics);
		int result = statistics.amountOfFitnessJumps;
		int expectedResult=1;
		assertEquals(expectedResult, result);
	}
	
	@Test
	public void countTestComplexCaseSameNameShouldCount1Mutation1(){
		setUpComplexHistoricalRecord();
		
		Node node = historicalRecord.getLastNode(0);
		statistics=new Statistics("Mutation1");
		walker.walk(node,statistics);
		int result = statistics.amountOfFitnessJumps;
		int expectedResult=1;
		assertEquals(expectedResult, result);
		
	}
	
	@Test
	public void countTestComplexCaseSameNameShouldCount1Mutation2(){
		setUpComplexHistoricalRecord();
		
		Node node = historicalRecord.getLastNode(0);
		statistics=new Statistics("Mutation1");
		walker.walk(node,statistics);
		int result = statistics.amountOfFitnessJumps;
		int expectedResult=1;
		assertEquals(expectedResult, result);
		
	}
}
