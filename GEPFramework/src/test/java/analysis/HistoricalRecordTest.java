/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package analysis;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author alejandro
 */
public class HistoricalRecordTest {

	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	public static HistoricalRecord historicalRecord1;
	public static HistoricalRecord historicalRecord2;

	int nodeNumber1;
	int chromosomeIndex1;
	int generationNumber1;
	float parentFitness1;

	int nodeNumber2;
	int chromosomeIndex2;
	int generationNumber2;

	@Before
	public void setUp() {
		historicalRecord1 = new HistoricalRecord( 10 , 0 );
		historicalRecord2 = new HistoricalRecord( 10 , 1 );

		nodeNumber1 = 0;
		chromosomeIndex1 = 5;
		generationNumber1 = 0;
		parentFitness1 = (float) 10;

		nodeNumber2 = 0;
		chromosomeIndex2 = 5;
		generationNumber2 = 0;
	}

	/**
	 * Test of getNode method, of class HistoricalRecord.
	 */
	@Test
	public void getNodeTest() {
		Node result = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex1 );

		assertEquals( nodeNumber1 , result.getNodeNumber() );
		assertEquals( chromosomeIndex1 , result.getIndicecromosoma() );
		assertEquals( generationNumber1 , result.getGenerationNumber() );
	}

	@Test( expected = java.lang.IndexOutOfBoundsException.class )
	public void getNodeTestWithNodeNumberLessThanZero() {
		int wrongNodeNumber = -1;
		historicalRecord1.getNode( generationNumber1 , wrongNodeNumber , chromosomeIndex1 );
	}

	@Test( expected = java.lang.IndexOutOfBoundsException.class )
	public void getNodeTestWithGenerationNumberlessThanZero() {
		int wrongGenerationNumber = -1;
		historicalRecord1.getNode( wrongGenerationNumber , nodeNumber1 , chromosomeIndex1 );
	}

	@Test( expected = java.lang.IndexOutOfBoundsException.class )
	public void getNodeTestWithGenerationNumberGreaterThanNodesSize() {
		int wrongGenerationNumber = 2;
		historicalRecord1.getNode( wrongGenerationNumber , nodeNumber1 , chromosomeIndex1 );
	}

	@Test( expected = java.lang.IndexOutOfBoundsException.class )
	public void getNodeTestWithChromosomeIndexMinorThanZero() {
		int wrongChromosomeIndex = -1;
		historicalRecord1.getNode( generationNumber1 , nodeNumber1 , wrongChromosomeIndex );
	}

	@Test( expected = java.lang.IndexOutOfBoundsException.class )
	public void getNodeTestWithChromosomeIndexGreaterThanTheAmmountOfChromosomes() {
		int wrongChromosomeIndex = 10;
		historicalRecord1.getNode( generationNumber1 , nodeNumber1 , wrongChromosomeIndex );
	}

	@Test
	public void getNodeTestWithAGreaterNodeNumber() {
		Node parentNode = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex1 );
		historicalRecord1.addNode( parentNode , (float) 10.1 , "Test" , generationNumber1 , chromosomeIndex1 );
		parentNode = parentNode.getSonNode( 0 );
		historicalRecord1.addNode( parentNode , (float) 10.1 , "Test" , generationNumber1 , chromosomeIndex1 );
		int nodeNumber = 2;

		Node expectedResult = parentNode.getSonNode( 0 );
		Node result = historicalRecord1.getNode( generationNumber1 , nodeNumber , chromosomeIndex1 );

		assertEquals( expectedResult , result );
	}

	@Test
	public void getNodeTestWithAGreaterNodeNumberButNotTheGreatest() {
		Node parentNode = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex1 );
		historicalRecord1.addNode( parentNode , (float) 10.1 , "Test" , generationNumber1 , chromosomeIndex1 );
		parentNode = parentNode.getSonNode( 0 );
		historicalRecord1.addNode( parentNode , (float) 10.1 , "Test" , generationNumber1 , chromosomeIndex1 );
		int nodeNumber = 1;

		Node expectedResult = parentNode;
		Node result = historicalRecord1.getNode( generationNumber1 , nodeNumber , chromosomeIndex1 );

		assertEquals( expectedResult , result );
	}

	@Test( expected = java.lang.IllegalArgumentException.class )
	public void getNodeTestWithANodeNumberGreaterThanTheNumberOfNodes() {
		Node parentNode = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex1 );
		historicalRecord1.addNode( parentNode , (float) 10.1 , "Test" , generationNumber1 , chromosomeIndex1 );
		parentNode = parentNode.getSonNode( 0 );
		historicalRecord1.addNode( parentNode , (float) 10.1 , "Test" , generationNumber1 , chromosomeIndex1 );
		int nodeNumber = 3;
		historicalRecord1.getNode( generationNumber1 , nodeNumber , chromosomeIndex1 );

		Node expectedResult = parentNode.getSonNode( 0 );
		Node result = historicalRecord1.getNode( generationNumber1 , nodeNumber , chromosomeIndex1 );

		assertEquals( expectedResult , result );
	}

	@Test( expected = java.lang.IndexOutOfBoundsException.class )
	public void getLastNodeTestWithChromosomeIndexMinorThanZero() {
		historicalRecord1.getLastNode( -1 );
	}

	@Test( expected = java.lang.IndexOutOfBoundsException.class )
	public void getLastNodeTestWithChromosomeIndexGreaterThanAmmountOfChromosomes() {
		historicalRecord1.getLastNode( 11 );
	}

	@Test( expected = java.lang.IndexOutOfBoundsException.class )
	public void getLastNodeOfGenerationTestWithChromosomeIndexMinorThanZero() {
		historicalRecord1.getLastNodeOfGeneration( generationNumber1 , -1 );
	}

	@Test( expected = java.lang.IndexOutOfBoundsException.class )
	public void getLastNodeOfGenerationTestWithGenerationNumberMinorThanZero() {
		historicalRecord1.getLastNodeOfGeneration( -1 , 10 );
	}

	@Test
	public void getLastNodeOfGenerationTestWithDifferentGenerations() {
		historicalRecord1.addNewGeneration();
		Node expectedResult = historicalRecord1.getNode( 0 , nodeNumber1 , chromosomeIndex1 );
		Node result = historicalRecord1.getLastNodeOfGeneration( 0 , chromosomeIndex1 );

		assertEquals( expectedResult , result );
	}

	/**
	 * Test of addNode method, of class HistoricalRecord.
	 */
	@Test
	public void addNodeTestSameHistorialSameChromosome() {
		float parentFitness = -10;

		Node parentNode = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex1 );
		historicalRecord1.addNode( parentNode , parentFitness , "Testing" , generationNumber1 , chromosomeIndex1 );

		Node expresult = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex1 ).getSonNode( 0 );
		nodeNumber2 = 1;
		Node result = historicalRecord1.getNode( generationNumber1 , nodeNumber2 , chromosomeIndex1 );

		assertEquals( expresult , result );
	}

	@Test
	public void addNodeTestSameHistorialSeveralTimesSameChromosome() {

		float parentFitness = -10;

		Node parentNode = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex1 );
		historicalRecord1.addNode( parentNode , parentFitness , "Testing1" , generationNumber1 , chromosomeIndex1 );

		Node parentNode2 = parentNode.getSonNode( 0 );
		historicalRecord1.addNode( parentNode2 , parentFitness - 2 , "Testing2" , generationNumber1 , chromosomeIndex1 );

		Node expresult = historicalRecord1.getLastNodeOfGeneration( generationNumber1 , chromosomeIndex1 );

		nodeNumber2 = 2;
		Node result = historicalRecord1.getNode( generationNumber1 , nodeNumber2 , chromosomeIndex1 );

		assertEquals( expresult , result );
	}

	@Test
	public void addNodeTestSameHistorialDifferentChromosome() {

		int generationNumber = 0;
		int chromosomeindex = 5;
		int nodeNumber = 0;
		float parentFitness = -10;
		int indicecromosomahijo = 1;

		Node parentNode = historicalRecord1.getNode( generationNumber , nodeNumber , chromosomeindex );
		historicalRecord1.addNode( parentNode , parentFitness , "Testing1" , generationNumber , indicecromosomahijo );

		Node expresult = historicalRecord1.getLastNode( indicecromosomahijo );
		Node result = parentNode.getSonNode( 0 );

		assertEquals( expresult , result );
	}

	/**
	 * Test of agregarnodo method, of class HistoricalRecord.
	 */
	@Test
	public void addNodeTestDifferentHistorial() {

		int numerogeneracion1 = 0;
		int numeronodo1 = 0;
		Node parentNode = historicalRecord1.getNode( numerogeneracion1 , numeronodo1 , chromosomeIndex1 );

		int numerogeneracion2 = 0;
		int indicecromosoma2 = 5;
		float parentFitness2 = -5;
		int nodeNumber2 = 0;
		historicalRecord2.addNode( parentNode , parentFitness1 , "Testing2" , numerogeneracion2 , indicecromosoma2 );

		Node expresult1 = historicalRecord1.getLastNodeOfGeneration( numerogeneracion1 , chromosomeIndex1 );
		Node result1 = historicalRecord2.getLastNodeOfGeneration( numerogeneracion2 , indicecromosoma2 );

		assertNotSame( expresult1 , result1 );

		expresult1 = historicalRecord1.getLastNode( chromosomeIndex1 ).getSonNode( 0 );
		result1 = historicalRecord2.getLastNode( chromosomeIndex1 );

		assertEquals( expresult1 , result1 );
	}

	/**
	 * Test of agregarnodo method, of class HistoricalRecord.
	 */
	@Test
	public void addNodeTestDifferentHistorialSeveralTimes() {
		float parentFitness = -10;

		Node parentNode = historicalRecord1.getNode( generationNumber2 , nodeNumber2 , chromosomeIndex2 );
		historicalRecord2.addNode( parentNode , parentFitness , "Testing1" , generationNumber2 , chromosomeIndex2 );

		parentNode = parentNode.getSonNode( 0 );
		historicalRecord2.addNode( parentNode , parentFitness , "Testing2" , generationNumber2 , chromosomeIndex2 );

		Node result1 = historicalRecord2.getLastNode( chromosomeIndex2 );
		Node expresult1 = parentNode.getSonNode( 0 );

		assertEquals( expresult1 , result1 );

		result1 = historicalRecord1.getLastNode( chromosomeIndex2 );
		expresult1 = historicalRecord2.getLastNode( chromosomeIndex2 );
		assertNotSame( expresult1 , result1 );

		result1 = historicalRecord1.getLastNodeOfGeneration( generationNumber2 , chromosomeIndex2 );
		expresult1 = historicalRecord1.getNode( generationNumber2 , 2 , chromosomeIndex2 );

		assertEquals( expresult1 , result1 );
	}

	@Test
	public void drawPatrilinealBranchTest() {
		Node parentNode = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex1 );
		historicalRecord1.addNode( parentNode , parentFitness1 , "Testing1" , generationNumber1 , chromosomeIndex1 );

		Node parentNode2 = parentNode.getSonNode( 0 );
		historicalRecord1.addNode( parentNode2 , parentFitness1 , "Testing1" , generationNumber1 , chromosomeIndex1 );

		historicalRecord1.showPatrilinealBranch( 5 );
	}

	@Test
	public void addNewGenerationTest() {
		historicalRecord1.addNewGeneration();
		int result = historicalRecord1.getLastNode( 0 ).getGenerationNumber();
		int expectedResult = 1;

		assertEquals( expectedResult , result );
	}
	
	@Test
	public void addNewGenerationTestNodeNumberShouldBeZero() {
		historicalRecord1.addNewGeneration();
		int result = historicalRecord1.getLastNode( 0 ).getNodeNumber();
		int expectedResult = 0;

		assertEquals( expectedResult , result );
	}
	
	@Test
	public void addNewGenerationTestTwoGenerations(){
		historicalRecord1.addNewGeneration();
		historicalRecord1.addNewGeneration();
		int result = historicalRecord1.getLastNode( 0 ).getGenerationNumber();
		int expectedResult = 2;
		
		assertEquals(expectedResult , result);
	}
	
	@Test
	public void addNewGenerationTestCompareGenerationNumbers(){
		historicalRecord1.addNewGeneration();
		historicalRecord1.addNewGeneration();
		boolean shouldStayTrue = true;
		for(int i=0;i<historicalRecord1.getSizeByPopulation();i++){
			if ( historicalRecord1.getLastNode(i).getGenerationNumber() <= 
					historicalRecord1.getLastNode(i).getParentNode(0).getGenerationNumber() ){
				shouldStayTrue = false;
			}
		}
		assertTrue(shouldStayTrue);
	}
	
	@Test
	public void addNewGenerationTestEachNewGenerationNodeShouldHaveOneParentAndViceversa(){
		historicalRecord1.addNewGeneration();
		historicalRecord1.addNewGeneration();
		boolean shouldStayTrue = true;
		Node lastNode;
		for(int i=0;i<historicalRecord1.getSizeByPopulation();i++){
			lastNode = historicalRecord1.getLastNode(i);
			if (lastNode.getParentNode(0) == null ){
				shouldStayTrue=false;
				break;
			}
			if(lastNode.getParentNode(1) != null){
				shouldStayTrue=false;
				break;
			}
			if(lastNode.getParentNode(0).getHowManySons()!=1){
				shouldStayTrue=false;
				break;
			}
		}
		assertTrue(shouldStayTrue);
	}

//	@Test
//	public void showStatsByConsoleTest() {
//		System.setOut( new PrintStream( outContent ) );
//
//		Node parentNode = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex1 );
//		historicalRecord1.addNode( parentNode , parentFitness1 , "Testing1" , generationNumber1 , chromosomeIndex1 );
//
//		Node parentNode2 = parentNode.getSonNode( 0 );
//		historicalRecord1.addNode( parentNode2 , parentFitness1 , "Testing1" , generationNumber1 , chromosomeIndex1 );
//
//		historicalRecord1.showStatsByConsole( "Testing1" , chromosomeIndex1 );
//		String expectedResult = "Testing1: 2\n100.00\n";
//		String result = outContent.toString();
//		assertEquals( expectedResult , result );
//	}
//
//	@Test
//	public void showTotalByConsoleTest() {
//		System.setOut( new PrintStream( outContent ) );
//
//		Node parentNode = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex1 );
//		historicalRecord1.addNode( parentNode , parentFitness1 , "Testing1" , generationNumber1 , chromosomeIndex1 );
//
//		Node parentNode2 = parentNode.getSonNode( 0 );
//		historicalRecord1.addNode( parentNode2 , parentFitness1 , "Testing1" , generationNumber1 , chromosomeIndex1 );
//
//		historicalRecord1.showTotalByConsole( chromosomeIndex1 );
//		String expectedResult = "Total of operations: 2\n";
//		String result = outContent.toString();
//		assertEquals( expectedResult , result );
//	}
//
//	@Test
//	public void walkTheGenealogicalTreeTestSingleParent() {
//		Node parentNode = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex1 );
//		historicalRecord1.addNode( parentNode , parentFitness1 , "Testing1" , generationNumber1 , chromosomeIndex1 );
//
//		parentNode = parentNode.getSonNode( 0 );
//		historicalRecord1.addNode( parentNode , parentFitness1 , "Testing1" , generationNumber1 , chromosomeIndex1 );
//		parentNode = parentNode.getSonNode( 0 );
//
//		historicalRecord1.walkTheGenealogicalTree( parentNode , "Testing1" );
//	}
//
//	@Test
//	public void walkTheGenealogicalTreeTestTwoParent() {
//		Node parentNode1 = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex1 );
//		int chromosomeIndex3 = 6;
//		Node parentNode2 = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex3 );
//		historicalRecord1.addNode( parentNode1 , parentFitness1 , "Testing1" , generationNumber1 , chromosomeIndex3 );
//		Node sonNode = parentNode1.getSonNode( 0 );
//		sonNode.addParentNode( parentNode2 );
//
//		historicalRecord1.walkTheGenealogicalTree( sonNode , "Testing1" );
//	}
//
//	@Test(expected = java.lang.UnsupportedOperationException.class)
//	public void walkTheGenealogicalTreeTestThreParent() {
//		Node parentNode1 = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex1 );
//		int chromosomeIndex3 = 6;
//		int chromosomeIndex4 = 7;
//		Node parentNode2 = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex3 );
//		Node parentNode3 = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex4 );
//		historicalRecord1.addNode( parentNode1 , parentFitness1 , "Testing1" , generationNumber1 , chromosomeIndex3 );
//
//		Node sonNode = parentNode1.getSonNode( 0 );
//		sonNode.addParentNode( parentNode2 );
//		sonNode.addParentNode( parentNode3 );
//
//		historicalRecord1.walkTheGenealogicalTree( sonNode , "Testing1" );
//	}
//	
//	@Test
//	public void walkPatrilinealBranchTestSingleParent() {
//		Node parentNode = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex1 );
//		historicalRecord1.addNode( parentNode , parentFitness1 , "Testing1" , generationNumber1 , chromosomeIndex1 );
//
//		parentNode = parentNode.getSonNode( 0 );
//		historicalRecord1.addNode( parentNode , parentFitness1 , "Testing1" , generationNumber1 , chromosomeIndex1 );
//		parentNode = parentNode.getSonNode( 0 );
//
//		historicalRecord1.walkPatrilinealBranch( parentNode , "Testing1" );
//	}
//	
//	@Test
//	public void walkPatrilinealBranchTestTwoParent() {
//		Node parentNode1 = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex1 );
//		int chromosomeIndex3 = 6;
//		Node parentNode2 = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex3 );
//		historicalRecord1.addNode( parentNode1 , parentFitness1 , "Testing1" , generationNumber1 , chromosomeIndex3 );
//		Node sonNode = parentNode1.getSonNode( 0 );
//		sonNode.addParentNode( parentNode2 );
//
//		historicalRecord1.walkPatrilinealBranch( sonNode , "Testing1" );
//	}
//
//	@Test
//	public void walkPatrilinealBranchTestThreParent() {
//		Node parentNode1 = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex1 );
//		int chromosomeIndex3 = 6;
//		int chromosomeIndex4 = 7;
//		Node parentNode2 = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex3 );
//		Node parentNode3 = historicalRecord1.getNode( generationNumber1 , nodeNumber1 , chromosomeIndex4 );
//		historicalRecord1.addNode( parentNode1 , parentFitness1 , "Testing1" , generationNumber1 , chromosomeIndex3 );
//
//		Node sonNode = parentNode1.getSonNode( 0 );
//		sonNode.addParentNode( parentNode2 );
//		sonNode.addParentNode( parentNode3 );
//
//		historicalRecord1.walkPatrilinealBranch( sonNode , "Testing1" );
//	}
}
