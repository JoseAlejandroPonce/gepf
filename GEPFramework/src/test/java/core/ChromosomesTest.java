/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package core;

import modeling.ChromosomeCoding;
import modeling.GeneCoding;
import examples.modeling.IntegerElementHandler;
import java.util.ArrayList;
import java.util.List;
import modeling.ElementHandler;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author alejandro
 */
public class ChromosomesTest {

    public static GeneCoding geneCoding1;
    public static GeneCoding geneCoding2;
    public static ChromosomeCoding chromosomeCoding;
    public static Chromosome chromosome;

    @Before
    public void setUpchromosomesTest() {

        String geneCodingName = "GEP-GEN";

        String[] domainNames = new String[3];
        domainNames[0] = "Head";
        domainNames[1] = "Tail";
        domainNames[2] = "Constantes";

        Integer[] highBound = new Integer[3];
        highBound[0] = 10;
        highBound[1] = 10;
        highBound[2] = 15;

        Integer[] lowBound = new Integer[3];
        lowBound[0] = 0;
        lowBound[1] = 6;
        lowBound[2] = 11;

        int[] domainSizes = new int[3];
        domainSizes[0] = 7;

        domainSizes[1] = domainSizes[0] * (2 - 1) + 2;
        domainSizes[2] = 10;
        int cantidadpartes = 3;

        ElementHandler[] elementHandlers = new ElementHandler[]{
            new IntegerElementHandler(),
            new IntegerElementHandler(),
            new IntegerElementHandler()
        };

        geneCoding1 = new GeneCoding( domainNames , geneCodingName , highBound , lowBound , domainSizes, elementHandlers );

        String geneCodingName2 = "GEP-GEN-Homeotic";

        String[] domainNames2 = new String[2];
        domainNames2[0] = "Head";
        domainNames2[1] = "Tail";

        Integer[] highBounds2 = new Integer[2];
        highBounds2[0] = 7;
        highBounds2[1] = 7;

        Integer[] lowBounds2 = new Integer[2];
        lowBounds2[0] = 0;
        lowBounds2[1] = 4;

        int[] domainSizes2 = new int[2];
        domainSizes2[0] = 7;

        domainSizes2[1] = domainSizes2[0] * (2 - 1) + 2;

        ElementHandler[] elementHandlers2 = new ElementHandler[3];
        elementHandlers2[0] = new IntegerElementHandler();
        elementHandlers2[1] = new IntegerElementHandler();
        elementHandlers2[2] = new IntegerElementHandler();

        geneCoding2 = new GeneCoding( domainNames2 , geneCodingName2 , highBounds2 , lowBounds2 , domainSizes2, elementHandlers2 );

        GeneCoding[] listacodgen = new GeneCoding[5];
        listacodgen[0] = geneCoding1;
        listacodgen[1] = geneCoding1;
        listacodgen[2] = geneCoding1;
        listacodgen[3] = geneCoding1;
        listacodgen[4] = geneCoding2;

        chromosomeCoding = new ChromosomeCoding( listacodgen , "ChromosomeCodingTest" );

        chromosome = new Chromosome( chromosomeCoding );
    }

    @Test
    public void sizeTest() {
        Integer expResult = 5;
        Integer result = chromosome.size();
        assertEquals( expResult , result );
    }

    @Test
    public void setFitnessAndGetFitnessTest() {
        float expResult = 5;
        chromosome.setFitness( expResult );
        float result = chromosome.getFitness();
	
        assertEquals( expResult , result , 0.1 );
    }

    @Test
    public void setIsFitnessValidAndHasValidFitnessTest() {
        boolean expResult = true;
        chromosome.setValidFitness( expResult );
        boolean result = chromosome.hasValidFitness();
	
        assertEquals( expResult , result );
    }

    @Test
    public void getGenesTest() {
        String expResult = "GEP-GEN";
        String expResult2 = "GEP-GEN-Homeotic";

        List<Gene> result = chromosome.getGenes();
        for ( int i = 0 ; i < result.size() - 1 ; i++ ) {
            assertEquals( expResult , result.get( i ).getName() );
        }
        assertEquals( expResult2 , result.get( result.size() - 1 ).getName() );
    }

    @Test
    public void chromosomesTestsetGenes() {
        Chromosome cromosomafuente = new Chromosome( chromosomeCoding );
        List<Gene> expResult = cromosomafuente.getGenes();
        chromosome.setGenes( expResult );
        List<Gene> result = chromosome.getGenes();

        assertEquals( expResult , result );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void chromosomesTestsetGenesWithANullElement() {
        Chromosome cromosomafuente = new Chromosome( chromosomeCoding );
        List<Gene> expResult = cromosomafuente.getGenes();
        expResult.set( 2 , null );
        chromosome.setGenes( expResult );
        List<Gene> result = chromosome.getGenes();

        assertEquals( expResult , result );
    }
    
    @Test( expected = java.lang.IllegalArgumentException.class )
    public void chromosomesTestsetGenesWithANullGenesList() {
        chromosome.setGenes( null );
    }

    @Test (expected=java.lang.IllegalArgumentException.class)
    public void setGenesTestWithAGeneWithDifferentName() {
        Chromosome aChromosome = new Chromosome( chromosomeCoding );
        List<Gene> expResult = aChromosome.getGenes();
        List<Gene> aux = new ArrayList<Gene>( aChromosome.getGenes() );
        expResult.set( expResult.size() - 1 , aux.get( 0 ) );
        chromosome.setGenes( expResult );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void chromosomesTestsetGenesConUnGenConGenConTamañoDistinto() {

        Chromosome originalChromosome = new Chromosome( chromosomeCoding );
        List<Gene> expResult = originalChromosome.getGenes();

        String geneCodingName = "GEP-GEN";

        String[] domainNames = new String[2];
        domainNames[0] = "Head";
        domainNames[1] = "Tail";

        Integer[] hiBounds = new Integer[2];
        hiBounds[0] = 10;
        hiBounds[1] = 10;

        Integer[] lowBounds = new Integer[2];
        lowBounds[0] = 0;
        lowBounds[1] = 6;

        int[] domainSizes = new int[2];
        domainSizes[0] = 7;

        domainSizes[1] = domainSizes[0] * (2 - 1) + 2;

        ElementHandler[] elementHandlers = new ElementHandler[3];
        elementHandlers[0] = new IntegerElementHandler();
        elementHandlers[1] = new IntegerElementHandler();
        elementHandlers[2] = new IntegerElementHandler();

        GeneCoding auxGeneCoding = new GeneCoding( domainNames , geneCodingName , hiBounds , lowBounds , domainSizes  , elementHandlers );
        expResult.set( 0 , new Gene( auxGeneCoding ) );

        chromosome.setGenes( expResult );
        List<Gene> result = chromosome.getGenes();

        assertEquals( expResult , result );
    }

    @Test
    public void chromosomesTestgetGene() {
        int indice = 0;
        List<Gene> genesauxiliar = new ArrayList<Gene>( chromosome.getGenes() );
        Gene expResult = genesauxiliar.get( 0 );
        Gene result = chromosome.getGene( indice );

        assertEquals( expResult , result );
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void chromosomesTestgetGeneWithIndexLowerThanZero() {
        chromosome.getGene( -1 );
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void chromosomesTestgetGenWithIndexGreaterThanAmountOfGenes() {
        chromosome.getGene(chromosome.size() );
    }

    @Test
    public void chromosomesTestsetGene() {
        int indice = 0;
        Gene gene = new Gene( geneCoding1 );
        String expResult = gene.getName();
        chromosome.setGen( gene , indice );

        String result = chromosome.getGene( 0 ).getName();

        assertEquals( expResult , result );
    }

    @Test
    public void getDomainI() {
        int domainIndex = 0;
        int geneIndex = 0;
        Domain expResult = chromosome.getGenes().get( geneIndex ).getDomainI( domainIndex );

        Domain result = chromosome.getDomainI( geneIndex , domainIndex );
	assertEquals( expResult , result );
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void chromosomesTestgetDomainIWithGeneIndexLowerThanZero() {
        int domainIndex = 0;
        int geneIndex = -1;
        Domain expResult = chromosome.getGenes().get( geneIndex ).getDomainI( domainIndex );

        Domain result = chromosome.getDomainI( geneIndex , domainIndex );
	assertEquals( expResult , result );
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void chromosomesTestgetDomainIWithDomainIndexLowerThanZero() {
        int domainIndex = -1;
        int geneIndex = 0;
        Domain expResult = chromosome.getGenes().get( geneIndex ).getDomainI( domainIndex );

        Domain result = chromosome.getDomainI( geneIndex , domainIndex );
	assertEquals( expResult , result );
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void chromosomesTestgetDomainIWithDomainIndexGreaterThanZero() {
        int geneIndex = 0;
        int domainIndex = chromosome.getGenes().get( geneIndex ).size();
        Domain expResult = chromosome.getGenes().get( geneIndex ).getDomainI( domainIndex );

        Domain result = chromosome.getDomainI( geneIndex , domainIndex );
	assertEquals( expResult , result );
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void chromosomesTestgetDomainIWithDomainIndexGreaterThanTheAmountOfDomainsOfThatGene() {

        int domainIndex = chromosome.getGenes().size();
        int geneIndex = 0;
        Domain expResult = chromosome.getGenes().get( geneIndex ).getDomainI( domainIndex );

        Domain result = chromosome.getDomainI( geneIndex , domainIndex );
	assertEquals( expResult , result );
    }

    @Test
    public void setDomainI() {

        int domainIndex = 0;
        int geneIndex = 0;
        String nombre = chromosome.getDomainI( geneIndex , domainIndex ).getName();
        Integer lowBound = (Integer) chromosome.getDomainI( geneIndex , domainIndex ).getLowBound();
        Integer highBound = (Integer) chromosome.getDomainI( geneIndex , domainIndex ).getHighBound();
        int tamañoparte = chromosome.getDomainI( geneIndex , domainIndex ).size();
        Domain domain = new Domain( nombre , tamañoparte , lowBound , highBound , new IntegerElementHandler() );

        chromosome.setDomainI( domain , geneIndex , domainIndex );

        Domain expresult = domain;

        Domain result = chromosome.getDomainI( geneIndex , domainIndex );

        assertEquals( expresult , result );
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void setDomainiConIndiceGenMenorQueCero() {
        int domainIndex = 0;
        int geneIndex = -1;
        String nombre = chromosome.getDomainI( geneIndex , domainIndex ).getName();
        Integer lowBound = (Integer) chromosome.getDomainI( geneIndex , domainIndex ).getLowBound();
        Integer highBound = (Integer) chromosome.getDomainI( geneIndex , domainIndex ).getHighBound();
        int tamañoparte = chromosome.getDomainI( geneIndex , domainIndex ).size();
        Domain domain = new Domain( nombre , tamañoparte , lowBound , highBound , new IntegerElementHandler() );

        chromosome.setDomainI( domain , geneIndex , domainIndex );
        Domain expresult = domain;
        Domain result = chromosome.getDomainI( geneIndex , domainIndex );
        assertEquals( expresult , result );
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void setDomainIWithGeneIndexGreaterThanTheAmountOfGenes() {
        int domainIndex = 0;
        int geneIndex = 0;
        String nombre = chromosome.getDomainI( geneIndex , domainIndex ).getName();
        Integer lowBound = (Integer) chromosome.getDomainI( geneIndex , domainIndex ).getLowBound();
        Integer highBound = (Integer) chromosome.getDomainI( geneIndex , domainIndex ).getHighBound();
        int tamañoparte = chromosome.getDomainI( geneIndex , domainIndex ).size();
        Domain domain = new Domain( nombre , tamañoparte , lowBound , highBound , new IntegerElementHandler() );
        geneIndex = chromosome.size();
        chromosome.setDomainI( domain , geneIndex , domainIndex );

        Domain expresult = domain;
        Domain result = chromosome.getDomainI( geneIndex , domainIndex );
        assertEquals( expresult , result );
    }

    @Test
    public void getParte() {
        int geneIndex = 0;
        int domainIndex = 0;
        Domain expResult = chromosome.getDomainI( geneIndex , domainIndex );

        String nombreparte = "Head";
        Domain result = chromosome.getDomain( geneIndex , nombreparte );
        assertEquals( expResult , result );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void getParteConNombreNull() {
        int geneIndex = 0;
        int domainIndex = 0;
        Domain expResult = chromosome.getDomainI( geneIndex , domainIndex );
        String nombreparte = null;
        Domain result = chromosome.getDomain( geneIndex , nombreparte );
	
        assertEquals( expResult , result );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void getDomainWithEmptyName() {
        int geneIndex = 0;
        int domainIndex = 0;
        Domain expResult = chromosome.getDomainI( geneIndex , domainIndex );
        String nombreparte = null;
        Domain result = chromosome.getDomain( geneIndex , nombreparte );

        assertEquals( expResult , result );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void getParteConNombreinexistenteEnGen() {

        int geneIndex = 0;
        int domainIndex = 0;

        Domain expResult = chromosome.getDomainI( geneIndex , domainIndex );

        String nombreparte = "Parte";
        Domain result = chromosome.getDomain( geneIndex , nombreparte );

        assertEquals( expResult , result );
    }

    @Test
    public void setDomain() {
        int geneIndex = 0;
        int domainIndex = 0;
        String nombre = chromosome.getDomainI( geneIndex , domainIndex ).getName();
        Integer lowBound = (Integer) chromosome.getDomainI( geneIndex , domainIndex ).getLowBound();
        Integer highBound = (Integer) chromosome.getDomainI( geneIndex , domainIndex ).getHighBound();
        int tamañoparte = chromosome.getDomainI( geneIndex , domainIndex ).size();
        Domain domain = new Domain( nombre , tamañoparte , lowBound , highBound , new IntegerElementHandler() );

        chromosome.setDomain( domain , geneIndex , nombre );
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void setDomainWithGeneIndexLessThanZero() {
        int geneIndex = 0;
        int domainIndex = 0;
        String nombre = chromosome.getDomainI( geneIndex , domainIndex ).getName();
        Integer lowBound = (Integer) chromosome.getDomainI( geneIndex , domainIndex ).getLowBound();
        Integer highBound = (Integer) chromosome.getDomainI( geneIndex , domainIndex ).getHighBound();
        int tamañoparte = chromosome.getDomainI( geneIndex , domainIndex ).size();
        Domain domain = new Domain( nombre , tamañoparte , lowBound , highBound , new IntegerElementHandler() );
        geneIndex = -1;
	
        chromosome.setDomain( domain , geneIndex , nombre );
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void setDomainWithGeneIndexGreaterThanTheAmmountOfGenes() {
        int geneIndex = 0;
        int domainIndex = 0;
        String nombre = chromosome.getDomainI( geneIndex , domainIndex ).getName();
        Integer lowBound = (Integer) chromosome.getDomainI( geneIndex , domainIndex ).getLowBound();
        Integer highBound = (Integer) chromosome.getDomainI( geneIndex , domainIndex ).getHighBound();
        int tamañoparte = chromosome.getDomainI( geneIndex , domainIndex ).size();
        Domain domain = new Domain( nombre , tamañoparte , lowBound , highBound , new IntegerElementHandler() );

        geneIndex = chromosome.size();
        chromosome.setDomain( domain , geneIndex , nombre );
    }

    @Test
    public void getElement() {
        int geneIndex = 0;
        int domainIndex = 0;
        int elementIndex = 0;
        int elemento = 2;
        Integer expResult = elemento;
        chromosome.getGene( geneIndex ).getDomainI( domainIndex ).setElement(  elemento ,elementIndex );
        Integer result = (Integer) chromosome.getElement( geneIndex , domainIndex , elementIndex );

        assertEquals( expResult , result );
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void getElementWithGeneIndexLesserThanZero() {

        int geneIndex = 0;
        int domainIndex = 0;
        int elementIndex = 0;
        int elemento = 2;
        Integer expResult = elemento;
        chromosome.getGene( geneIndex ).getDomainI( domainIndex ).setElement( elementIndex , elemento );
        geneIndex = -1;
        Integer result = (Integer) chromosome.getElement( geneIndex , domainIndex , elementIndex );

        assertEquals( expResult , result );
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void getElementWithGeneIndexGreaterThanTheAmountOfGenes() {
        int geneIndex = 0;
        int domainIndex = 0;
        int elementIndex = 0;
        int elemento = 2;
        Integer expResult = elemento;
        chromosome.getGene( geneIndex ).getDomainI( domainIndex ).setElement( elementIndex , elemento );
        geneIndex = chromosome.size();
        Integer result = (Integer) chromosome.getElement( geneIndex , domainIndex , elementIndex );

        assertEquals( expResult , result );
    }

    @Test
    public void setElement() {
        int geneIndex = 0;
        int domainIndex = 0;
        int elementIndex = 0;
        Integer elemento = 2;
        Integer expResult = elemento;
        chromosome.setElement( elemento , geneIndex , domainIndex , elementIndex );
        Integer result = (Integer) chromosome.getElement( geneIndex , domainIndex , elementIndex );

        assertEquals( expResult , result );
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void setElementWithGeneIndexLessThanZero() {
        int geneIndex = 0;
        int domainIndex = 0;
        int elementIndex = 0;
        int element = 2;
        Integer expResult = element;
        chromosome.setElement( element , geneIndex , domainIndex , elementIndex );
        geneIndex = -1;
        Integer result = (Integer) chromosome.getElement( geneIndex , domainIndex , elementIndex );

        assertEquals( expResult , result );

    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void setElementConWithGeneIndexGreaterThanTheAmountOfGenes() {
        int geneIndex = 0;
        int domainIndex = 0;
        int elementIndex = 0;
        int elemento = 2;
        Integer expResult = elemento;
        chromosome.setElement( elemento , geneIndex , domainIndex , elementIndex );
        geneIndex = chromosome.size();
        Integer result = (Integer) chromosome.getElement( geneIndex , domainIndex , elementIndex );

        assertEquals( expResult , result );
    }

    @Test
    public void chromosomesTestConstructorChromosomeCoding() {
        chromosome = new Chromosome( chromosomeCoding );
    }
    
    @Test
    public void toStringTest(){
	    String aString = chromosome.toString();
	    System.out.println(aString);
    }

}
