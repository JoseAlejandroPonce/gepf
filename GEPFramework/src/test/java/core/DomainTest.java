/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package core;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static junit.framework.Assert.assertEquals;
import modeling.ElementHandler;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author alejandro
 */

public class DomainTest {

    private Domain testDomain;
    private static ElementHandler<Integer> mock;
    

    @Before
    public void setUp() {
        String name = "DomainForTests";
        int domainSize = 10;
        Integer lowBound = 0;
        Integer highBound = 10;
	
	mock = (ElementHandler<Integer>) mock( ElementHandler.class);
	when(mock.generate(lowBound,highBound))
		.thenReturn((int) Math.floor( (Math.random() * (highBound - lowBound + 1)) + lowBound ));
	    Boolean aBoolean= true;
	    
	when(mock.check( 0 , lowBound , highBound )).thenReturn( aBoolean );
	when(mock.check( 1 , lowBound , highBound )).thenReturn( aBoolean );
	when(mock.check( 2 , lowBound , highBound )).thenReturn( aBoolean );
	when(mock.check( 3 , lowBound , highBound )).thenReturn( aBoolean );
	when(mock.check( 4 , lowBound , highBound )).thenReturn( aBoolean );
	when(mock.check( 5 , lowBound , highBound )).thenReturn( aBoolean );
	when(mock.check( 6 , lowBound , highBound )).thenReturn( aBoolean );
	when(mock.check( 7 , lowBound , highBound )).thenReturn( aBoolean );
	when(mock.check( 8 , lowBound , highBound )).thenReturn( aBoolean );
	when(mock.check( 9 , lowBound , highBound )).thenReturn( aBoolean );
	when(mock.check( 10 , lowBound , highBound )).thenReturn( aBoolean );
	
	when(mock.copy(any(Integer.class))).thenReturn(new Integer(1));
	
        testDomain = new Domain( name , domainSize , lowBound , highBound , mock );
	
	
    }

    @Test
    public void domainSizeTest() {
        assertEquals( 10 , testDomain.size() );
    }

    @Test
    public void getNameTest() {
        assertEquals( "DomainForTests" , testDomain.getName() );
    }

    @Test
    public void getBoundsTest() {
        assertEquals( 0 , testDomain.getLowBound() );
        assertEquals( 10 , testDomain.getHighBound() );
    }

    @Test
    public void getElementTest() {
        List<Integer> auxElements = new ArrayList<Integer>();
        int i = 0;
        for ( i = 0 ; i < testDomain.size() ; i++ ) {
            assertTrue( (int) testDomain.getElement( i ) <= (int) testDomain.getHighBound() );
            assertTrue( (int) testDomain.getElement( i ) >= (int) testDomain.getLowBound() );
        }
    }

    @Test
    public void setElementTest() {
        for ( int i = 0 ; i < testDomain.size() ; i++ ) {
            testDomain.setElement( 0 , i  );
        }
        for ( int i = 0 ; i < testDomain.size() ; i++ ) {
            assertEquals( testDomain.getElement( i ) , (Integer) 0 );
        }
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void setElementTestWithElementLowerThanLowBound() {
        for ( int i = 0 ; i < testDomain.size() ; i++ ) {
            testDomain.setElement(  (int) testDomain.getLowBound() - i ,i );
        }
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void setElementTestWithAnElementGreaterThanTheHighBound() {
        for ( int i = 0 ; i < testDomain.size() ; i++ ) {
            testDomain.setElement(  (int) testDomain.getHighBound() + i ,i );
        }
    }

    @Test
    public void constructorTestUsoCorrecto() {
        String nombre = "DomainForTests";
        Integer lowBound = 0;
        Integer highBound = 10;
        int domainSize = 10;
        Domain domain = new Domain( nombre , domainSize , lowBound , highBound , mock);

        assertEquals( "DomainForTests" , domain.getName() );
        assertEquals( lowBound , domain.getLowBound() );
        assertEquals( highBound , domain.getHighBound() );
        assertEquals( 10 , domain.size() );
        assertEquals( domainSize , domain.size() );

        for ( int i = 0 ; i < domainSize ; i++ ) {
            assertTrue( (int) domain.getElement( i ) >= (int) domain.getLowBound() );
            assertTrue( (int) domain.getElement( i ) <= (int) domain.getHighBound() );
        }
    }


    @Test( expected = java.lang.IllegalArgumentException.class )
    public void constructorTestWithSize0() {
        String name = "ParteDePrueba";
        Integer lowBound = 0;
        Integer highBound = 10;
        int domainSize = 0;

        Domain domain = new Domain( name , domainSize , lowBound , highBound , mock);
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void constructorTestWithSizeLessThanZero() {
        String name = "ParteDePrueba";
        Integer lowBound = 0;
        Integer highBound = 10;
        int tamañoparte = -1;

	Domain domain = new Domain( name , tamañoparte , lowBound , highBound , mock );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void Constructor1TestWithEmptyName() {
        String name = "";
        Integer lowBound = 0;
        Integer highBound = 10;
        int domainSize = 10;

        Domain domain = new Domain( name , domainSize , lowBound , highBound , mock);
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void constructor1TestWithNullName() {
        String name = null;
        Integer lowBound = 0;
        Integer highBound = 10;
        int domainSize = 10;

        Domain domain = new Domain( name , domainSize , lowBound , highBound , mock);
    }
    
    @Test( expected = java.lang.IllegalArgumentException.class )
    public void constructor2TestWithNullInputDomain(){
	    Domain newDomain = new Domain(null);
    }
    
    @Test
    public void generateElementTest(){
	    Integer lowBound = (Integer) testDomain.getLowBound();
	    Integer highBound = (Integer) testDomain.getHighBound();
	    Integer element = (Integer) testDomain.generateElement();
	    assertTrue(( (int) element > (int) lowBound ) && ((element) < highBound));
    }
    
    @Test
    public void copyTest(){
	    Integer result = 1;
	    Integer expectedResult = (Integer) testDomain.copy( result);
	    assertEquals( expectedResult , result );
    }

}
