/*
 * Copyright (C) 2016 José Alejandro Ponce
 *
 *  This file is part of GEPFramework.
 *
 *  GEPFramework is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GEPFramework is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GEPFramework.  If not, see <http://www.gnu.org/licenses/>.
 */
package core;

import analysis.HistoricalRecord;
import modeling.ChromosomeCoding;
import modeling.GeneCoding;
import examples.modeling.IntegerElementHandler;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import modeling.ElementHandler;

/**
 *
 * @author José Alejandro Ponce
 */
public class PopulationTest {

    public static GeneCoding geneCoding1;
    public static GeneCoding geneCoding2;
    public static ChromosomeCoding chromosomeCoding;
    public static Population population;

    @Before
    public void setUp() {
        String geneCodingName = "GEP-GEN";

        String[] domainNames = new String[3];
        domainNames[0] = "Head";
        domainNames[1] = "Tail";
        domainNames[2] = "Constantes";

        Integer[] highBounds = new Integer[3];
        highBounds[0] = 10;
        highBounds[1] = 10;
        highBounds[2] = 15;

        Integer[] lowBounds = new Integer[3];
        lowBounds[0] = 0;
        lowBounds[1] = 6;
        lowBounds[2] = 11;

        int[] domainSizes = new int[3];
        domainSizes[0] = 7;

        domainSizes[1] = domainSizes[0] * (2 - 1) + 2;
        domainSizes[2] = 10;
        ElementHandler[] elementHandlers = new ElementHandler[3];
        elementHandlers[0] = new IntegerElementHandler();
        elementHandlers[1] = new IntegerElementHandler();
        elementHandlers[2] = new IntegerElementHandler();

        geneCoding1 = new GeneCoding( domainNames , geneCodingName , highBounds , lowBounds , domainSizes  , elementHandlers );

        String geneCodingName2 = "GEP-GEN-Homeotic";

        String[] domainNames2 = new String[2];
        domainNames2[0] = "Head";
        domainNames2[1] = "Tail";

        Integer[] highBounds2 = new Integer[2];
        highBounds2[0] = 7;
        highBounds2[1] = 7;

        Integer[] lowBounds2 = new Integer[2];
        lowBounds2[0] = 0;
        lowBounds2[1] = 4;

        int[] domainSizes2 = new int[2];
        domainSizes2[0] = 7;

        domainSizes2[1] = domainSizes[0] * (2 - 1) + 2;

        ElementHandler[]elementHandlers2 = new ElementHandler[3];
        elementHandlers2[0] = new IntegerElementHandler();
        elementHandlers2[1] = new IntegerElementHandler();
        elementHandlers2[2] = new IntegerElementHandler();

        geneCoding2 = new GeneCoding( domainNames2 , geneCodingName2 , highBounds2 , lowBounds2 , domainSizes2 , elementHandlers2 );

        GeneCoding[] geneCodingArray = new GeneCoding[5];
        geneCodingArray[0] = geneCoding1;
        geneCodingArray[1] = geneCoding1;
        geneCodingArray[2] = geneCoding1;
        geneCodingArray[3] = geneCoding1;
        geneCodingArray[4] = geneCoding2;

        chromosomeCoding = new ChromosomeCoding( geneCodingArray , "CodificacionCromosomaTest" );

        int tamaño = 10;
        population = new Population( tamaño , 0 , chromosomeCoding );

    }

    @Test
    public void constructorTestRightUsage() {
        int tamaño = 10;
        Population nuevapoblacion = new Population( tamaño , 1 , chromosomeCoding );
        int expResult = tamaño;
        int result = nuevapoblacion.size();

        assertEquals( expResult , result );
        assertEquals( expResult , result );

    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void constructorTestWithSizeLesserThanZero() {
        int size = -1;
        Population newPopulation = new Population( size , 2 , chromosomeCoding );
        int expectedResult = size;
        int result = newPopulation.size();

        assertEquals( expectedResult , result );
    }
    
    @Test
    public void constructorTestWithPopulationAsInputArgument(){
	    Population expectedResult = new Population(population);
	    assertEquals(expectedResult.size(),population.size());
    }

    @Test
    public void sizeTest() {
        int expResult = 10;
        int result = population.size();

        assertEquals( expResult , result );
    }

    @Test
    public void getChromosomesTest() {
        List<Chromosome> chromosomes = population.getchromosomes();
        int expectedResult = population.size();
        int result = chromosomes.size();

        assertEquals( expectedResult , result );
    }

    @Test
    public void setChromosomesTest() {
        int size = 10;
        Population auxPopulation = new Population( size , 3 , chromosomeCoding );
        List<Chromosome> expectedResult = auxPopulation.getchromosomes();
        population.setchromosomes( auxPopulation.getchromosomes() );
        List<Chromosome> result = population.getchromosomes();

        assertEquals( expectedResult , result );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void setChromosomesTestWithDifferentSizes() {
        int size = 11;
        Population auxPopulation = new Population( size , 4 , chromosomeCoding );
        List<Chromosome> expectedResult = auxPopulation.getchromosomes();
        population.setchromosomes( auxPopulation.getchromosomes() );
        List<Chromosome> result = population.getchromosomes();

        assertEquals( expectedResult , result );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void SetChromosomesTestWithEmptyList() {
        List<Chromosome> expResult = new ArrayList<Chromosome>();
        population.setchromosomes( expResult );
        List<Chromosome> result = population.getchromosomes();

        assertEquals( expResult , result );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void setChromosomesTestWithNullList() {
        List<Chromosome> expResult = null;
        population.setchromosomes( expResult );
        List<Chromosome> result = population.getchromosomes();

        assertEquals( expResult , result );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void setChromosomesTestWithANullChromosome() {
        int size = 10;
        Population auxPopulation = new Population( size , 7 , chromosomeCoding );
        List<Chromosome> expectedResult = auxPopulation.getchromosomes();
        expectedResult.set( 2 , null );
        population.setchromosomes( expectedResult );
        List<Chromosome> result = population.getchromosomes();

        assertEquals( expectedResult , result );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void setChromosomesTestWithAWrongSizedChromosome() {
        int size = 10;
        Population auxPopulation = new Population( size , 8 , chromosomeCoding );
        GeneCoding[] auxGenes = new GeneCoding[2];
        auxGenes[0] = geneCoding1;
        auxGenes[1] = geneCoding1;
        ChromosomeCoding auxChromosomeCoding = new ChromosomeCoding( auxGenes , "Codificacion con tamaño incorrecto" );

        Chromosome wrongChromosome = new Chromosome( auxChromosomeCoding );

        List<Chromosome> expectedResult = auxPopulation.getchromosomes();
        expectedResult.set( 2 , wrongChromosome );

        population.setchromosomes( expectedResult );
        List<Chromosome> result = population.getchromosomes();
        assertEquals( expectedResult , result );
    }

    @Test
    public void getChromosomeTest() {
        int index = 0;
        Chromosome expectedResult = population.getchromosomes().get( index );
        Chromosome result = population.getChromosome( index );

        assertEquals( expectedResult , result );
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void getChromosomeTestWIthNegativeIndex() {
        int index = -1;

        Chromosome result = population.getChromosome( index );
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void GetChromosomeTestWithIndexGreaterThanTheSizeOfThePopulation() {
        int index = population.size()+1;

        Chromosome result = population.getChromosome( index );
    }

    @Test
    public void SetChromosomeTest() {
        int index = 0;
        Chromosome chromosome = new Chromosome( chromosomeCoding );
        Chromosome expResult = chromosome;
        population.setChromosome( chromosome , index );
        Chromosome result = population.getChromosome( index );
	
        assertEquals( result , expResult );
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void setChromosomeTestWithIndexLessThanZero() {
        int index = -1;
        Chromosome chromosome = new Chromosome( chromosomeCoding );
        Chromosome expectedResult = chromosome;

        population.setChromosome( chromosome , index );
        Chromosome result = population.getChromosome( index );
        assertEquals( result , expectedResult );
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void setChromosomeTestWithIndexGreaterThanSizeOfPopulation() {
        int index = population.size()+1;
        Chromosome chromosome = new Chromosome( chromosomeCoding );
        Chromosome expResult = chromosome;
        population.setChromosome( chromosome , index );
        Chromosome result = population.getChromosome( index );
	
        assertEquals( result , expResult );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void setChromosomeTestWithNullChromosome() {
        int index = 0;
        Chromosome chromosome = null;
        Chromosome expectedResult = null;
        population.setChromosome( chromosome , index );
        Chromosome result = population.getChromosome( index );
	
        assertEquals( result , expectedResult );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void setChromosomeTestWithAChromosomeWithDifferentSize() {
        int index = 0;
        Chromosome chromosome = new Chromosome(population.getChromosome( index ));
	chromosome.getGenes( ).add( new Gene(geneCoding1));
	
        population.setChromosome( chromosome , index );
    }

    @Test
    public void testGetGene() {
        int geneIndex = 0;
        int chromosomeIndex = 0;
        Gene result = new Gene( geneCoding1 );
        population.getChromosome( chromosomeIndex ).setGen( result , geneIndex );
        Gene expResult = population.getGen( chromosomeIndex , geneIndex );

        assertEquals( expResult , result );
    }

    @Test
    public void testSetGene() {
        int geneIndex = 0;
        int chromosomeindex = 0;
        Gene result = new Gene( geneCoding1 );
        population.setGen( chromosomeindex , geneIndex , result );
        Gene expResult = population.getGen( chromosomeindex , geneIndex );

        assertEquals( expResult , result );
    }

    @Test
    public void testSetDomain() {
        int chromosomeindex = 0;
        int geneIndex = 0;
        int domainIndex = 0;
        Domain expResult = new Domain(
                population.getDomainI( chromosomeindex , geneIndex , domainIndex ).getName() ,
                population.getDomainI( chromosomeindex , geneIndex , domainIndex ).size() ,
                population.getDomainI( chromosomeindex , geneIndex , domainIndex ).getLowBound() ,
                population.getDomainI( chromosomeindex , geneIndex , domainIndex ).getHighBound() , (ElementHandler) population.getGen( chromosomeindex , geneIndex ).getDomainI( domainIndex ).getElementHandler()
        );
        String nombreparte = population.getGen( chromosomeindex , geneIndex ).getDomainI( domainIndex ).getName();

        population.setDomain( expResult , chromosomeindex , geneIndex , nombreparte );
        Domain result = population.getDomain( chromosomeindex , geneIndex , nombreparte );

        assertEquals( expResult , result );

    }

    @Test
    public void testGetDomainI() {
        int chromosomeIndex = 0;
        int geneIndex = 0;
        int domainIndex = 0;
        Domain expResult = population.getGen( chromosomeIndex , geneIndex ).getDomainI( geneIndex );
        Domain result = population.getDomainI( chromosomeIndex , geneIndex , domainIndex );

        assertEquals( expResult , result );
    }

    @Test
    public void testSetDomainI() {
        int chromosomeIndex = 0;
        int geneIndex = 0;
        int domainIndex = 0;
        Domain expResult = new Domain(
                population.getDomainI(chromosomeIndex,geneIndex,domainIndex).getName() ,
                population.getDomainI(chromosomeIndex,geneIndex,domainIndex).size() , 
                population.getDomainI(chromosomeIndex,geneIndex,domainIndex).getLowBound() , 
                population.getDomainI(chromosomeIndex,geneIndex,domainIndex).getHighBound() ,
                population.getDomainI(chromosomeIndex,geneIndex,domainIndex).getElementHandler()
        );

        population.setDomainI( expResult , chromosomeIndex , geneIndex , domainIndex );
        Domain result = population.getDomainI( chromosomeIndex , geneIndex , domainIndex );

        assertEquals( expResult , result );
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void testSetDomainWithIndexLessThanZero() {
        int chromosomeIndex = -1;
        int geneIndex = 0;
        int domainIndex = 0;

        Domain expResult = new Domain(
                  population.getGen( chromosomeIndex , geneIndex ).getDomainI( domainIndex ).getName()
		, population.getGen( chromosomeIndex , geneIndex ).getDomainI( domainIndex ).size() 
		, (Object[]) population.getGen( chromosomeIndex , geneIndex ).getDomainI( domainIndex ).getLowBound() 
		, (Object[]) population.getGen( chromosomeIndex , geneIndex ).getDomainI( domainIndex ).getHighBound() 
		, (ElementHandler) population.getGen( chromosomeIndex , geneIndex ).getDomainI( domainIndex ).getElementHandler()
        );

        population.setDomainI( expResult , chromosomeIndex , geneIndex , domainIndex );
        Domain result = population.getDomainI( chromosomeIndex , geneIndex , domainIndex );

        assertEquals( expResult , result );
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void testSetDomainWithChromosomeIndexGreaterThanSizeOfPopulation() {
        int chromosomeindex = population.size();
        int geneIndex = 0;
        int domainIndex = 0;

        Domain expResult = new Domain(
                  population.getGen( chromosomeindex , geneIndex ).getDomainI( domainIndex ).getName() 
		, population.getGen( chromosomeindex , geneIndex ).getDomainI( domainIndex ).size() 
		, (Object[]) population.getGen( chromosomeindex , geneIndex ).getDomainI( domainIndex ).getLowBound() 
		, (Object[]) population.getGen( chromosomeindex , geneIndex ).getDomainI( domainIndex ).getHighBound() 
		, (ElementHandler) population.getGen( chromosomeindex , geneIndex ).getDomainI( domainIndex ).getElementHandler()
        );

        population.setDomainI( expResult , chromosomeindex , geneIndex , domainIndex );
        Domain result = population.getDomainI( chromosomeindex , geneIndex , domainIndex );

        assertEquals( expResult , result );
    }

    @Test
    public void testgetElement() {
        int chromosomeindex = 0;
        int geneIndex = 0;
        int domainIndex = 0;
        int elementIndex = 0;
        Integer elemento = (Integer) population.getElement( chromosomeindex , geneIndex , domainIndex , elementIndex );
        int lowBounds = (Integer) population.getDomainI( chromosomeindex , geneIndex , domainIndex ).getLowBound();
        int highBounds = (Integer) population.getDomainI( chromosomeindex , geneIndex , domainIndex ).getHighBound();

        assertTrue( elemento >= lowBounds );
        assertTrue( elemento <= highBounds );
    }

    @Test
    public void setElementTest() {
        int chromosomeindex = 0;
        int geneIndex = 0;
        int domainIndex = 0;
        int elementIndex = 0;
        Integer elemento1 = 5;
        population.setElement( elemento1 , chromosomeindex , geneIndex , domainIndex , elementIndex );
        Integer elemento = (Integer) population.getElement( chromosomeindex , geneIndex , domainIndex , elementIndex );
        int lowBounds = (Integer) population.getDomainI( chromosomeindex , geneIndex , domainIndex ).getLowBound();
        int highBounds = (Integer) population.getDomainI( chromosomeindex , geneIndex , domainIndex ).getHighBound();

        assertTrue( elemento >= lowBounds );
        assertTrue( elemento <= highBounds );
    }

    @Test
    public void getChromosomeCodingTest() {
        assertEquals(chromosomeCoding , population.getChromosomeCoding() );
    }
    
    @Test
    public void getHistoricalRecordTest(){
	    assertTrue( population.getHistoricalRecord() instanceof HistoricalRecord);
    }

}
