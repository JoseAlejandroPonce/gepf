/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package core;

import examples.modeling.FloatElementHandler;
import modeling.GeneCoding;
import examples.modeling.IntegerElementHandler;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import modeling.ElementHandler;

/**
 *
 * @author alejandro
 */
public class GenesTest {

    public static Gene gene;
    public static GeneCoding geneCoding;

    @Before
    public void setUpGenesTest() {

        String geneCodingName = "GEP-GEN";

        String[] domainNames = new String[3];
        domainNames[0] = "Head";
        domainNames[1] = "Tail";
        domainNames[2] = "Constants";

        Object[] highBound = new Object[3];
        highBound[0] = 10;
        highBound[1] = 10;
        highBound[2] = 15f;

        Object[] lowBounds = new Object[3];
        lowBounds[0] = 0;
        lowBounds[1] = 6;
        lowBounds[2] = 0.1f;

        int[] domainSizes = new int[3];
        domainSizes[0] = 7;

        domainSizes[1] = domainSizes[0] * (2 - 1) + 2;
        domainSizes[2] = 10;

        ElementHandler[] elementHandlers = new ElementHandler[3];
        elementHandlers[0] = new IntegerElementHandler();
        elementHandlers[1] = new IntegerElementHandler();
        elementHandlers[2] = new FloatElementHandler();

        geneCoding = new GeneCoding( domainNames , geneCodingName , highBound , lowBounds , domainSizes  , elementHandlers );

        gene = new Gene( geneCoding );
    }

    @Test
    public void constructorTestRightUsage() {
        Gene geneTest = new Gene( geneCoding );
        assertTrue( null != geneTest );
    }

    @Test
    public void getNameTest() {
        String expResult = "GEP-GEN";
        String result = gene.getName();
        assertEquals( expResult , result );

    }

    @Test
    public void geneSizeTest() {
        int resultEsp = 3;
        int result = gene.size();
        assertEquals( resultEsp , result );
    }

    @Test
    public void GenesTestgetDomainI() {
        List<Domain> domainsList = new ArrayList<Domain>();
        domainsList.add( new Domain( "Head" , 7 , 0 , 10 , new IntegerElementHandler() ) );
        domainsList.add( new Domain( "Tail" , 9 , 6 , 10 , new IntegerElementHandler() ) );
        domainsList.add( new Domain( "Constants" , 10 , 0.1f , 15f , new FloatElementHandler() ) );
	Object lowBound; 

        for ( int domainIndex = 0 ; domainIndex < domainsList.size() ; domainIndex++ ) {
	    lowBound =  gene.getDomainI( domainIndex ).getLowBound();
            assertEquals( gene.getDomainI( domainIndex ).getName() , domainsList.get( domainIndex ).getName() );
            assertEquals( gene.getDomainI( domainIndex ).size() , domainsList.get( domainIndex ).size() );
            assertEquals( lowBound , domainsList.get( domainIndex ).getLowBound() );
            assertEquals( lowBound , domainsList.get( domainIndex ).getLowBound() );
        }
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void GenesTestgetDomainIWithIndexLessThanZero() {
	Domain domain = gene.getDomainI( -1 );
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void GenesTestgetDomainIWithIndexGreaterThanTheSizeOfTheGene() {
        Domain domain = gene.getDomainI( 3 );
    }

    /**
     * Test of setDomainI method, of class Gene.
     */
    @Test
    public void testsetDomainI() {
        Domain domain = new Domain( "Head" , 7 , 0 , 10 , new IntegerElementHandler() );
        gene.setDomainI( domain , 0 );
        assertEquals( domain , gene.getDomainI( 0 ) );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void GenesTestsetDomainIWithANullDomain() {
        Domain domain = null;
        gene.setDomainI( domain , 0 );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void GenesTestsetDomainIWithADomainWithSize0() {
        Domain domain = new Domain( "Head" , 0 , 0 , 10 , new IntegerElementHandler() );

        gene.setDomainI( domain , 0 );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void GenesTestsetDomainIWithADomainWithALesserSize() {
        Domain domain = new Domain( "Head" , 5 , 0 , 10 , new IntegerElementHandler() );

        gene.setDomainI( domain , 0 );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void GenesTestsetDomainIWithADomainWithAGreaterSize() {
        Domain domain = new Domain( "Head" , 8 , 0 , 10 , new IntegerElementHandler() );

        gene.setDomainI( domain , 0 );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void GenesTestsetDomainIWithWrongBounds() {
        Domain domain = new Domain( "Head" , 7 , 1 , 10 , new IntegerElementHandler() );

        gene.setDomainI( domain , 0 );
    }
    
    @Test( expected = java.lang.IllegalArgumentException.class )
    public void GenesTestsetDomainIWithWrongBounds2() {
        Domain domain = new Domain( "Constants" , 10 , 5f , 15f , new FloatElementHandler() );

        gene.setDomainI( domain , 2 );
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void GenesTestsetDomainIWithIndexLessThanZero() {
        Domain domain = new Domain( "Head" , 7 , 1 , 10 , new IntegerElementHandler() );

        gene.setDomainI( domain , -1 );
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void GenesTestsetDomainIWithIndexGreaterThanSizeOfGene() {
        Domain domain = new Domain( "Head" , 7 , 1 , 10 , new IntegerElementHandler() );

        gene.setDomainI( domain , 3 );
    }
    
    @Test( expected = java.lang.IllegalArgumentException.class )
    public void GenesTestsetDomainIWithADifferentName() {
        Domain domain = new Domain( "Header" , 7 , 1 , 10 , new IntegerElementHandler() );

        gene.setDomainI( domain , 0 );
    }

    @Test
    public void GenesTestGetDomain() {
        String domainName = "Tail";
        String expResult = "Tail";

        String result = gene.getDomain( domainName ).getName();
        assertEquals( expResult , result );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void GenesTestGetDomainWithWrongName() {
        String domainName = "Tairu";

        String expResult = "Tail";

        String result = gene.getDomain( domainName ).getName();
        assertEquals( expResult , result );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void GenesTestGetDomainWithNullName() {
        String domainName = null;
        String expResult = "Tail";

        String result = gene.getDomain( domainName ).getName();
        assertEquals( expResult , result );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void GenesTestGetParteConNombreVacio() {
        String domainName = "";
        String expResult = "Tail";

        String result = gene.getDomain( domainName ).getName();
        assertEquals( expResult , result );
    }

    @Test
    public void testSetDomain() {
        String domainName = "Constants";
        Domain domain = new Domain( "Constants" , 10 , 0.1f , 15f , new FloatElementHandler() );
        gene.setDomain( domain , domainName );
        assertEquals( domain , gene.getDomain( domainName ) );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void GenesTestSetDomainWithNullName() {
        String domainName = null;
        Domain domain = new Domain( "Constants" , 10 , 11 , 15 , new IntegerElementHandler() );
        gene.setDomain( domain , domainName );
        assertEquals( domain , gene.getDomain( domainName ) );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void GenesTestSetDomainWithEmptyName() {
        String domainName = "";
        Domain domain = new Domain( "Constants" , 10 , 11 , 15 , new IntegerElementHandler() );
        gene.setDomain( domain , domainName );
        assertEquals( domain , gene.getDomain( domainName ) );
    }

    @Test( expected = java.lang.IllegalArgumentException.class )
    public void testSetDomainWithUnexistingName() {
        String domainName = "Ventanita";
        Domain domain = new Domain( "Constants" , 10 , 11 , 15 , new IntegerElementHandler() );
        gene.setDomain( domain , domainName );
        assertEquals( domain , gene.getDomain( domainName ) );
    }

    @Test
    public void testgetElement() {
        System.out.println( "getElement" );
        int domainIndex = 0;
        int elementIndex = 0;
        int result = (int) gene.getElement( domainIndex , elementIndex );
        assertTrue(
                (int) gene.getDomainI( domainIndex ).getLowBound() <= result
                && result <= (int) gene.getDomainI( domainIndex ).getHighBound()
        );
    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void testgetElementWithElementIndexLessThanZero() {
        System.out.println( "getElement" );
        int domainIndex = 0;
        int elementIndex = -1;

        int result = (int) gene.getElement( domainIndex , elementIndex );
        assertTrue(
                (int) gene.getDomainI( domainIndex ).getLowBound() <= result
                && result <= (int) gene.getDomainI( domainIndex ).getLowBound() );

    }

    @Test( expected = java.lang.IndexOutOfBoundsException.class )
    public void testgetElementWithElementIndexGreaterThanTheDomainSize() {
        System.out.println( "getElement" );
        int domainIndex = 0;
        int elementIndex = gene.getDomainI( domainIndex ).size();

        int result = (int) gene.getElement( domainIndex , elementIndex );
        assertTrue(
                (int) gene.getDomainI( domainIndex ).getLowBound() <= result
                && result <= (int) gene.getDomainI( domainIndex ).getLowBound() );
    }

    @Test
    public void setElementTest() {
        int elemento = 2;
        int elementIndex = 0;
        int domainIndex = 0;
        gene.setElement( elemento , domainIndex , elementIndex );

        assertEquals( gene.getElement( domainIndex , elementIndex ) , elemento );
    }

}
