/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Operators;

import core.Chromosome;
import core.Gene;
import core.Mutation;
import core.Domain;
import java.util.Random;

/**
 *
 * @author José Alejandro Ponce
 */
public class MutationHeadRandomTerminalInsertion extends Mutation{

    public MutationHeadRandomTerminalInsertion(char modo) {
        super(modo,"MutationHeadRandomTerminalInsertion");
    }

    @Override
    public Chromosome operate(Chromosome operando) {
        Chromosome result=new Chromosome(operando);
        Domain domain;
        Gene gene;
        
        
        Random random = new Random();

        domain=result.getDomainI(0,0);
        
        int elementIndex;
        
        elementIndex = random.nextInt(domain.size());
        
        Object nuevoElemento;
        Integer low_bound = (Integer) result.getDomainI(0,1).getLowBound();
        Integer high_bound =(Integer) result.getDomainI(0,1).getHighBound();
        nuevoElemento = (int) Math.floor(Math.random()*(high_bound-low_bound+1)+low_bound );
        domain.setElement( nuevoElemento,elementIndex);
        return result;
    }
    
}
