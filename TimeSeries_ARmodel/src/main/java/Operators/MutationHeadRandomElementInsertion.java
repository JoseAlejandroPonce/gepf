/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Operators;

import core.Chromosome;
import core.Gene;
import core.Mutation;
import core.Domain;
import java.util.Random;

/**
 *
 * @author José Alejandro Ponce
 */
public class MutationHeadRandomElementInsertion extends Mutation{

    public MutationHeadRandomElementInsertion(char modo) {
        super(modo,"MutationHeadRandomElementInsertion");
    }

    @Override
    public Chromosome operate(Chromosome operando) {
        Chromosome result=new Chromosome(operando);
        Domain domain;
        Gene gene;
        
        Random random = new Random();

        domain = result.getDomainI(0,0);
        
        domain.setElement( domain.generateElement() , random.nextInt(domain.size()) );
        return result;
    }
}
