/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Operators;

import core.Chromosome;
import core.Gene;
import core.Domain;
import core.Permutation;
import java.util.Random;

/**
 *
 * @author alejandro
 */
public class PermutationDomainSwap extends Permutation {

    public PermutationDomainSwap(char modo) {
        super(modo, "PermutationDomainSwap");
    }

    @Override
    public Chromosome operate(Chromosome cromosoma) {
        Random random = new Random();
        int geneIndex1, geneIndex2, domainIndex1;
        Gene geneAux;
        Domain domainAux;

        Chromosome result = new Chromosome(cromosoma);

        geneIndex1 = random.nextInt(result.size()-1);
        geneAux = result.getGene(geneIndex1);

        domainIndex1 = random.nextInt(geneAux.size()-1);
        domainAux = new Domain(geneAux.getDomainI(domainIndex1));

        geneIndex2 = random.nextInt(result.size());
        
        for (int contador = 0; contador < result.size(); contador++) {
            if (result.getGene(geneIndex2).getName() == geneAux.getName()) {
                result.setDomainI(result.getDomainI(geneIndex2, domainIndex1), geneIndex1, domainIndex1);
                result.setDomainI(domainAux, geneIndex2, domainIndex1);
                break;
            }
            geneIndex2 = random.nextInt(result.size());
        }
        return result;
    }
}
