/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Operators;

import core.Chromosome;
import core.Gene;
import core.Permutation;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author alejandro
 */
public class PermutationTranspositionGene extends Permutation {

    public PermutationTranspositionGene(char modo) {
        super(modo, "PermutationTranspositionGENE");
    }

    /**
     * 
     * @param cromosoma
     * @return
     */
    @Override
    public Chromosome operate(Chromosome cromosoma) {
        Random rand = new Random();
        int igo;
        Gene geneAux;
        List<Gene> genesList = new ArrayList<Gene>();
        Chromosome result = new Chromosome(cromosoma);
        igo = rand.nextInt(result.size() - 1);
        geneAux = new Gene(result.getGene(igo));
        genesList.add( geneAux );
        for (int i = 0; i<result.size()-1 ; i++){
            genesList.add( result.getGene( i));
            result.setGen( genesList.get(i), i);
        }
        return result;
    }
}
