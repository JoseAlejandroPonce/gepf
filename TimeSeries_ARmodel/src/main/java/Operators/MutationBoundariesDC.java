/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Operators;

import core.Chromosome;
import core.Gene;
import core.Mutation;
import core.Domain;
import java.util.Random;

/**
 *
 * @author José Alejandro Ponce
 */
public class MutationBoundariesDC extends Mutation {

    public MutationBoundariesDC(char c) {

        super(c, "MutationBaundariesDC");
    }

    @Override
    public Chromosome operate(Chromosome operando) {
        Chromosome result = new Chromosome(operando);
        Domain domain;
        Gene gene;

        int geneIndex , elementIndex;

        Random random = new Random();

        geneIndex = random.nextInt(operando.size() - 1);
        gene = result.getGene(geneIndex);

        domain = gene.getDomainI(gene.size() - 1);

        elementIndex = random.nextInt(domain.size());

        float x = random.nextFloat();

        if (x < 0.5) {
            domain.setElement(domain.copy( domain.getLowBound() ),elementIndex);
        } else if (x >= 0.5) {
            domain.setElement(domain.copy( domain.getHighBound() ),elementIndex);
        }

        return result;
    }
}
