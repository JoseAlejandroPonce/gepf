/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package regresionsimbolicageplinkdc;

import examples.modeling.CsvFilesHelper;
import examples.operators.CrossoverSingleElementSwap;
import examples.operators.CrossoverSingleDomainSwap;
import examples.operators.CrossoverSinglePoint;
import examples.operators.CrossoverTwoPoints;
import modeling.EvaluationRMSE_GEPLINKDCI_AR;
import examples.modeling.Function;
import examples.operators.MutationSingleElementBoundaries;
import Operators.MutationBoundariesDC;
import Operators.MutationDomainStepDC;
import Operators.MutationElementStep;
import Operators.MutationElementStepDC;
import Operators.MutationGeneHighBoundary;
import examples.operators.MutationRandomElementInsertion;
import examples.operators.MutationRandomGeneInsertion;
import examples.operators.MutationRandomDomainInsertion;
import examples.operators.MutationTwoPointsBoundaries;
import Operators.MutationTwoPointElementStep;
import Operators.MutationPlainCopy;
import Operators.PermutationElementSwap;
import Operators.PermutationGeneSwap;
import examples.operators.PermutationTranspositionIS;
import examples.operators.PermutationTranspositionRIS;
import examples.operators.SelectionSUS;
import modeling.DataHelper;
import modeling.ChromosomeCoding;
import modeling.GeneCoding;
import modeling.Evaluation;
import examples.modeling.FloatElementHandler;
import examples.modeling.IntegerElementHandler;
import modeling.Nucleotide;
import core.gepAlgorithm;
import core.Chromosome;
import core.Operator;
import core.Population;
import Operators.MutationHeadRandomElementInsertion;
import Operators.MutationHeadRandomFunctionInsertion;
import Operators.MutationHeadRandomTerminalInsertion;
import examples.operators.MutationRandomChromosomeInsertion;
import Operators.MutationRandomElementInsertionDC;
import Operators.PermutationDomainSwap;
import Operators.PermutationTranspositionGene;
import examples.operators.CrossoverSingleGeneSwap;
import examples.operators.PermutationDomainInversion;
import examples.operators.SelectionRoulette;
import java.util.ArrayList;
import modeling.ElementHandler;
import modeling.EvaluationInverseRMSE_GEPLINKDCI_AR;
import modeling.EvaluationSSE_GEPLINKDCI_PP_AR;
import modeling.Phenotype;


/**
 *
 * @author alejandro
 */
public class WolferSunspots {

    
    public static void addAllOperators( gepAlgorithm alg1 , Population[] poblaciones , int[] tamañopoblaciones ) {
        /**
         * fc = first usable chromosome lc = last usable chromosome op = origin
         * population dp = destination population
         */
        int fc = 0, lc = poblaciones[0].getchromosomes().size(), op = 0, dp = 0;
        float value = (float) 0.1;

        alg1.addOperator( new SelectionSUS() , (float) 1 , op , fc , lc , dp , fc , lc - 1 );
        //Crossovers
        alg1.addOperator( new CrossoverSingleElementSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverSingleGeneSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverSingleDomainSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverSinglePoint( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverTwoPoints( '%' ) , value , op , fc , lc , dp , fc , lc );

        //Example Mutations
        alg1.addOperator( new MutationRandomChromosomeInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationRandomDomainInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationRandomElementInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationRandomGeneInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationSingleElementBoundaries( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationTwoPointsBoundaries( '%' ) , value , op , fc , lc , dp , fc , lc );

        //Specific mutations
        alg1.addOperator( new MutationBoundariesDC( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationDomainStepDC( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationElementStep( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationElementStepDC( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationHeadRandomElementInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationHeadRandomFunctionInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationHeadRandomTerminalInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationRandomElementInsertionDC( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationGeneHighBoundary( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationTwoPointElementStep( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationPlainCopy( '%' ) , value , op , fc , lc , dp , fc , lc );

        // Example Permutations
        alg1.addOperator( new PermutationDomainInversion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new PermutationTranspositionIS( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new PermutationTranspositionRIS( '%' ) , value , op , fc , lc , dp , fc , lc );

        //Specific permutations
        alg1.addOperator( new PermutationDomainSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator(new PermutationElementSwap('%'), value, op, fc, lc, dp, fc,lc);
        alg1.addOperator(new PermutationGeneSwap('%'), value, op, fc, lc, dp, fc,lc);
        alg1.addOperator(new PermutationTranspositionGene('%'), value, op, fc, lc, dp, fc,lc);
    }

    public static void addOptimizedOperators( gepAlgorithm alg1 , Population[] poblaciones , int[] tamañopoblaciones ) {

        /**
         * fc = first usable chromosome lc = last usable chromosome op = origin
         * population dp = destination population
         *
         */
        int fc = 0, lc = poblaciones[0].getchromosomes().size(), op = 0, dp = 0;
		float veryLowValue=0.01f;
		float lowValue=0.02f;
        float value=0.05f;
		float highValue=0.1f;
        alg1.addOperator( new SelectionSUS() , (float) 1 , op , fc , lc , dp , fc , lc - 1 );
        //Crossovers
        alg1.addOperator( new CrossoverSingleElementSwap( '%' ) , highValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverSingleGeneSwap( '%' ) , highValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverSingleDomainSwap( '%' ) , highValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverSinglePoint( '%' ) , highValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new CrossoverTwoPoints( '%' ) , highValue , op , fc , lc , dp , fc , lc );

        //Example Mutations
        alg1.addOperator( new MutationRandomChromosomeInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationRandomDomainInsertion( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationRandomElementInsertion( '%' ) , lowValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationRandomGeneInsertion( '%' ) , lowValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationSingleElementBoundaries( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationTwoPointsBoundaries( '%' ) , value , op , fc , lc , dp , fc , lc );

        //Specific mutations
        alg1.addOperator( new MutationBoundariesDC( '%' ) , veryLowValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationDomainStepDC( '%' ) , lowValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationElementStep( '%' ) , veryLowValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationElementStepDC( '%' ) , veryLowValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationHeadRandomElementInsertion( '%' ) , lowValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationHeadRandomFunctionInsertion( '%' ) , highValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationHeadRandomTerminalInsertion( '%' ) , lowValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationRandomElementInsertionDC( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationGeneHighBoundary( '%' ) , lowValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationTwoPointElementStep( '%' ) , veryLowValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new MutationPlainCopy( '%' ) , lowValue , op , fc , lc , dp , fc , lc );

        // Example Permutations
        alg1.addOperator( new PermutationDomainInversion( '%' ) , veryLowValue , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new PermutationTranspositionIS( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new PermutationTranspositionRIS( '%' ) , lowValue , op , fc , lc , dp , fc , lc );

        //Specific permutations
        alg1.addOperator( new PermutationDomainSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new PermutationElementSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new PermutationGeneSwap( '%' ) , value , op , fc , lc , dp , fc , lc );
        alg1.addOperator( new PermutationTranspositionGene( '%' ) , value , op , fc , lc , dp , fc , lc );
    }

    public static void addOperatorsOnlyTheBest( gepAlgorithm alg1 , Population[] poblaciones , int[] tamañopoblaciones ) {
        /**
         * fc = first usable chromosome lc = last usable chromosome op = origin
         * population dp = destination population
         */
        int fc = 0, lc = poblaciones[0].getchromosomes().size(), op = 0, dp = 0;
        alg1.addOperator( new SelectionRoulette() , (float) 1 , op , fc , lc , dp , fc , lc - 1 );
        alg1.addOperator( new CrossoverSingleElementSwap( '%' ) , 0.05f , op , fc , lc , dp , fc , lc - 1 );
        alg1.addOperator( new MutationElementStepDC( '%' ) , 0.05f , op , fc , lc , dp , fc , lc - 1 );
        alg1.addOperator( new MutationHeadRandomFunctionInsertion( '%' ) , 0.2f , op , fc , lc , dp , fc , lc - 1 );
        alg1.addOperator( new PermutationDomainSwap( '%' ) , 0.05f , op , fc , lc , dp , fc , lc - 1 );
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Create the nucleotides List
        Nucleotide[] nucleotides = new Nucleotide[]{
            new Nucleotide("+", 2),
            new Nucleotide("-", 2),
            new Nucleotide("*", 2),
            new Nucleotide("/", 2),
            new Nucleotide("?", 0),
            new Nucleotide("y1", 0),//y(t-1) 
            new Nucleotide("y2", 0),//y(t-1)
            new Nucleotide("y3", 0),
            new Nucleotide("y4", 0),
            new Nucleotide("y5", 0),
            new Nucleotide("y6", 0),
            new Nucleotide("y7", 0),
            new Nucleotide("y8", 0),
            new Nucleotide("y9", 0),
            new Nucleotide("y10", 0)
        };
        //Crete the geneCoding for GEP-GENE
        String geneCodingName = "GEP-GENE";
        String[] domainNamesArray = new String[]{ "Head" , "Tail" , "TailDCI" , "TailDC" };
        Object[] highBoundsArray = new Object[]{ 11 , 11 , 9 , (float) 1000 };
        Object[] lowBoundsArray = new Object[]{ 0 , 4 , 0 , (float) 0 };
        int hS = 13; //hS = headSize
        int[] domainSizesArray = new int[]{ hS , hS*(2-1)+1 , hS*(2-1)+1 , 15 };
        ElementHandler[]elementHandlersArray = new ElementHandler[]{
            new IntegerElementHandler() ,
            new IntegerElementHandler() ,
            new IntegerElementHandler() ,
            new FloatElementHandler()
        };
        GeneCoding codgen1;
        codgen1 = new GeneCoding(
                domainNamesArray 
                , geneCodingName
                , highBoundsArray 
                , lowBoundsArray 
                , domainSizesArray 
                ,elementHandlersArray 
        );
        //Create the geneCoding for the linking gene
        GeneCoding codgen2 = new GeneCoding(
                new String[]{"Link"},       //DomainNamesArray
                "Link",                     //geneCodingName
                new Object[]{ 0 } ,         //highBoundsArray
                new Object[]{ 0 } ,         //lowBoundsArray
                new int[]{ 1 } ,            //domainSizesArray
                new ElementHandler[]{new IntegerElementHandler() } //elementHandlersArray
        );
        //Create the chromosomeCoding
        ChromosomeCoding codcrom1 = new ChromosomeCoding(
                new GeneCoding[]{ codgen1 , codgen1 , codgen2 } ,   //geneCodingsArray
                "GEP-LINK-+"                                        //chromosomeCodingName
        );
        //Create the algorithm
        String name = "GEP-LINK";
        int populationsQuantity = 1;
        int[] populationSizesArray = new int[]{ 100 };
        Population[] populationsArray = new Population[1];
        populationsArray[0] = new Population(populationSizesArray[0], 0, codcrom1);
        int[] elitesSizesArray = new int[]{ 1 };
        int generations = 2000;

        float acceptableFitness = 100000000;
        Evaluation evaluation = new EvaluationSSE_GEPLINKDCI_PP_AR(nucleotides, acceptableFitness,1000);

        DataHelper helper = CsvFilesHelper.getInstance("../DataSets/","SunSpots.txt");
        gepAlgorithm alg1;
        Phenotype func;
        
        alg1 = new gepAlgorithm(
                name,
                populationsQuantity,
                populationSizesArray, 
                populationsArray, 
                elitesSizesArray, 
                new ArrayList<Operator>() 
                , null 
                , null 
                , null 
                , generations
                , evaluation
                , helper
        );
//         addAllOperators(alg1 , populationsArray , populationSizesArray);
            addOptimizedOperators(alg1 , populationsArray , populationSizesArray);
//            addOperatorsByAverageCopy(alg1 , populationsArray , populationSizesArray);
//            addOperatorsOnlyTheBest(alg1 , populationsArray , populationSizesArray);


        for (int oi = 0; oi < alg1.getOperatorListSize() ; oi++){
            System.out.println(alg1.getOperator( oi).getName());
        }


        alg1.setDebug(false);
        alg1.dontUseHistoricalRecord();
//        alg1.paralelizeFitnessEvaluation();
        

        Evaluation isseppma = new EvaluationInverseRMSE_GEPLINKDCI_AR( nucleotides, acceptableFitness, 100 );
        alg1.setEvaluation( isseppma );
        alg1.setGenerations( 20000 );
        alg1.execute();

        Chromosome mejor = alg1.getElites()[0].getChromosome(0);
        Evaluation error = new EvaluationRMSE_GEPLINKDCI_AR( nucleotides, acceptableFitness );
        func = new Function();
        System.out.println( mejor.getFitness() );
        error.evaluate(mejor);
        System.out.println( mejor.getFitness() );
        func = error.expressChromosome( mejor );
        func.draw();
        System.out.println( func.phenotype.toString() );
        
        System.out.print("\n\nExecution completed without runtime errors\n");
    }
}