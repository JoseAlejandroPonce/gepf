/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package modeling;

import examples.modeling.CsvFilesHelper;
import java.awt.Dimension;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import org.math.plot.Plot2DPanel;

/**
 * Implementation of the Phenotype abstract class. Made to handle the function
 * type of phenotype.
 *
 * @author José Alejandro Ponce
 */
public class FunctionAR extends Phenotype {

    private static JFrame frame = null;

    /**
     * Draws the function using the jmathPlot library.
     *
     * @param evaluation
     */
    @Override
    public void draw() {
	
        
        Plot2DPanel    plot = new Plot2DPanel();
       
        if ( frame == null ) {
            frame = new JFrame( "Elite" );
        }
        CsvFilesHelper ayudante = CsvFilesHelper.getInstance();

        int tamañodatos = ayudante.getData().size();
        ArrayList datos = (ArrayList) ayudante.getData();
        
        double[] x = new double[tamañodatos-10];
        double[] y = new double[tamañodatos-10];

        double[] Y = new double[tamañodatos-10];
        Expression e;
        // Create the exp4j expression variable
        e = new ExpressionBuilder((String) this.phenotype)
                .variables("x","y1","y2","y3","y4","y5","y6","y7","y8","y9","y10")
                .build();
        Float[] s;
        Float[] y1;
        Float[] y2;
        Float[] y3;
        Float[] y4;
        Float[] y5;
        Float[] y6;
        Float[] y7;
        Float[] y8;
        Float[] y9;
        Float[] y10;
        for (int indicedatos = 10; indicedatos < datos.size(); indicedatos++) {
            s = (Float[]) datos.get(indicedatos);
            y1 = (Float[]) datos.get(indicedatos-1);
            y2 = (Float[]) datos.get(indicedatos-2);
            y3 = (Float[]) datos.get(indicedatos-3);
            y4 = (Float[]) datos.get(indicedatos-4);
            y5 = (Float[]) datos.get(indicedatos-5);
            y6 = (Float[]) datos.get(indicedatos-6);
            y7 = (Float[]) datos.get(indicedatos-7);
            y8 = (Float[]) datos.get(indicedatos-8);
            y9 = (Float[]) datos.get(indicedatos-9);
            y10 = (Float[]) datos.get(indicedatos-10);
            e.setVariable("x", s[0]);
            e.setVariable("y1", y1[1]);
            e.setVariable("y2", y2[1]);
            e.setVariable("y3", y3[1]);
            e.setVariable("y4", y4[1]);
            e.setVariable("y5", y5[1]);
            e.setVariable("y6", y6[1]);
            e.setVariable("y7", y7[1]);
            e.setVariable("y8", y8[1]);
            e.setVariable("y9", y9[1]);
            e.setVariable("y10", y10[1]);
            x[indicedatos-10] =  s[0] ;
            Y[indicedatos-10] =  s[1] ;
            y[indicedatos-10] = (float) e.evaluate();
        }

        SwingUtilities.invokeLater( new Runnable() {
            public void run() {
                plot.removeAllPlots();
                plot.addLinePlot( "Results" , x , y );
                plot.addLinePlot( "Data" , x , Y );
                //plot.setFixedBounds(0,1700,2010);
                frame.setContentPane( plot );
                frame.setSize( new Dimension( 800 , 600 ) );
                frame.setVisible( true );
            }
        }
        );
    }
}
