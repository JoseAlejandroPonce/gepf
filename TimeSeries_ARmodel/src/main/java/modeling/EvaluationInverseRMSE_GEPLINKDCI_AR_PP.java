/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package modeling;

import examples.modeling.CsvFilesHelper;
import core.Chromosome;
import core.Gene;
import java.util.ArrayList;
import java.util.List;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

/**
 *
 * @author alejandro
 */
public class EvaluationInverseRMSE_GEPLINKDCI_AR_PP extends Evaluation {

    private static int R;
    //public static int indiceconstantes;
    public EvaluationInverseRMSE_GEPLINKDCI_AR_PP(Nucleotide[] nucleotides, float acceptableFitness,int R) {
        super(nucleotides, acceptableFitness);
        this.R=R;
    }

    /**
     * Implementation of the fitness evaluation method using the ranged inverse of 
     * the RMSE (R+1) / ( RMSE + expression length ). for the case
     * when the phenotype is a mathematical expression with 11 input variables, with
     * each input beeing the current and previous values of the time series.
     *
     * @param phenotype a multivariate mathematical expression
     * @return
     */
    @Override
    public float evaluation(Phenotype phenotype) {
        Float fitness;
        //Primero pasamos el directorio (carpeta) 
        //Esto se usa tambien para guardar los archivos por eso debe estar separado
        DataHelper ayudante = CsvFilesHelper.getInstance();
        //Ahora le indicamos el nombre del archivo desde el cual cargar los datos
        List datos = ayudante.getData();
        //Creo un array para guardar los results parciales
        //float[] partialfitness= new float[datos.size()];
        float fitnessfinal = 0;
        float evaluation;
        boolean flag;
        Expression e = new ExpressionBuilder((String) phenotype.phenotype)
                    .variables("x","y1","y2","y3","y4","y5","y6","y7","y8","y9","y10")
                    .build();
        Float[] s;
        Float[] y1;
        Float[] y2;
        Float[] y3;
        Float[] y4;
        Float[] y5;
        Float[] y6;
        Float[] y7;
        Float[] y8;
        Float[] y9;
        Float[] y10;
        //Itero por cada valor de los datos de entrada (X en este caso)
        for (int i = 10; i < datos.size(); i++) {
            //Cargo en s el i-ezimo dato
            s = (Float[]) datos.get(i);
            y1 = (Float[]) datos.get(i-1);
            y2 = (Float[]) datos.get(i-2);
            y3 = (Float[]) datos.get(i-3);
            y4 = (Float[]) datos.get(i-4);
            y5 = (Float[]) datos.get(i-5);
            y6 = (Float[]) datos.get(i-6);
            y7 = (Float[]) datos.get(i-7);
            y8 = (Float[]) datos.get(i-8);
            y9 = (Float[]) datos.get(i-9);
            y10 = (Float[]) datos.get(i-10);
            // creo la variable expresion de exp4j
            e.setVariable("x", s[0]);
            e.setVariable("y1", y1[1]);
            e.setVariable("y2", y2[1]);
            e.setVariable("y3", y3[1]);
            e.setVariable("y4", y4[1]);
            e.setVariable("y5", y5[1]);
            e.setVariable("y6", y6[1]);
            e.setVariable("y7", y7[1]);
            e.setVariable("y8", y8[1]);
            e.setVariable("y9", y9[1]);
            e.setVariable("y10", y10[1]);
            //evaluo. esto debe ir en un try catch para tomar las excepciones 
            //matematicas (ej: division por cero)
            try {
                evaluation = (float) e.evaluate();
            } catch (java.lang.ArithmeticException ee) {
                //Nota: Este valor es positivo por que despues se le cambia el signo
                //Debido al uso de la potencia al cuadrado en los results parciales
                evaluation = 1000000;
            }
            fitnessfinal += (float) Math.pow(s[1] - evaluation,2);
        }
        //Por ultimo una vez obtenido el vector partialfitness 
        //Calculamos el valor float haciendo una suma o promedio o lo que 
        //Corresponda
        fitness = (float)  (R+1)/ ((float) Math.sqrt(fitnessfinal/datos.size())+ 0.001f*phenotype.phenotype.toString().length());
        return fitness;
    }
    
    @Override
    public Phenotype expressChromosome(Chromosome chromosomes) {
        
        Phenotype func= new FunctionAR();
        Nucleotide link;
        String linkstring = "";
        String function = "";
        List<String> subfuncion = new ArrayList<String>();
        boolean tieneLink = false;
        Integer indicetailDC;
        for (int geneIndex = 0; geneIndex < chromosomes.size(); geneIndex++) {
            if (chromosomes.getGene(geneIndex).getName().compareTo("core.Link") == 0) {
                //Extraigo de la lista de nucleotides
                //La funcion de link
                //usando como indice el unico elemento del gene con nombre "Link" 
                link = Evaluation.nucleotides[(int) chromosomes.getGene(geneIndex).getDomainI(0).getElement(0)];
                linkstring = (String) link.getNucleotide();
                tieneLink = true;
            }
        }
        //Ahora que ya tenemos la funcion de link tenemos que traducir 
        for (int geneIndex = 0; geneIndex < chromosomes.size(); geneIndex++) {
            //Primero revisamos que el nombre del gene sea GEP-GEN 
            if (chromosomes.getGene(geneIndex).getName().compareTo("GEP-GENE") == 0) {
                //Ahora creamos un vector con las arities del head en el gene
                //Que sera necesario para el metodo traducir gene

                //Declaramos el vector (debe hacerse aqui lamentablemente)
                int[] arities = new int[chromosomes.getGene(geneIndex).getDomainI(0).size()];
                Integer elementAux;
                //Creamos el vector
                for (int elementIndex = 0; elementIndex < arities.length; elementIndex++) {
                    elementAux = (int) chromosomes.getGene(geneIndex).getDomainI(0).getElement(elementIndex);
                    arities[elementIndex] = EvaluationInverseRMSE_GEPLINKDCI_AR_PP.nucleotides[elementAux].getArity();
                }
                for (int elementIndex = 1; elementIndex < arities.length; elementIndex++) {
                    arities[elementIndex] = arities[elementIndex] + arities[elementIndex - 1];
                }
                //Fundamental. asignar indicetail=0 antes de llamar traducirGEN
                indicetailDC = 0;
                subfuncion.add(translateGEPGENE(chromosomes.getGene(geneIndex), arities, 0,indicetailDC));
            }
        }
        if (tieneLink) {
            //Ponemos la primer subfuncion (correspondiente al gene 0
            function = function.concat(subfuncion.get(0));
            for (int geneIndex = 1; geneIndex < subfuncion.size(); geneIndex++) {
                //Concatenamos usando la funcion de link
                function = function.concat(linkstring);
                function = function.concat(subfuncion.get(geneIndex));
            }
            //Ahora dejamos seteado el phenotype de la evaluation para luego evaluate
            func.setPhenotype(function);
        } else {
            func.setPhenotype(subfuncion.get(0));
        }
        return func;
    }

    //Debe devolver todo entre parentesis
    public String translateGEPGENE(Gene gene, int[] arities, int elementIndex,Integer indicetailDC) {
        //Obtenemos el nucleotido 
        //Traduccion particular para el caso GEPLINKDC
       if (elementIndex >=gene.getDomainI(0).size()){
                //int aux=indicetail;
                //indicetail++;
            int aux = elementIndex - gene.getDomainI(0).size();
            if (this.nucleotides[(int) gene.getElement(1, aux)].getNucleotide()== "?"){
                indicetailDC++;
                return "("+gene.getElement(3, (int) gene.getElement(2,indicetailDC-1)).toString()+")";
            }
            return (String) "("+this.nucleotides[(int) gene.getElement(1, aux)].getNucleotide().toString()+")";
        }else{
            Nucleotide nuc = this.nucleotides[(int) gene.getDomainI(0).getElement(elementIndex)];
            int aridad = nuc.getArity();
            switch (aridad) {
                case 0:
                    if (nuc.getNucleotide() == "?") {
                        indicetailDC++;
                        return "(" + gene.getElement( 3, (int) gene.getElement(2,indicetailDC-1)).toString() + ")";
                    }
                    return (String) "(" + nuc.getNucleotide() + ")";
                case 1:
                    if (elementIndex > 0) {
                        elementIndex = arities[elementIndex - 1];
                    }
                    return (String) nuc.getNucleotide() + "(" + this.translateGEPGENE(gene, arities, elementIndex + 1,indicetailDC) + ")";
                case 2:
                    if (elementIndex > 0) {
                        elementIndex = arities[elementIndex - 1];
                    }
                    return "(" + (String) this.translateGEPGENE(gene, arities, elementIndex + 1,indicetailDC) + (String) nuc.getNucleotide() + this.translateGEPGENE(gene, arities, elementIndex + 2,indicetailDC) + ")";
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }
}
