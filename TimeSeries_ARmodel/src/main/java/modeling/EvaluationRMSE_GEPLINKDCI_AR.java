/* 
 * Copyright (C) 2016 José Alejandro Ponce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package modeling;

import examples.modeling.CsvFilesHelper;
import modeling.DataHelper;
import modeling.Evaluation;
import modeling.Phenotype;
import modeling.Nucleotide;
import core.Chromosome;
import core.Gene;
import examples.modeling.Function;
import java.util.ArrayList;
import java.util.List;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

/**
 *
 * @author José Alejandro Ponce
 */
public class EvaluationRMSE_GEPLINKDCI_AR extends Evaluation {

    public EvaluationRMSE_GEPLINKDCI_AR(Nucleotide[] nucleotides, float acceptableFitness) {
        super(nucleotides, acceptableFitness);
    }

    /**
     * Implementation of the fitness evaluation method using the RMSE.for the case
     * when the phenotype is a mathematical expression with 11 input variables, with
     * each input beeing the current and previous values of the time series.
     *
     * @param phenotype
     * @return
     */
    @Override
    public float evaluation(Phenotype phenotype) {
        Float finalFitness;
        DataHelper ayudante = CsvFilesHelper.getInstance();
        List datos = ayudante.getData();
        float partialFitness = 0;
        float evaluation;
        boolean flag;
        Expression e = new ExpressionBuilder((String) phenotype.phenotype)
                    .variables("x","y1","y2","y3","y4","y5","y6","y7","y8","y9","y10")
                    .build();
        Float[] s;
        Float[] y1;
        Float[] y2;
        Float[] y3;
        Float[] y4;
        Float[] y5;
        Float[] y6;
        Float[] y7;
        Float[] y8;
        Float[] y9;
        Float[] y10;
        for (int i = 10; i < datos.size(); i++) {
            s = (Float[]) datos.get(i);
            y1 = (Float[]) datos.get(i-1);
            y2 = (Float[]) datos.get(i-2);
            y3 = (Float[]) datos.get(i-3);
            y4 = (Float[]) datos.get(i-4);
            y5 = (Float[]) datos.get(i-5);
            y6 = (Float[]) datos.get(i-6);
            y7 = (Float[]) datos.get(i-7);
            y8 = (Float[]) datos.get(i-8);
            y9 = (Float[]) datos.get(i-9);
            y10 = (Float[]) datos.get(i-10);
            e.setVariable("x", s[0]);
            e.setVariable("y1", y1[1]);
            e.setVariable("y2", y2[1]);
            e.setVariable("y3", y3[1]);
            e.setVariable("y4", y4[1]);
            e.setVariable("y5", y5[1]);
            e.setVariable("y6", y6[1]);
            e.setVariable("y7", y7[1]);
            e.setVariable("y8", y8[1]);
            e.setVariable("y9", y9[1]);
            e.setVariable("y10", y10[1]);
            try {
                evaluation = (float) e.evaluate();
            } catch (java.lang.ArithmeticException ee) {
                evaluation = 1000000;
            }
            partialFitness += (float) Math.pow(s[1] - evaluation,2);
        }
        finalFitness = (float) Math.sqrt(partialFitness/datos.size());
        return finalFitness;
    }
    
    @Override
    public Phenotype expressChromosome(Chromosome chromosomes) {
        Phenotype func= new FunctionAR();
        Nucleotide link;
        String linkstring = "";
        String function = "";
        List<String> subfuncion = new ArrayList<String>();
        boolean tieneLink = false;
        Integer indicetailDC;
            if (chromosomes.getGene(chromosomes.size()-1).getName().compareTo("Link") == 0) {
                link = Evaluation.nucleotides[(int) chromosomes.getGene(chromosomes.size()-1).getDomainI(0).getElement(0)];
                linkstring = (String) link.getNucleotide();
                tieneLink = true;
            }
        for (int geneIndex = 0; geneIndex < chromosomes.size(); geneIndex++) {
            if (chromosomes.getGene(geneIndex).getName().compareTo("GEP-GENE") == 0) {
                int[] arities = new int[chromosomes.getGene(geneIndex).getDomainI(0).size()];
                Integer elementAux;
                for (int elementIndex = 0; elementIndex < arities.length; elementIndex++) {
                    elementAux = (int) chromosomes.getGene(geneIndex).getDomainI(0).getElement(elementIndex);
                    arities[elementIndex] = EvaluationRMSE_GEPLINKDCI_AR.nucleotides[elementAux].getArity();
                }
                for (int elementIndex = 1; elementIndex < arities.length; elementIndex++) {
                    arities[elementIndex] = arities[elementIndex] + arities[elementIndex - 1];
                }
                indicetailDC = 0;
                subfuncion.add(translateGEPGENE(chromosomes.getGene(geneIndex), arities, 0,indicetailDC));
            }
        }
        if (tieneLink) {
            function = function.concat(subfuncion.get(0));
            for (int geneIndex = 1; geneIndex < subfuncion.size(); geneIndex++) {
                function = function.concat(linkstring);
                function = function.concat(subfuncion.get(geneIndex));
            }
            func.setPhenotype(function);
        } else {
            func.setPhenotype(subfuncion.get(0));
        }
        return func;
    }

    public String translateGEPGENE(Gene gene, int[] arities, int elementIndex,Integer indicetailDC) {
       if (elementIndex >=gene.getDomainI(0).size()){
            int aux = elementIndex - gene.getDomainI(0).size();
            if (this.nucleotides[(int) gene.getElement(1, aux)].getNucleotide()== "?"){
                indicetailDC++;
                return "("+gene.getElement(3, (int) gene.getElement(2,indicetailDC-1)).toString()+")";
            }
            return (String) "("+this.nucleotides[(int) gene.getElement(1, aux)].getNucleotide().toString()+")";
        }else{
            Nucleotide nuc = this.nucleotides[(int) gene.getDomainI(0).getElement(elementIndex)];
            int aridad = nuc.getArity();
            switch (aridad) {
                case 0:
                    if (nuc.getNucleotide() == "?") {
                        indicetailDC++;
                        return "(" + gene.getElement( 3, (int) gene.getElement(2,indicetailDC-1)).toString() + ")";
                    }
                    return (String) "(" + nuc.getNucleotide() + ")";
                case 1:
                    if (elementIndex > 0) {
                        elementIndex = arities[elementIndex - 1];
                    }
                    return (String) nuc.getNucleotide() + "(" + this.translateGEPGENE(gene, arities, elementIndex + 1,indicetailDC) + ")";
                case 2:
                    if (elementIndex > 0) {
                        elementIndex = arities[elementIndex - 1];
                    }
                    return "(" + (String) this.translateGEPGENE(gene, arities, elementIndex + 1,indicetailDC) + (String) nuc.getNucleotide() + this.translateGEPGENE(gene, arities, elementIndex + 2,indicetailDC) + ")";
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }
}
